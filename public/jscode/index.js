$(function () {


    $("body").on("click", ".slider1_click_modal", function () {
        var modal_title = $(this).data('modal_title');
        var modal_body = $(this).data('modal_body');
        var modal_button = $(this).data('modal_button');


        swal({
            title: modal_title,
            confirmButtonText: modal_button,
            width: '80%',
            html: modal_body ,
            showCloseButton: true,

        })
    });


});