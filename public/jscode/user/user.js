$(function () {

    $('.prevent_form_from_submit_on_keypress').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $("body").on("change", ".available_regions_list_class", function () {
        var region_id = $(this).val();
        var parent_element = $($(this).data("parent_element"));

        if (typeof ($(this).data("parent_element")) == "undefined") {
            var parent_element = $("body");
        }

        if ($(this).is(":checked")) {
            $(".available_countries_list_class[data-region_id='" + region_id + "']", parent_element).prop("checked", "checked");
        }
        else {
            $(".available_countries_list_class[data-region_id='" + region_id + "']", parent_element).removeAttr("checked");
        }

    });

    $("body").on("click", ".select_all", function () {

        var confirm_res = confirm("Are you sure?");

        if (confirm_res) {
            var parent_element = $($(this).data("parent_element"));
            $("input", parent_element).prop("checked", "checked");
        }

        return false;
    });

    $("body").on("click", ".deselect_all", function () {

        var confirm_res = confirm("Are you sure?");

        if (confirm_res) {
            var parent_element = $($(this).data("parent_element"));
            $("input", parent_element).removeAttr("checked");
        }

        return false;
    });

    $('body').on("click", ".favorite_blacklist", function () {
        var confirm_res = confirm("Are you Sure?");
        if (confirm_res == true) {

            var this_element = $(this);
            var object = {};

            object._token = _token;
            object.comp_id = this_element.data("compid");
            object.blacklisted_or_favourite = this_element.data("accept");
            object.to_or_from = this_element.data("tofrom");

            //show load img
            this_element.append("<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' width='20'>");

            $.ajax({
                url: base_url2 + '/user/broadcast/favorite_company',
                type: 'POST',
                data: object,
                success: function (data) {
                    // console.log(data);

                    var returned_data = JSON.parse(data);
                    if (typeof (returned_data.msg) != "undefined") {
                        $('.ajax_loader_class', this_element).remove();
                        this_element.parents(".parent_accepters_div").html(returned_data.msg);
                    }

                }
            });

        }//end confirmation if

        return false;
    });

    // Delete Item stock

    $('body').on("click", ".delete_item", function () {
        var confirm_res = confirm("Are you Sure?");
        if (confirm_res == true) {

            var this_element = $(this);
            var object = {};

            var item_id = this_element.data("itemid");

            object._token = _token;
            object.item_id = item_id;

            //show load img
            this_element.append("<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' width='20'>");

            $.ajax({
                url: base_url2 + '/user/remove_item',
                type: 'POST',
                data: object,
                success: function (data) {
                    // console.log(data);

                    if (data) {
                        $('.ajax_loader_class', this_element).remove();
                        this_element.parents(".parent_row").remove();
                    }

                }
            });

        }//end confirmation if

        return false;
    });

    // remove email list
    $('body').on("click", ".delete_email", function () {
        var confirm_res = confirm("Are you Sure?");
        if (confirm_res == true) {

            var this_element = $(this);
            var object = {};

            var ume_id = this_element.data("emailid");

            object._token = _token;
            object.ume_id = ume_id;

            //show load img
            this_element.append("<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' width='20'>");

            $.ajax({
                url: base_url2 + '/user/marketing_messages/remove_email_marketing',
                type: 'POST',
                data: object,
                success: function (data) {
                    // console.log(data);

                    if (data) {
                        $('.ajax_loader_class', this_element).remove();
                        this_element.parents(".parent_row").remove();
                    }

                }
            });

        }//end confirmation if

        return false;
    });

    // convert currency

    $('body').on("change", ".select_change_currency", function () {
        $('.convert').click();
    });

    $('body').on("click", ".convert", function () {
        var parent_div = $(".convert_currency_parent_div");

        if (!check_valid(parent_div)) {
            return false;
        }


        var this_element = $(this);
        var object = {};

        var from_currency = $('.from_currency').val();
        var to_currency = $('.to_currency').val();
        var currency_number = $('.currency_number').val();

        object._token = _token;
        object.currency_number = currency_number;
        object.from_currency = from_currency;
        object.to_currency = to_currency;

        //show load img
        this_element.append(ajax_loader_img_func("10px"));

        $.ajax({
            url: base_url2 + '/user/currency',
            type: 'POST',
            data: object,
            success: function (data) {
                console.log(data);
                if (data) {
                    $('.ajax_loader_class', this_element).remove();
                    $('.result').val(data);
                }

            }
        });


        return false;
    });

    $("body").on("click", ".go_to_reply_button", function () {

        $('.load_view_modal .modal-body').animate({scrollTop: $(".reply_div_panel", $('.load_view_modal .modal-body')).offset().top - 150}, 800, 'swing');

        if ($(".reply_div_panel .panel-heading", $('.load_view_modal .modal-body')).attr("class").includes("collapsed")) {
            $(".reply_div_panel .panel-heading", $('.load_view_modal .modal-body')).click();
        }
        return false;
    });

    $("body").on("click", ".go_to_company_button", function () {

        $('.load_view_modal .modal-body').animate({scrollTop: $(".company_parent_div", $('.load_view_modal .modal-body')).offset().top - 150}, 800, 'swing');

        if ($(".company_parent_div .panel-heading", $('.load_view_modal .modal-body')).attr("class").includes("collapsed")) {
            $(".company_parent_div .panel-heading", $('.load_view_modal .modal-body')).click();
        }
        return false;
    });

    //dashboard
    $(".dashboard_search_by").change(function () {

        $(".search_type").html($(this).val());

        if ($(this).val() == "Broadcast") {
            $(".search_input").attr("name", "search_for_broadcast");
            $(".search_form").attr("action", base_url2 + "/user/broadcast/show_all_broadcasts");
        }
        else {
            $(".search_input").attr("name", "keyword");
            $(".search_form").attr("action", base_url2 + "/user/search/item");
        }
    });






    $(".select_comp_to_data").change();

    $(".send_broadcast_form").submit(function () {

        var to_user_type_count = $('[name="to_available_user_types_list[]"]:checked').length;
        var to_regions_count = $('[name="to_available_regions_list[]"]:checked').length;
        var to_countries_count = $('[name="to_available_countries_list[]"]:checked').length;


        if (
            to_user_type_count == 0 ||
            to_regions_count == 0 ||
            to_countries_count == 0
        ) {
            alert("you must select regions,countries,user types");
            return false;
        }

    });


    //broadcast notifications
    $(".select_all_notifications").click(function () {

        var confirm_res = confirm("Are you sure?");

        if (!confirm_res) {
            return false;
        }

        var check_type = $(this).data("check_type");
        var parent_table = $(this).parents("table");

        $("input[type='radio']", parent_table).removeAttr("checked")
        $("input[type='radio'][value='" + check_type + "']", parent_table).prop("checked", "checked");

        return false;
    });


    Error_js = {};
    $("body").on("blur", ".company_website", function () {
        var check = isUrlValid($('.company_website').val())
        if (!check) {
            $('#company_website').html("<div class='alert alert-danger'>invalid Website please make this Ex. https://www.google.com</div>")
            Error_js.company_website = "<div class='alert alert-danger'>invalid Website please make this Ex. https://www.google.com</div>";
        } else {
            delete Error_js.company_website
            $('#company_website').html("");
        }

    });

    function isUrlValid(url) {
        return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
    }


    if ($(".show_broadcast_at_latest_broadcasts").length) {
        $("[data-href='" + base_url2 + "/user/broadcast/show_broadcast?broadcast_id=" + $(".show_broadcast_at_latest_broadcasts").val() + "']").first().click();
    }

    if ($(".show_remote_broadcast_at_latest_broadcasts").length) {
        $("[data-href='" + base_url2 + "/user/remote_broadcast/show_remote_broadcast?remote_broad_id=" + $(".show_remote_broadcast_at_latest_broadcasts").val() + "']").first().click();
    }


    if ($(".show_marketing_msg_at_latest_marketing_msgs").length) {
        $("[data-href='" + base_url2 + "/user/marketing_messages/show_marketing_msg?marketing_msg_id=" + $(".show_marketing_msg_at_latest_marketing_msgs").val() + "']").first().click();
    }




    $('body').on("submit", "#save_form", function () {
        if (!$.isEmptyObject(Error_js)){
            return false;
        }
    });

});