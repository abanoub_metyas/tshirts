$(function(){

    try {
        var socket = io.connect($(".socket_link").val())
    } catch (e) {
        //warn user
    }

    if (socket !== undefined) {
        console.log("ok");

        socket.emit("login",$(".header_user_id").val(),base_url2);

        //listen for output
        socket.on("user_already_logged_in",function(data){
            alert("your account is logged in at another location");
            location.href=base_url2+"/logout";
        });


    }
});