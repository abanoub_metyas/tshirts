
var show_or_hide_delete_all_btn;

$(function () {

    show_or_hide_delete_all_btn=function(){
        if ($('.select_checkbox_item:checked').length > 0) {
            $('.div_send_list_ref').show();
        } else {
            $('.div_send_list_ref').hide();
        }
    };


    $(".select_all_stock_items").click(function(){

        var confirm_res = confirm("Are you sure?");

        if (confirm_res) {
            var parent_element = $($(this).data("parent_element"));
            $("input", parent_element).prop("checked", "checked");
        }

        show_or_hide_delete_all_btn();

        return false;
    });


    $(".de_select_all_stock_items").click(function(){

        var confirm_res = confirm("Are you sure?");

        if (confirm_res) {
            var parent_element = $($(this).data("parent_element"));
            $("input", parent_element).removeAttr("checked");
        }

        show_or_hide_delete_all_btn();

        return false;
    });


    $("body").on("change", ".select_child_cat_id", function () {
        var cat_id = $(this).val();
        console.log(cat_id);

        $(".select_checkbox_item").removeAttr("checked");

        if (cat_id == 0) {
            $(".select_checkbox_item[data-parent_cat_id='" + $('.select_parent_cat_id').val() + "']").prop("checked", "checked");
        }
        else {
            $(".select_checkbox_item[data-child_cat_id='" + cat_id + "']").prop("checked", "checked");
        }

        show_or_hide_delete_all_btn();
    });

    $("body").on("click", ".select_checkbox_item", function () {
        show_or_hide_delete_all_btn();
    });

    $("body").on("click", ".delete_all_select", function () {

        var confirm_res = confirm("Are you Sure?");
        if (confirm_res == true) {
            var parent_element = $('.parent_item_div')
            var item_list_ids = [];
            $.each($(".select_checkbox_item", parent_element), function () {
                if ($(this).is(':checked')) {
                    item_list_ids.push($(this).data('item_id'));
                }
            });
            var object = {}

            object.item_list_ids = JSON.stringify(item_list_ids)
            object._token = _token;

            $.ajax({
                url: base_url2 + '/user/delete_item_from_stock',
                type: 'POST',
                data: object,
                success: function (data) {
                    location.reload();
                }
            });
        }

    });

});