$(function () {




    $("body").on("click", ".select_checkbox_item", function () {

        if ($('.select_checkbox_item:checked').length > 0) {
            $('.div_send_list_ref').show();
        }else {
            $('.div_send_list_ref').hide();
        }
    });

    $("body").on("click", ".send_list_ref", function () {

        var parent_element = $('.parent_item_div')
        var item_list_ids =[];

        $.each($(".select_checkbox_item", parent_element), function () {
            if ($(this).is(':checked')) {
                item_list_ids.push($(this).data('rowid'));
            }
        });

        $('.item_list_ids').val(JSON.stringify(item_list_ids));
        $('#send_list_items_Modal').modal('show');

    });

});