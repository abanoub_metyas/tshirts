$(function () {

    var to_company_code = $('.to_company_code').val();

    if(to_company_code.length >0) {

        console.log(to_company_code);
        var object ={};
        object._token = _token;
        object.user_code = to_company_code;

        $.ajax({
            url: base_url2 + '/get_company_data',
            type: 'GET',
            data: object,
            success: function (data) {
                // console.log(data);

                var returned_data = JSON.parse(data);
                if (data) {

                    $('.comp_city_to').val(returned_data.city);
                    $('.comp_address_to').val(returned_data.company_address);
                    $('.comp_name_to').val(returned_data.company_name);
                    $('.comp_country_to').val(returned_data.country_name);
                    $('.comp_phone_to').val(returned_data.telephone);
                    $('.comp_code_to').val(returned_data.phone_code);
                    $('.comp_mobile_to').val(returned_data.mobile_number);
                    $('.comp_region_to').val(returned_data.region_name);
                    $('.comp_zipcode_to').val(returned_data.zipcode);
                    $('.list_company_to').hide();
                    $('.input_search_to').val(returned_data.username+' - '+returned_data.company_name+' - '+returned_data.country_name);

                }

            }
        });


    }

    $("body").on("keyup", ".input_search_from", function () {

        var value = $('.input_search_from').val().toLowerCase();
        var list_li = $('.list_company_from').children('li');
        if (value.length > 2){

            $.each( list_li, function( key, li_element) {
                if ($(this).data("string").includes(value)){
                    $(this).show();
                }
                else{
                    $(this).hide();
                }
            });
            $('.list_company_from').show();

        }
        else{
            $('.list_company_from').hide();
        }

    });


    $('body').on("click", ".choose_company_from", function () {

        var this_element = $(this);
        var object = {};
        var comp_id = this_element.attr('data-companyid');

        var text_company_name = this_element.parent().attr('data-string');
        $('.input_search_from').val(text_company_name)

        object._token = _token;
        object.comp_id = comp_id;
        //show load img
        this_element.append(ajax_loader_img_func("10px"));

        $.ajax({
            url: base_url2 + '/get_company_data',
            type: 'GET',
            data: object,
            success: function (data) {
                // console.log(data);

                var returned_data = JSON.parse(data);
                if (data) {

                    $('.comp_city_from').val(returned_data.city);
                    $('.comp_address_from').val(returned_data.company_address);
                    $('.comp_name_from').val(returned_data.company_name);
                    $('.comp_country_from').val(returned_data.country_name);
                    $('.comp_phone_from').val(returned_data.telephone);
                    $('.comp_code_from').val(returned_data.phone_code);
                    $('.comp_mobile_from').val(returned_data.mobile_number);
                    $('.comp_region_from').val(returned_data.region_name);
                    $('.comp_zipcode_from').val(returned_data.zipcode);
                    $('.ajax_loader_class', this_element).remove();
                    $('.list_company_from').hide();

                }

            }
        });


        return false;

    });

    $("body").on("keyup", ".input_search_to", function () {

        var value = $('.input_search_to').val().toLowerCase();
        var list_li = $('.list_company_to').children('li');
        if (value.length > 2){

            $.each( list_li, function( key, li_element) {
                if ($(this).data("string").includes(value)){
                    $(this).show();
                }
                else{
                    $(this).hide();
                }
            });
            $('.list_company_to').show();

        }
        else{
            $('.list_company_to').hide();
        }

    });

    $('body').on("click", ".choose_company_to", function () {


        var this_element = $(this);
        var object = {};
        var comp_id = this_element.attr('data-companyid');

        var text_company_name = this_element.parent().attr('data-string');
        $('.input_search_to').val(text_company_name)


        object._token = _token;
        object.comp_id = comp_id;
        //show load img
        this_element.append(ajax_loader_img_func("10px"));

        $.ajax({
            url: base_url2 + '/get_company_data',
            type: 'GET',
            data: object,
            success: function (data) {
                // console.log(data);

                var returned_data = JSON.parse(data);
                if (data) {

                    $('.comp_city_to').val(returned_data.city);
                    $('.comp_address_to').val(returned_data.company_address);
                    $('.comp_name_to').val(returned_data.company_name);
                    $('.comp_country_to').val(returned_data.country_name);
                    $('.comp_phone_to').val(returned_data.telephone);
                    $('.comp_code_to').val(returned_data.phone_code);
                    $('.comp_mobile_to').val(returned_data.mobile_number);
                    $('.comp_region_to').val(returned_data.region_name);
                    $('.comp_zipcode_to').val(returned_data.zipcode);
                    $('.ajax_loader_class', this_element).remove();
                    $('.list_company_to').hide();

                }

            }
        });


        return false;

    });


});