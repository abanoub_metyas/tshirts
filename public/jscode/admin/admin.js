
var calc_order_total_amount;

$(function () {

    $('.prevent_form_from_submit_on_keypress').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $("body").on("click",".add_new_pro_quantity",function(){

        var confirm_res=confirm("are you sure?");

        if(!confirm_res)return false;


        var clone_item=$(".first_new_one").first().clone();

        $.each($("select,input",clone_item),function () {
            var new_name=$(this).attr("name");
            new_name=new_name.replace("new_0_", "new_"+$(".first_new_one").length+"_");
            $(this).attr("name",new_name);
        });

        $(".new_items").append(clone_item);

    });

    $('.print_page').click(function () {
        window.print();
    });


    calc_order_total_amount=function(){
        var total_amount=0;
        $.each($(".order_item"),function () {

            total_amount+=
                parseFloat($(".order_price",$(this)).text())*
                parseFloat($(".order_quantity",$(this)).text());

        });

        $(".order_total_amount").html(total_amount);
    };


    if($(".order_total_amount").length){
        calc_order_total_amount();
    }

    $(".add_single_product_to_bill").click(function () {

        var this_element = $(this);
        this_element.append(ajax_loader_img_func("15px"));

        var obj={};
        obj._token=_token;
        obj.bill_id=this_element.data("bill_id");

        xhr=$.ajax({
            url: base_url2+"/service_operator/orders/add_single_product",
            type: "POST",
            data: obj,
            success: function (data) {
                this_element.children("img").remove();

                show_flash_message("info",data);
            }
        });


    });

    $(".confirm_before_go").click(function () {

        var confirm_res=confirm("Are you sure?");

        if(!confirm_res){
            return false;
        }

    });


});