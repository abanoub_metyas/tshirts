$(function () {


    $("body").on("blur", ".company_website", function () {
        var check = isUrlValid($('.company_website').val())
        if (!check) {
            $('#company_website').html("<div class='alert alert-danger'>invalid Website please make this Ex. https://www.google.com</div>")
        } else {
            $('#company_website').html("");
        }

    });


    $("body").on("blur", ".username", function () {
        var string_data = $('.username').val();
        var check = string_data.indexOf(' ');
        console.log(check);

        if (check >= 0) {
            $('#username').html("<div class='alert alert-danger'>Minimum 4 characters, and no spaces allowed</div>");
            return false;
        }
        else {
            $('#username').html("");
        }

        console.log(check);
        if (string_data.length < 4) {
            $('#username').html("<div class='alert alert-danger'>Minimum 4 characters, and no spaces allowed </div>")
        }
        else {
            $('#username').html("");
        }
    });

    function isUrlValid(url) {
        return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
    }

    $("body").on("click", ".register_btn", function () {

        var this_element = $(this);
        var parent_element = this_element.parents(".register_parent_div");

        if (!check_valid(parent_element)) {
            return false;
        }

        var check__website = isUrlValid($('.company_website').val())
        if (!check__website) {
            $('#company_website').html("<div class='alert alert-danger'>invalid Website please make this Ex. https://www.google.com</div>")
            return false;
        } else {
            $('#company_website').html("");
        }


        var string_data = $('.username').val();
        var check_username = string_data.indexOf(' ');

        if (check_username >= 0) {
            $('#username').html("<div class='alert alert-danger'>Please not use space</div>");
            return false;
        }
        else {
            $('#username').html("");
        }

        if (string_data.length < 4) {
            $('#username').html("<div class='alert alert-danger'>username length must be more than 4 characters </div>")
            return false;
        }
        else {
            $('#username').html("");
        }




        if ($(".email", parent_element).val() != $(".email_confirm", parent_element).val()) {
            $('#message').html("<div class='alert alert-danger'>email does not match</div>");
            return false;
        }
        if ($(".password", parent_element).val() != $(".password_confirm", parent_element).val()) {
            $('#message').html("<div class='alert alert-danger'>password does not match</div>");
            return false;
        }

        this_element.append(ajax_loader_img_func("10px"));
        this_element.attr('disabled', 'disabled');

        var url = parent_element.data("url");
        var imgs_input = $("#user_img_fileid", parent_element)[0];

        var form_data = new FormData();

        $.each($('.user_attachments_files_class'), function () {
            form_data.append("user_attachments_files[]", $(this)[0].files[0]);
        });


        form_data.append("user_img_file", imgs_input.files[0]);
        form_data.append("json_values_of_slideruser_attachments_files", $('#json_values_of_slider_iduser_attachments_files').val());

        if ($(".membership_id", parent_element).length) {
            form_data.append("membership_id", $(".membership_id", parent_element).val());
        }

        form_data.append("username", $(".username", parent_element).val());
        form_data.append("company_name", $(".company_name", parent_element).val());

        $.each($(".user_type_ids", parent_element), function () {
            if ($(this).is(':checked')) {
                form_data.append("user_type_ids[]", $(this).val());
            }
        });


        form_data.append("company_address", $(".company_address", parent_element).val());
        form_data.append("user_timezone", $("#user_timezone_id", parent_element).val());
        form_data.append("city", $(".city", parent_element).val());
        form_data.append("zipcode", $(".zipcode", parent_element).val());
        form_data.append("region_id", $(".region_id", parent_element).val());
        form_data.append("country_id", $(".country_id", parent_element).val());
        form_data.append("description", $(".description", parent_element).val());
        form_data.append("contact_person", $(".contact_person", parent_element).val());
        form_data.append("your_position", $(".your_position", parent_element).val());
        form_data.append("phone_code", $(".phone_code", parent_element).val());
        form_data.append("hear_from_us", $(".hear_from_us", parent_element).val());
        form_data.append("g-recaptcha-response", $(".g-recaptcha-response", parent_element).val());

        if ($(".email", parent_element).length) {
            form_data.append("email", $(".email", parent_element).val());
        }

        if ($(".password", parent_element).length) {
            form_data.append("password", $(".password", parent_element).val());
        }

        form_data.append("mobile_number", $(".mobile_number", parent_element).val());
        form_data.append("telephone", $(".telephone", parent_element).val());
        form_data.append("company_website", $(".company_website", parent_element).val());
        form_data.append("vat_registration_number", $(".vat_registration_number", parent_element).val());
        form_data.append("company_registration_number", $(".company_registration_number", parent_element).val());
        form_data.append("_token", _token);

        //

        //
        // if($(".parent_user_code").length){
        //     obj.user_code=$(".parent_user_code").val();
        // }
        //
        // this_element.append(ajax_loader_img_func("10px"));
        //
        // console.log(obj);
        //

        $.ajax({
            url: url,
            type: 'POST',
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data);
                $('#message').html(data)
                this_element.removeAttr('disabled');
                this_element.children("img").remove();
                $("#save_form")[0].reset();
                $('.country_id').val(0);
                $('.region_id').val(0);
                $('#user_timezone_id').val(0);
            },
        });


        return false;
    });

    //
    // $("#formRegister").submit(function(e){
    //     var confirmPassword = $('#confirmPassword').val()
    //     var password = $('#password').val()
    //
    //     if (confirmPassword != password){
    //         $('#errorPassword').show();
    //         // $(this).focus();
    //         e.preventDefault(e);
    //     }
    //
    // });

});