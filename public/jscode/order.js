var calc_checkout_total;
var round_2_numbers;

$(function () {

    round_2_numbers=function(number){
        return Math.round(number * 100) / 100
        // return Math.round(number);
    };

    $(".add_to_cart").click(function () {
        var this_element = $(this);

        var obj={};
        obj._token=_token;
        obj.pro_id = this_element.data('pro_id');
        obj.pro_quantity = $(".order_quantity").val();
        obj.quantity_id=$(".selected_product_quantity").val();
        obj.selected_product_size=$(".selected_product_size").val();

        this_element.append(ajax_loader_img_func("15px"));

        $.ajax({
            url: base_url2+"/order/order_item",
            type: "POST",
            data: obj,
            success: function (data) {
                this_element.children("img").remove();

                var json_data=JSON.parse(data);

                if (typeof(json_data)!="undefined"&&json_data.done=="yes") {
                    $("#add_to_cart_modal").modal("show");
                }
                else if(typeof (json_data.add_to_old_bill)!="undefined"&&json_data.add_to_old_bill.length>0){
                    show_flash_message("info",json_data.add_to_old_bill);
                }
                else{
                    show_flash_message("info",json_data.done);
                }
            }
        });


    });

    $("#add_new_address_form").submit(function(){

        var this_element = $(this);
        var obj_data=new FormData($(this)[0]);

        $(".submit_form",this_element).attr("disabled","disabled");
        $(".submit_form",this_element).append(ajax_loader_img_func("15px"));

        $.ajax({
            url: base_url2+"/user/order/add_new_address",
            type: "POST",
            data: obj_data,
            cache:false,
            contentType: false,
            processData: false,
            success: function (data) {

                show_flash_message("info","Added and selected");

                var json_data=JSON.parse(data);
                $(".ajax_msgs").html(json_data.msg);

                //add new address to .select_address
                $(".select_address").append(json_data.new_address_html);

                $(".ajax_loader_class",$(".submit_form",this_element)).remove();
                $(".submit_form",this_element).removeAttr("disabled");

                $(".selected_user_address_class").last().attr("checked","checked");
                $(json_data.fire_class).last().change();

            }
        });



        return false;
    });

    $("body").on("click", ".remove_product_form_cart", function () {

        var confirm_del=confirm("are you sure?");

        if (confirm_del==false) {
            return false;
        }

        var obj={};

        obj.pro_id= $(this).data('proid');
        obj.selected_size= $(this).data('selected_size');
        obj.quantity_id= $(this).data('quantity_id');
        obj._token=_token;

        var this_element = $(this);
        this_element.html(ajax_loader_img_func("15px"));

        var price = $(this).data("price");


        $.ajax({
            url: base_url2+"/order/remove_item",
            type: "POST",
            data: obj,
            success: function (data) {

                //hide element
                this_element.parents(".product_cart_item").remove();

                calc_checkout_total();
            }
        });

        return false;
    });

    var xhr;

    $("body").on("change keyup",".change_row_quantity",function(){

        var row_quantity_element=$(".change_row_quantity",$(this).parents(".product_cart_item"));

        var products_cost=(parseFloat(row_quantity_element.val())*parseFloat(row_quantity_element.data("proprice")));

        //add packaging price
        var packaging_cost=0;


        $(".row_sum",row_quantity_element.parents(".product_cart_item")).html(round_2_numbers(products_cost+packaging_cost));

        if(xhr){
            xhr.abort();
        }


        var this_element = $(this);
        //this_element.append(ajax_loader_img_func("15px"));
        //this_element.attr("disabled","disabled");
        $(".show_msg",this_element.parents(".product_cart_item")).html(ajax_loader_img_func("15px"));

        var obj={};
        obj._token=_token;
        obj.pro_id=this_element.data("proid");
        obj.new_quantity=this_element.val();
        obj.selected_size= $(this).data('selected_size');
        obj.quantity_id= $(this).data('quantity_id');

        xhr=$.ajax({
            url: base_url2+"/order/change_pro_quantity",
            type: "POST",
            data: obj,
            success: function (data) {

                var json_data=JSON.parse(data);

                if(typeof (json_data.msg)!="undefined"){
                    $(".show_msg",this_element.parents(".product_cart_item")).html(json_data.msg);
                }

                calc_checkout_total();
//                this_element.removeAttr("disabled");
            }
        });


    });

    calc_checkout_total=function(){
        var checkout_total=0;

        $.each($(".row_sum"),function (i,v) {
            checkout_total=checkout_total+round_2_numbers(parseFloat($(this).html()));
        });

        //calculate coupon
        if($(".coupon_type").val()=="amount"){
            checkout_total=checkout_total-parseFloat($(".coupon_value").val());
        }
        else if($(".coupon_type").val()=="percentage"){
            checkout_total=checkout_total- ((checkout_total*parseFloat($(".coupon_value").val()))/100);
        }

        if(checkout_total<0){
            checkout_total=0;
        }

        $(".checkout_total").html(round_2_numbers(checkout_total));
    };

    $("body").on("click",".check_coupon_btn",function(){
        var this_element = $(this);

        if(xhr){
            this_element.children("img").remove();
            xhr.abort();
        }

        if($(".check_coupon_text").val().length==0){
            $(".show_coupons_msg").html("");

            return;
        }

        this_element.append(ajax_loader_img_func("15px"));

        var obj={};
        obj._token=_token;
        obj.check_coupon_text=$(".check_coupon_text").val();

        $(".coupon_type").val("");
        $(".coupon_value").val("");

        xhr=$.ajax({
            url: base_url2+"/order/check_coupon",
            type: "POST",
            data: obj,
            success: function (data) {

                var json_data=JSON.parse(data);

                if(typeof (json_data.msg)!="undefined"){
                    this_element.children("img").remove();

                    $(".show_coupons_msg").html(json_data.msg);

                    $(".coupon_type").val(json_data.coupon_type);
                    $(".coupon_value").val(json_data.coupon_value);
                }

                calc_checkout_total();
            }
        });


    });

    $("body").on("change keyup",".check_coupon_text",function(){
        $(".check_coupon_btn").click();
    });

    $("body").on("change select",".select_add_governorate",function(){

        if($(".do_not_run_select_add_governorate_code").length==0){
            var city_shipping_values=$(".city_shipping_values").val();
            city_shipping_values=JSON.parse(city_shipping_values);

            var selected_country=$(".select_add_country").val();
            var selected_city=$(this).val();

            if(city_shipping_values[selected_country+"_"+selected_city] ==undefined){
                show_flash_message("info"," this city hasn't available shipping methods");
                return false;
            }

            var seelcted_row=city_shipping_values[selected_country+"_"+selected_city][0];
            var city_shipping_city_rate=parseFloat($(".city_shipping_city_rate").val());

            $(".fetchr_value .price_value").html(" "+round_2_numbers(city_shipping_city_rate*parseFloat(seelcted_row.fetchr_shipping))+" ");
            $(".aramex_value .price_value").html(" "+round_2_numbers(city_shipping_city_rate*parseFloat(seelcted_row.aramex_shipping))+" ");
        }
    });

    $("body").on("change select",".select_add_outside_country_list_EGP",function(){

        var city_shipping_values=$(".city_shipping_values").val();
        city_shipping_values=JSON.parse(city_shipping_values);

        var selected_country=$(this).data("country");
        var selected_city=$(this).data("city");

        if(city_shipping_values[selected_country+"_"+selected_city] == undefined){
            show_flash_message("info"," this city hasn't available shipping methods");
            return false;
        }

        var seelcted_row=city_shipping_values[selected_country+"_"+selected_city][0];
        var city_shipping_city_rate=parseFloat($(".city_shipping_city_rate").val());

        $(".fetchr_value .price_value").html(" "+round_2_numbers(city_shipping_city_rate*parseFloat(seelcted_row.fetchr_shipping))+" ");
        $(".aramex_value .price_value").html(" "+round_2_numbers(city_shipping_city_rate*parseFloat(seelcted_row.aramex_shipping))+" ");
    });

    $(".select_add_outside_country_list_EGP:checked").change();

    $("body").on("change",".select_add_outside_country",function(){

        if($(".do_not_run_select_add_governorate_code").length==0){
            var country_shipping_values=$(".country_shipping_values").val();
            country_shipping_values=JSON.parse(country_shipping_values);

            var selected_country=$(this).val();

            if(country_shipping_values[selected_country] == undefined){
                show_flash_message("info"," this Country hasn't available shipping methods");
                return false;
            }

            var seelcted_row=country_shipping_values[selected_country][0];
            var city_shipping_city_rate=parseFloat($(".city_shipping_city_rate").val());

            $(".aramex_value .price_value").html(" "+round_2_numbers(city_shipping_city_rate*parseFloat(seelcted_row.aramex_shipping))+" ");
        }
    });

    $(".select_add_outside_country").change();

    if($(".payment_method_input").length>0){
        $(".payment_method_input").first().attr("checked","checked");
    }

    $("body").on("change",".select_add_outside_country_list_USD",function(){

        var country_shipping_values=$(".country_shipping_values").val();
        country_shipping_values=JSON.parse(country_shipping_values);

        var selected_country=$(this).data("country");

        if(country_shipping_values[selected_country] == undefined){
            show_flash_message("info"," this Country hasn't available shipping methods");
            return false;
        }

        var seelcted_row=country_shipping_values[selected_country][0];
        var city_shipping_city_rate=parseFloat($(".city_shipping_city_rate").val());

        $(".aramex_value .price_value").html(" "+round_2_numbers(city_shipping_city_rate*parseFloat(seelcted_row.aramex_shipping))+" ");
    });

    $(".select_add_outside_country_list_USD:checked").change();



});