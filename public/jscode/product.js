$(function(){

    $("#share").jsSocials({
        shares: [ "twitter", "facebook", "linkedin", "pinterest", "whatsapp"]
    });

    $(".select_color_btn").click(function () {

        var pro_available_sizes=$(this).data("pro_available_sizes");
        $("option",$(".pro_available_sizes")).remove();

        $.each(pro_available_sizes,function (index,val) {
            $(".pro_available_sizes").append("<option value='"+val+"'>"+val+"</option>");
        });

        $(".pro_available_sizes option").first().attr("selected","selected");
        $(".pro_available_sizes").change();


        var pro_price=parseFloat($(this).data("pro_price"));
        var pro_discount_amount=parseFloat($(this).data("pro_discount_amount"));

        $(".load_price .price-old").hide();

        if(pro_discount_amount>0){
            $(".load_price .price-old").show();
            $(".price_val",$(".load_price .price-old")).html(pro_price);
        }

        $(".price_val",$(".load_price .price-new")).html(pro_price-pro_discount_amount);


        $(".selected_product_quantity").val($(this).data("pro_quantity_id"));
        $(".selected_color_name").html($(this).data("pro_color"));
    });

    $(".select_color_btn").first().click();

    $(".pro_available_sizes").change(function () {


        $(".selected_product_size").val($(this).val());
        $(".selected_size_name").html($(this).val());

    });

    $(".pro_available_sizes").change();

});