var base_url2;
var base_url;
var _token;
var ajax_loader_img;
var ajax_loader_img_func;
var lang_url_class;
var call_datatable;
var call_order;
var check_valid;
var show_flash_message;
var timzoom_json;


$(function(){

    base_url2 = $(".url_class").val();
    base_url = base_url2 + "/public/";
    _token = $(".csrf_input_class").val();
    lang_url_class = $(".lang_url_class").val()+"/";
    ajax_loader_img="<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' width='20'>";
    ajax_loader_img_func=function(img_width){
        return "<img src='" + base_url + "img/ajax-loader.gif' class='ajax_loader_class' style='width:"+img_width+";height:"+img_width+";'>";
    };

    $(".collapse.in").parents(".panel").children(".panel-heading").removeClass("collapsed");


    check_valid=function(parent_element){

        var valid=true;

        $.each($(".check_valid",parent_element),function () {
            if(!$(this)[0].reportValidity()){
                valid=false;
                return false;
            }
        });

        return valid;
    };

    $("body").on("click",".hide_alert_fixed",function(){

        var parent_div=$(this).parents(".alert-fixed");

        parent_div.removeClass("alert-fixed-show");
        parent_div.removeClass("alert-success");
        parent_div.removeClass("alert-danger");
        parent_div.removeClass("alert-info");
        parent_div.removeClass("alert-warning");

        return false;
    });

    show_flash_message=function (type,get_flash_message){

        var pure_msg = '<p style="font-size: 20px;color: #FFF;">' + get_flash_message + '</p>';

        toastr.options = {
            "closeButton": 1,
            "debug": !1,
            "newestOnTop": !0,
            "progressBar": 1,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": !1,
            "onclick": null,
            "showDuration": "0",
            "hideDuration": "0",
            "timeOut": "10000",
            "extendedTimeOut": "10000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        toastr[type](pure_msg, '', toastr.options);

    }

    if ($('.get_flash_message').length > 0) {
        var get_flash_message = $('.get_flash_message').val();

        if(get_flash_message.length>0){
            var str = $('.get_flash_message').val();
            var type = 'success';
            var pure_msg = '<p style="font-size: 20px;color: #fff;">' + $(str).html() + '</p>';
            if (str.indexOf('alert-danger') > -1) {
                type = 'warning'
            } else if (str.indexOf('alert-warning') > -1) {
                type = 'warning'
            } else if (str.indexOf('alert-info') > -1) {
                type = 'info'
            }

            show_flash_message(type,pure_msg);
        }

    }

    if($('.select_2_class').length){
        $('.select_2_class').select2();
    }


    $(".load_broadcast_at_view").click(function(){

        var this_element=$(this);
        this_element.append(ajax_loader_img_func("10px"));

        var obj={};
        obj._token=_token;

        var loaded_data;

        $.ajax({
            url:this_element.attr("data-href"),
            type:"GET",
            data:obj,
            success:function(data){
                $(".load_view_modal").modal("show");
                $(".load_view_here").html(data);
                $(".for_print").html(data);
                this_element.children(".ajax_loader_class").remove();

                loaded_data=data;

                $(".collapse.in").parents(".panel").children(".panel-heading").removeClass("collapsed");
            }
        }).then(function(){
            $(".ckeditor",$(".load_view_here")).ckeditor();
        });

        return false;
    });

    if($(".rate_it").length){
        $(".rate_it").rateit();
    }



    $(".load_stock_item_at_view").click(function(){

        var this_element=$(this);
        this_element.append(ajax_loader_img_func("10px"));

        var obj={};
        obj._token=_token;

        var loaded_data;


        $.ajax({
            url:this_element.attr("data-href"),
            type:"GET",
            data:obj,
            success:function(data){
                $(".load_view_modal").modal("show");
                $(".load_view_here").html(data);
                $(".for_print").html(data);
                this_element.children(".ajax_loader_class").remove();

                loaded_data=data;

                $(".collapse.in").parents(".panel").children(".panel-heading").removeClass("collapsed");
            }
        }).then(function(){
            $(".ckeditor",$(".load_view_here")).ckeditor();
        });

        return false;
    });




});