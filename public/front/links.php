<link rel="icon" href="images/icon.png" type="image/png" sizes="16x16">
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox-plus.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.ez-plus.css"/>
<link rel="stylesheet" href="css/animate.css">
<link href="css/animate.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.min.css" integrity="" crossorigin="anonymous">
<link href="css/bootstrap-better-nav.css" rel="stylesheet">
<link rel="stylesheet" href="css/styles.css" >
<link href="web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">
