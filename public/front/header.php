<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        T-Shirt
    </title>


    
<?php
include_once "links.php"; // this will include a.php
?>
</head>


<body>
<!--=====================================================-->
<!--=====================================================-->

<div class="container-fluid">
<nav class="navbar navbar-expand-sm   navbar-light navexpand">
    <a class="navbar-brand" href="#">
        <img src="images/logo.png" class="img-fluid">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="#">
                    How it works
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    Sell everything
                </a>
            </li>
            <li class="nav-item dropdown dmenu">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Sell everywhere
                </a>
                <div class="dropdown-menu sm-menu">
                    <a class="dropdown-item" href="#">service2</a>
                    <a class="dropdown-item" href="#">service 2</a>
                    <a class="dropdown-item" href="#">service 3</a>
                </div>
            </li>


        </ul>
        <div class="social-part">
            <ul class="list-unstyled">
            <li class="">
                <a class="" href="#">
                    Start Designing
                </a>
            </li>
            <li class="">
                <a class="login" href="#">
                    Login
                </a>
            </li>
            </ul>
        </div>
    </div>
</nav>
</div>

<!--=============================end of navbar =======================================-->
