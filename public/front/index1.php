<?php
include_once "header.php"; // this will include a.php
?>

<!--============================= start div of background ============================-->
<div class="container-fluid">
    <div class="firstbackground">
        <div class="row">
            <div class="col-md-6">
                <h1>
                    Find Something Made for You.
                </h1>
                <div class="search">
                    <form>
                        <i class="fas fa-search"></i>
                        <input type="text" placeholder="Favorite animal?" class="form-control">
                        <button type="submit" class="btn btn-danger">
                            Go
                        </button>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="startdesign">
        <div class="title">
            <strong>
                Design your own products?
            </strong>
            <span>
                Teespring makes it easy to create, sell, and order custom products.
            </span>
            <a href=" " class="btn btn-danger">
                Start Designing
            </a>
        </div>

    </div>
</div>



<!--============================== end div of background ===============================-->


<!--=========================== start div of recommended product ======================-->

<div class="container-fluid">
    <div class="recommend">
        <h2>
            Recommendations For You
        </h2>
    </div>

</div>
<div class="container">
    <div class="row">

        <?php for ($i = 1; $i <= 4; $i++) {  ?>
        <div class="col-md-3">

            <div class="product">
                <a href="" title="test">
                    <img src="images/560.jpg" class="img-fluid">
                    <div class="detail">
                        <p class="title">
                            Feminist Sweatshirts to Support Scholars
                        </p>
                        <p class="desc">
                            by Feminists Support UMN Scholars
                        </p>
                    </div>
                </a>
            </div>

        </div>
		<?php   }; ?>

    </div>
</div>


<!--=========================== end div of recommended product =========================-->


<!--=========================== start div of packs product ======================-->

<div class="container-fluid">
    <div class="recommend">
        <h2>
            Staff Picks
        </h2>
    </div>

</div>
<div class="container">
    <div class="row">
		<?php for ($i = 1; $i <= 4; $i++) {  ?>
        <div class="col-md-3">

            <div class="product">
                <a href="" title="test">
                    <img src="images/560.jpg" class="img-fluid">
                    <div class="detail">
                        <p class="title">
                            Feminist Sweatshirts to Support Scholars
                        </p>
                        <p class="desc">
                            by Feminists Support UMN Scholars
                        </p>
                    </div>
                </a>
            </div>

        </div>
		<?php   }; ?>
    </div>
</div>


<!--=========================== end div of packs product =========================-->

<!--========================= start div of firstbanner =====================-->

<div class="container-fluid">
    <div class="bannerone">
        <a href="" class="">
            <img src="images/leggings_editorial_desktop.jpg" class="img-fluid one">
            <img src="images/leggings_editorial_mobile.jpg" class="img-fluid two">
        </a>
    </div>
</div>


<!--============================= end div of firstbanner ==============-->


<div class="container-fluid">
    <div class="recommend">
        <h2>
            Featured Collections

        </h2>
    </div>

</div>
<div class="container-fluid">
    <div class="desimg">
    <div class="row">
        <div class="col-md-4">
            <a href="">
                <div class="">
                    <img src="images/Creator-Landing-Page.png" class="img-fluid">
                </div>
            </a>
        </div>

        <div class="col-md-4">
            <a href="">
                <div class="">
                    <img src="images/DesignYourOwn.png" class="img-fluid">
                </div>
            </a>
        </div>

        <div class="col-md-4">
            <a href="">
                <div class="">
                    <img src="images/verified_donations.jpg" class="img-fluid">
                </div>
            </a>
        </div>

    </div>
    </div>
</div>

<!--=================================== end of img collect ====================================-->

<!--===================================== start div of mobile ==========================-->
<div class="container-fluid">
    <div class="recommend">
        <h2>
            Protect your iPhone
            <span>
            <a href="">
                Shop collection
            </a>
        </span>
        </h2>

    </div>

</div>
<div class="container">
    <div class="row">
<?php for ($i = 1; $i <= 4; $i++) {  ?>
        <div class="col-md-3">

            <div class="product">
                <a href="" title="test">
                    <img src="images/560.jpg" class="img-fluid">
                    <div class="detail">
                        <p class="title">
                            Feminist Sweatshirts to Support Scholars
                        </p>
                        <p class="desc">
                            by Feminists Support UMN Scholars
                        </p>
                    </div>
                </a>
            </div>

        </div>
<?php  } ?>
    </div>
</div>


<!--======================================== end div of mobile ===========================-->

<!--============================= end div of firstbanner ==============-->


<div class="container-fluid">
    <div class="recommend">
        <h2>
            Featured Creators

        </h2>
    </div>

</div>
<div class="container-fluid">
    <div class="desimg">
        <div class="row">
            <div class="col-md-4">
                <a href="">
                    <div class="">
                        <img src="images/DuckStudios.png" class="img-fluid">
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a href="">
                    <div class="">
                        <img src="images/RECKFUL+(1).png" class="img-fluid">
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a href="">
                    <div class="">
                        <img src="images/PHIL.png" class="img-fluid">
                    </div>
                </a>
            </div>

        </div>
    </div>
</div>

<!--=================================== end of img collect ====================================-->

<!--===================================== start div of mobile ==========================-->
<div class="container-fluid">
    <div class="recommend">
        <h2>
            Leggings

            <span>
            <a href="">
                Shop collection
            </a>
        </span>
        </h2>

    </div>

</div>
<div class="container">
    <div class="row">
<?php for ($i = 1; $i <= 4; $i++) {  ?>
        <div class="col-md-3">

            <div class="product">
                <a href="" title="test">
                    <img src="images/560.jpg" class="img-fluid">
                    <div class="detail">
                        <p class="title">
                            Feminist Sweatshirts to Support Scholars
                        </p>
                        <p class="desc">
                            by Feminists Support UMN Scholars
                        </p>
                    </div>
                </a>
            </div>

        </div>
<?php   } ?>
    </div>
</div>


<!--======================================== end div of mobile ===========================-->
<!--========================= start div of firstbanner =====================-->

<div class="container-fluid">
    <div class="bannerone">
        <a href="" class="">
            <img src="images/Teespring.png" class="img-fluid one">
            <img src="images/Teespring2.png" class="img-fluid two">
        </a>
    </div>
</div>


<!--============================= end div of firstbanner ==============-->

<!--=========================== start div of video ====================-->

<div class="container-fluid">
    <div class="bannerone video">
        <div class="row">
            <div class="col-md-6">
                <div data-video="be0aFAqIhXk" id="video">
                    <img src="images/video.jpg" alt="Use your own screenshot.">
                </div>
            </div>

            <div class="col-md-6">
               <div class="last_vid">
                   <h2>
                       Teespring is the free and easy way to bring your ideas to life.
                   </h2>
                   <p>
                       Design your shirt, set a price, add a goal and start selling.
                       Teespring handles the rest - production, shipping, and customer service - and you keep the profit!
                   </p>
                   <div class="">
                       <a href="" class="btn btn-danger">
                           Get started now!
                       </a>
                   </div>
               </div>
            </div>
        </div>

    </div>
</div>

<!--=========================== end div of video =========================-->
<!--==================================================script footers ===============================-->
















<?php
include_once "footer.php"; // this will include a.php
?>