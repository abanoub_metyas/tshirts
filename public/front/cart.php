<?php
include_once "header.php"; // this will include a.php
?>

<!--===================================end div of content ====================================================-->
<div class="container pagepro">
    <div class="row">
        <div class="col-md-12 col-12">
           <h1 class="weight">
               Use Gift Certificate  (0.00kg)
           </h1>
        </div>

    </div>
</div>
<!--==============================================-->


<div class="container-fluid tablesize">
    <hr>



    <div class="card">
        <table class="table table-hover shopping-cart-wrap">
            <thead class="text-muted">
            <tr>
                <th scope="col">Product</th>
                <th scope="col" width="120">Model</th>
                <th scope="col" width="120">Quantity</th>
                <th scope="col" width="120">Price</th>
                <th scope="col" width="200" class="text-center">Action</th>
                <th scope="col" width="120">total</th>
            </tr>
            </thead>
            <tbody>

            	<?php for ($i = 1; $i <= 4; $i++) {  ?>
            <tr>
                <td>
                    <figure class="media">
                        <div class="img-wrap"><a href=""> <img src="images/pro_one.png" class="img-thumbnail img-sm"></a></div>
                        <figcaption class="media-body">
                            <h6 class="title text-truncate">
                                <a href=""> Product name goes here</a>
                            </h6>
                            <dl class="param param-inline small">
                                <dt>Size: </dt>
                                <dd>XXL</dd>
                            </dl>
                            <dl class="param param-inline small">
                                <dt>Color: </dt>
                                <dd>Orange color</dd>
                            </dl>
                        </figcaption>
                    </figure>
                </td>
                <td>
                    be-stranger-sh

                </td>
                <td>
                    <input type="number" class="form-control">

                </td>
                <td>
                    <div class="price-wrap">
                        <var class="price">USD 145</var>
                        <small class="text-muted">(USD5 each)</small>
                    </div> <!-- price-wrap .// -->
                </td>
                <td class="text-center">
                    <a href="" class="btn btn-outline-danger"> × Remove</a>
                </td>
                <td>
                    <div class="price-wrap">
                        <var class="price">USD 145</var>
                        <small class="text-muted">(USD5 each)</small>
                    </div> <!-- price-wrap .// -->
                </td>
            </tr>
<?php  } ;?>
            </tbody>
        </table>
    </div> <!-- card.// -->

</div>
<!--container end.//-->

<!--===============================================================-->




<div class="container-fluid pagepro">
    <div class="row">
        <div class="col-md-12 col-12">
            <h2 class="like">
                WHAT WOULD YOU LIKE TO DO NEXT?
            </h2>
            <p class="likep">
                Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.

            </p>
        </div>

    </div>
</div>

<!--============================== accordion tabs =================================================-->

<div class="container-fluid tablesize">
    <div id="accordion" role="tablist">
        <div class="card">
            <div class="card-header" role="tab" id="headingOne">
                <h5 class="mb-0">
                    <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        USE COUPON CODE
                        <i class="fas fa-chevron-down"></i>
                    </a>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <p>
                        Enter your coupon here
                    </p>
                    <div class="row">
                        <div class="col-md-10">
                            <input class="form-control" type="text" placeholder="Enter Your Coupon Here">
                        </div>
                        <div class="col-md-2">
                            <a href="" class="btn btn-primary coupon">
                                Apply Coupon
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" role="tab" id="headingTwo">
                <h5 class="mb-0">
                    <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        ESTIMATE SHIPPING & TAXES
                        <i class="fas fa-chevron-down"></i>
                    </a>
                </h5>
            </div>
            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    <p>
                        Enter your destination to get a shipping estimate.
                    </p>

                        <form method="" action="">
                            <div class="row">
                                <div class="col-md-2 titleoption">
                                    <label class="control-label" for="input-country">Country</label>
                                </div>
                                <div class="col-md-10">
                                    <select id="input-country" class="form-control">
                                        <option>
                                            ......
                                        </option>
                                        <option>
                                            one
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 titleoption">
                                    <label class="control-label" for="input-countrey">region / state </label>
                                </div>
                                <div class="col-md-10">
                                    <select id="input-countrey" class="form-control">
                                        <option>
                                            ......
                                        </option>
                                        <option>
                                            one
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2 titleoption">
                                    <label class="control-label" for="input-countreey">Post Code </label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" id="input-countreey" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="getcotes">
                                    <a href="" class="btn btn-primary coupon">
                                        Get Quotes
                                    </a>
                                </div>
                            </div>
                        </form>

                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                    <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        USE GIFT CERTIFICATE
                        <i class="fas fa-chevron-down"></i>
                    </a>
                </h5>
            </div>
            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    <p>
                        Enter your gift certificate code here
                    </p>
                    <div class="row">
                        <div class="col-md-10">
                            <input class="form-control" type="text" placeholder="Enter Your Gift Certificate Code Here">
                        </div>
                        <div class="col-md-2">
                            <a href="" class="btn btn-primary coupon">
                                Apply Gift Certificate
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!--===================================================-->


    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4 ">
            <table class="table table-bordered lasttable">
                <tbody><tr>
                    <td class="text-right"><strong>Sub-Total:</strong></td>
                    <td class="text-right"><span class="text-price">EGP700.00ج.م</span></td>
                </tr>
                <tr>
                    <td class="text-right"><strong>Total:</strong></td>
                    <td class="text-right"><span class="text-price">EGP700.00ج.م</span></td>
                </tr>
                </tbody></table>
        </div>

    </div>


    <div class="row check">
        <div class="col-md-8">
            <a class="btn btn-light testw">
                Continue Shopping
            </a>
        </div>
        <div class="col-md-4 ">
            <div class="text-right">
            <a href="" class="btn btn-primary coupon">
                Check Out
            </a>
            </div>
        </div>

    </div>
</div>


<!--=========================== end div of video =========================-->




<?php
include_once "footer.php"; // this will include a.php
?>