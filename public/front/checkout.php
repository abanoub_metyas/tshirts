<?php
include_once "header.php"; // this will include a.php
?>

<div class="container-fluid tablesize">
	<div class="alert alert-warning" role="alert">
  Your Order is Reserved
</div>
	<div class="row">
		<div class="col-md-6 col-12">
			 <h1 class="checkout">
               Checkout
               <i class="fab fa-cc-visa visa"></i>
           </h1>



           <!-- ==================================== -->


             <form action="" method="">
                            <div class="form-group">
                                <div class="col-md-12"><strong>Contact Info:</strong></div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" placeholder="admin@mail.com" name="Contact" value="" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12"><strong>Shipping Info:</strong></div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control ship" placeholder="Full name" name="full_name" value="" />
                                </div>

                                 <div class="col-md-10">
                                    <input type="text" class="form-control ship" placeholder="Street Address" name="street_address" value="" />
                                </div>

                                 <div class="col-md-10">
                                    <input type="text" class="form-control ship" placeholder="Apt/Suite" name="other" value="" />
                                </div>
                                <div class="form-group row">
	                                <div class="col-md-5 col-6">
	                                
	                                    <input type="text" name="state" class="form-control together" value="" placeholder="Cairo" />
	                                </div>
	                                
	                                <div class="col-md-5 col-6">
	                                  
	                                    <input type="text" name="country" placeholder="Mansoura" class="form-control tog" value="" />
	                                </div>
                            	</div>


                                <div class="form-group row contry">
	                                <div class="col-md-5 col-6">
	                                
	                                    <input type="text" name="postal" class=" ship form-control together" value="" placeholder="Postal code" />
	                                </div>
                                
	                                <div class="col-md-5 col-6">
	                                  
	                                    <select class="form-control ship tog">
	                                    	<option>
	                                    		Egypt
	                                    	</option>
	                                    </select> 
	                                </div>
	                            </div>
								<div class="col-md-10">
                                    <input type="text" class="form-control" placeholder="Phone Number" name="Phone" value="" />

                                </div>
                                <div class="col-md-10">
                                    <div class="alert alert-info" role="alert">
										<input type="checkbox" name="remember">
										Save Address Info
									</div>
                                </div>

                                <article class="cards">
									<div class="card-body col-md-11">

									<ul class="nav bg-light nav-pills rounded nav-fill mb-3" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="pill" href="#nav-tab-card">
											<i class="fa fa-credit-card"></i> Credit Card</a></li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="pill" href="#nav-tab-paypal">
											<i class="fab fa-paypal"></i>  Paypal</a></li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="pill" href="#nav-tab-bank">
											<i class="fa fa-university"></i>  Bank Transfer</a></li>
									</ul>

									<div class="tab-content">
									<div class="tab-pane fade show active" id="nav-tab-card">
										<p class="alert alert-success">Some text success or error</p>
										<form role="form">
										<div class="form-group">
											<label for="username">Full name (on the card)</label>
											<input type="text" class="form-control" name="username" placeholder="" required="">
										</div> <!-- form-group.// -->

										<div class="form-group">
											<label for="cardNumber">Card number</label>
											<div class="input-group">
												<input type="text" class="form-control" name="cardNumber" placeholder="">
												<div class="input-group-append">
													<span class="input-group-text text-muted">
														<i class="fab fa-cc-visa"></i>   <i class="fab fa-cc-amex"></i>   
														<i class="fab fa-cc-mastercard"></i> 
													</span>
												</div>
											</div>
										</div> <!-- form-group.// -->

										<div class="row">
										    <div class="col-sm-8">
										        <div class="form-group">
										            <label><span class="hidden-xs">Expiration</span> </label>
										        	<div class="input-group">
										        		<input type="number" class="form-control" placeholder="MM" name="">
										        		<span style="width:10%; text-align: center"> / </span>
											            <input type="number" class="form-control" placeholder="YY" name="">
										        	</div>
										        </div>
										    </div>
										    <div class="col-sm-4">
										        <div class="form-group">
										            <label data-toggle="tooltip" title="" data-original-title="3 digits code on back side of the card">CVV <i class="fa fa-question-circle"></i></label>
										            <input type="number" class="form-control" required="">
										        </div> <!-- form-group.// -->
										    </div>
										</div> <!-- row.// -->
										
										</form>
									</div> <!-- tab-pane.// -->
										<div class="tab-pane fade" id="nav-tab-paypal">
													<p>Paypal is easiest way to pay online</p>
													<p>
													<button type="button" class="btn btn-primary"> <i class="fab fa-paypal"></i> Log in my Paypal </button>
													</p>
													<p><strong>Note:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
														tempor incididunt ut labore et dolore magna aliqua. </p>
										</div>
										<div class="tab-pane fade" id="nav-tab-bank">
											<p>Bank accaunt details</p>
											<dl class="param">
											  <dt>BANK: </dt>
											  <dd> THE WORLD BANK</dd>
											</dl>
											<dl class="param">
											  <dt>Accaunt number: </dt>
											  <dd> 12345678912345</dd>
											</dl>
											<dl class="param">
											  <dt>IBAN: </dt>
											  <dd> 123456789</dd>
											</dl>
											<p><strong>Note:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. </p>
										</div> <!-- tab-pane.// -->
										</div> <!-- tab-content .// -->

							</div> <!-- card-body.// -->
							</article> 
                                
                              <div class="col-md-10">
                                    <button class="subscribe btn btn-primary btn-block" type="button"> Place Your Order  </button>
                                </div>
                            </div>
                            </form>
                            </div>

                            <!-- ===================================== -->
					<div class="col-md-6">
                    <!--SHIPPING METHOD-->
                    <div class="panel panel-default">
                        <div class="panel-heading text-left"><h4>Current Cart</h4></div>
                        <div class="panel-body">
                           <table class="table borderless">
    						<thead>
                                <tr>
        							<td><strong>Your Cart: # item</strong></td>
        							<td></td>
        							<td></td>
        							<td></td>
        							<td></td>
                                </tr>
    						</thead>
    						<tbody>
    							<!-- foreach ($order->lineItems as $line) or some such thing here -->
    							<tr>
    								<td class="col-md-3">
    								    <div class="media">
    								         <a class="thumbnail pull-left" href="#"> <img class="media-object" src="images/pro_2.png" style="width: 72px; height: 72px;"> </a>
    								         <div class="media-body">
    								             <h5 class="media-heading"> Product Name</h5>
    								             <h5 class="media-heading"> Color</h5>
    								         </div>
    								    </div>
    								</td>
    								<td class="text-center">$10.99</td>
    								<td class="text-center">1</td>
    								<td class="text-right">$32.99</td>
    								<td class="text-right"><button type="button" class="btn btn-danger">Remove</button></td>
    							</tr>
                                <tr>
    								<td class="col-md-3">
    								    <div class="media">
    								         <a class="thumbnail pull-left" href="#"> <img class="media-object" src="images/pro_one.png" style="width: 72px; height: 72px;"> </a>
    								         <div class="media-body">
    								             <h5 class="media-heading"> Product Name</h5>
    								             <h5 class="media-heading"> Color</h5>
    								         </div>
    								    </div>
    								</td>
    								<td class="text-center">$10.99</td>
    								<td class="text-center">1</td>
    								<td class="text-right"> $32.99</td>
    								<td class="text-right"><button type="button" class="btn btn-danger">Remove</button></td>
    							</tr>
    						</tbody>
    					</table> 
                        </div>
                    </div>
                    <!--SHIPPING METHOD END-->
                </div>
                </div>
                </div>

                            

                            <!-- =============end ===================================== -->






	</div>
</div>


















<?php
include_once "footer.php"; // this will include a.php
?>







