<?php

Route::group([
    'middleware' => 'check_service_operator',
    'prefix'=>'service_operator',
    'namespace'=>'service_operator'
], function () {

    Route::get('dashboard', 'dashboard@index');

    Route::get('orders/show_all/{user_id?}','orders@index');
    Route::get('orders/bill_orders/{bill_id}','orders@bill_orders');

    Route::get('orders/edit_bill_orders','orders@edit_bill_orders');
    Route::post('orders/edit_bill_orders','orders@edit_bill_orders');
    Route::post('orders/add_single_product','orders@add_single_product');

    Route::get('orders/change_status','orders@change_status');
    Route::post('orders/change_status','orders@change_status');
    Route::post('orders/remove_order','orders@remove_order');

    Route::get('orders/send_to_printing_operator','orders@send_to_printing_operator');
    Route::post('orders/send_to_printing_operator','orders@send_to_printing_operator');

    Route::get('return_orders/show_all','return_orders@show_all');
    Route::get('return_orders/select_product','return_orders@select_product');
    Route::get('return_orders/save/{pro_id}/{id?}','return_orders@save');
    Route::post('return_orders/save/{pro_id}/{id?}','return_orders@save');

    Route::get('printed_tshirts/show_all','printed_tshirts@show_all');
    Route::get('printed_tshirts/select_product','printed_tshirts@select_product');
    Route::get('printed_tshirts/save/{pro_id}/{id?}','printed_tshirts@save');
    Route::post('printed_tshirts/save/{pro_id}/{id?}','printed_tshirts@save');


    Route::get('orders/send_to_fetchr','orders@send_to_fetchr');
    Route::get('orders/cancel_order_fetchr','orders@cancel_order_fetchr');

    Route::get('orders/send_to_aramex','orders@send_to_aramex');


});

