<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Notifications\InvoicePaid;

use Illuminate\Notifications\Notifiable;

Route::get('/', function () {

    $users = \App\User::all()->first();


    \Illuminate\Support\Facades\Notification::send($users, new InvoicePaid($obj = null));
    return view('welcome');
});


Route::get('login/google', 'Auth\LoginController@redirectToProvider');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('login/google', function(){
    return  redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
