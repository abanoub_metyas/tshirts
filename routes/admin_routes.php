<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

date_default_timezone_set("Africa/Cairo");




Route::group(['middleware' => 'check_admin'], function () {


    // Start Admin Category Routing
    Route::get('/admin/category/save_cat/{cat_type?}','admin\category@save_cat');
    Route::get('/admin/category/save_cat/{cat_type}/{cat_id?}','admin\category@save_cat');
    Route::post('/admin/category/save_cat/{cat_type}/{cat_id?}','admin\category@save_cat');

    Route::post('/admin/category/delete_cat','admin\category@delete_cat');


    Route::get('/admin/category/{cat_type?}/{parent_id?}','admin\category@index')->where('parent_id', '[0-9]+');


    // End Admin Category Routing

    // Start Admin Product Routing
    Route::get('/admin/product/{cat_id?}','admin\product@index')->where('cat_id', '[0-9]+');
    Route::get('/admin/product/save_pro/{pro_id?}','admin\product@save_pro');
    Route::post('/admin/product/save_pro/{pro_id?}','admin\product@save_pro');

    Route::get('/admin/product/show_product_reviews','admin\product@show_product_reviews');
    // End Admin Product Routing


    #region coupon
    Route::get('/admin/coupons','admin\coupons@index');
    Route::get('/admin/coupons/save_coupon/{coupon_id?}','admin\coupons@save_coupon');
    Route::post('/admin/coupons/save_coupon/{coupon_id?}','admin\coupons@save_coupon');
    Route::post('/admin/coupons/remove_coupon','admin\coupons@remove_coupon');
    #endregion

    #region materials
    Route::get('/admin/materials','admin\materials@index');
    Route::get('/admin/materials/save_material/{material_id?}','admin\materials@save_material');
    Route::post('/admin/materials/save_material/{material_id?}','admin\materials@save_material');
    Route::post('/admin/materials/remove_material','admin\materials@remove_material');
    Route::get('/admin/materials/material_usage','admin\materials@material_usage');
    #endregion

    #region country
    Route::get("/admin/countries/show_all",'admin\country@country_list');

    Route::get("/admin/countries/save/{country_id?}",'admin\country@save_country');
    Route::post("/admin/countries/save/{country_id?}",'admin\country@save_country');

    Route::get("/admin/cities/show_all",'admin\city@city_list');

    Route::get("/admin/cities/save/{city_id?}",'admin\city@save_city');
    Route::post("/admin/cities/save/{city_id?}",'admin\city@save_city');
    #endregion



    //edit_content
    Route::get('/admin/show_methods','admin\edit_content@show_methods');
    Route::get('admin/edit_content/{lang_id}/{slug}','admin\edit_content@check_function');
    Route::post('admin/edit_content/{lang_id}/{slug}','admin\edit_content@check_function');
    //END edit_content


    Route::get('admin/dashboard', 'admin\dashboard@index');

    // Start notifications

    Route::get('/admin/notifications/show_all','admin\notifications@index');
    Route::post('/admin/notifications/delete_notification','admin\notifications@delete_notification');

    // End notifications





    // Start Admin Langs Routing

    Route::get('/admin/langs','admin\langs@index');
    Route::get('/admin/langs/save_lang/{lang_id?}','admin\langs@save_lang');
    Route::post('/admin/langs/save_lang/{lang_id?}','admin\langs@save_lang');
    Route::post('/admin/langs/delete_lang','admin\langs@delete_lang');

    // End Admin Langs Routing


    // Start Admin pages Routing
    Route::get('/admin/pages/save_page/{page_type?}/{page_id?}','admin\pages@save_page');
    Route::post('/admin/pages/save_page/{page_type?}/{page_id?}','admin\pages@save_page');
    Route::post('/admin/pages/check_validation_for_save_page/{page_id?}','admin\pages@check_validation_for_save_page');
    Route::post('/admin/pages/remove_page','admin\pages@remove_page');

    Route::get('/admin/pages/show_all/{page_type?}/{cat_id?}','admin\pages@index');
    // End Admin pages Routing



    // Start Admin users Routing
    Route::get('admin/users/get_all_users', 'admin\users@get_all_users');

    Route::get('admin/users/save/{user_id?}', 'admin\users@save_user');
    Route::post('admin/users/save/{user_id?}', 'admin\users@save_user');

    Route::get('admin/users/show_profit_and_loss', 'admin\users@show_profit_and_loss');

    Route::get('admin/users/assign_permission/{user_id}', 'admin\users@assign_permission');
    Route::post('admin/users/assign_permission/{user_id}', 'admin\users@assign_permission');

    Route::post('/admin/users/remove_admin','admin\users@remove_admin');

    // End Admin users Routing


    // Start Admin support_messages Routing

    Route::get('/admin/support_messages','admin\support_messages@index');
    Route::post('/admin/delete_support_messages','admin\support_messages@remove_msg');

    // End Admin support_messages Routing


    // Start Admin subscribe Routing

    Route::get('/admin/subscribe','admin\subscribe@index');
    Route::post('/admin/subscribe/send_custom_email','admin\subscribe@send_custom_email');
    Route::post('/admin/subscribe/send_all_subscribers_email','admin\subscribe@send_all_subscribers_email');
    Route::get('/admin/subscribe/stop','admin\subscribe@stop');
    Route::get('/admin/subscribe/pause','admin\subscribe@pause');
    Route::get('/admin/subscribe/resume','admin\subscribe@resume');

    Route::get('/admin/subscribe/save_email','admin\subscribe@save_email');
    Route::post('/admin/subscribe/save_email','admin\subscribe@save_email');

    Route::get('/admin/subscribe/email_settings','admin\subscribe@email_settings');
    Route::post('/admin/subscribe/email_settings','admin\subscribe@email_settings');
    Route::get('/admin/subscribe/export_subscribe','admin\subscribe@export_subscribe');

    Route::post('/admin/subscribe/remove_email','admin\subscribe@remove_email');


    // End Admin subscribe Routing



    //manage ads
    Route::get("/admin/ads",'admin\ads@index');
    Route::get("/admin/ads/save_ad/{ad_id?}",'admin\ads@save_ad');
    Route::post("/admin/ads/save_ad/{ad_id?}",'admin\ads@save_ad');
    Route::post("/admin/ads/remove_ads",'admin\ads@remove_ads');
    //END manage ads

    //uploader
    Route::get('/admin/uploader','admin\uploader@index');
    Route::post('/upload_files','admin\uploader@load_files');

    //END uploader


    Route::get('/admin/settings','admin\settings@index');


});


// Password Reset Routes...
$this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
$this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
$this->post('password/reset', 'Auth\PasswordController@reset');

//Route::auth();
//
//Route::get('/home', 'HomeController@index');
