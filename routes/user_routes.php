<?php

Route::group([
    'middleware' => 'check_user',
    'prefix'=>'user',
    'namespace'=>'user',
], function () {

    Route::get('dashboard', 'dashboard@index');
    Route::get('orders', 'order@show_user_orders');

    Route::post('edit_data', 'dashboard@edit_data');
    Route::post('change_password', 'dashboard@change_password');

    Route::get('checkout', 'order@checkout_orders');
    Route::post('checkout','order@post_bill');


    Route::post('order/add_new_address', 'order@add_new_address');



});

