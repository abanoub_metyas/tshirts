<?php

date_default_timezone_set("Africa/Cairo");

// Web Routing
Route::group(['middleware' => ['web']], function () {

    Route::get("/",'front\pages@homepage');

    //register And Login
    Route::get('/login','front\register_login@login');
    Route::post('/login','front\register_login@login');

    Route::get('/register','front\register_login@register');
    Route::post('/register','front\register_login@register');


    Route::post('/upload_files','admin\uploader@load_files');

    Route::get("/logout","logout@index");

    #region Subscribe & Support

    Route::get('/contact_us','front\subscribe_contact@index');
    Route::post('/subscribe_contact/subscribe', 'front\subscribe_contact@subscribe');
    Route::post('/subscribe_contact/make_a_contact', 'front\subscribe_contact@make_a_contact');

    #endregion



    // Password Reset Routes...
    Route::get('password/reset/{token?}', 'Auth\ForgotPasswordController@showResetForm')->name('password.reset');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ForgotPasswordController@reset');


    Route::get("upload",'Controller@ckeditor_upload');
    Route::post("upload",'Controller@ckeditor_upload');

    Route::get("browse",'Controller@ckeditor_browse');
    Route::post("browse",'Controller@ckeditor_browse');


    Route::post("/btm_form_helper/edit_slider_item",'Controller@edit_slider_item');


    Route::get("/pages/search",'front\pages@search');
    Route::get("/pages/show_page",'front\pages@show_page');

    Route::get("/stores/{name}",'front\pages@youtuber_profile');

    Route::get('login/google', 'Auth\LoginController@redirectToProvider');
    Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');

    Route::get('login/facebook', 'Auth\LoginController@redirectToProviderFacebook');
    Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallbackFacebook');


});










