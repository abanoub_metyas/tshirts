<?php

Route::group([
    'middleware' => 'check_printing_operator',
    'prefix'=>'printing_operator',
    'namespace'=>'printing_operator',
], function () {

    Route::get('dashboard', 'dashboard@index');

    Route::get('orders/show_all/{user_id?}','orders@index');
    Route::get('orders/bill_orders/{bill_id}','orders@bill_orders');

    Route::get('printing_requests/change_status','printing_requests@change_status');
    Route::post('printing_requests/change_status','printing_requests@change_status');

    Route::get('printing_requests','printing_requests@show_all');
});

