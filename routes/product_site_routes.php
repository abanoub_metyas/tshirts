<?php

Route::group(['middleware' => ['web']], function () {
    #region cats
    Route::get("show_parent_cat",'front\category@show_parent_cat');
    Route::get("show_child_cat",'front\category@show_child_cat');
    Route::get("show_product",'front\category@show_product');
    Route::post("submit_review",'front\category@submit_review');


    Route::post('/order/order_item','front\order@order_item');
    Route::get('/order_status','front\order@order_status');

    Route::post('order/remove_item','front\order@remove_item');
    Route::post('order/change_pro_quantity','front\order@change_pro_quantity');
    Route::post('order/check_coupon','front\order@check_coupon');

    Route::get('visitor/checkout', 'front\order@checkout_orders');
    Route::post('visitor/checkout','front\order@post_bill');

    Route::get('complete_payment', 'front\order@complete_payment');
    Route::get('accept_payment_notification', 'front\order@accept_payment_notification');
    Route::post('accept_payment_notification', 'front\order@accept_payment_notification');

    Route::get('accept_payment_kiosk_notification', 'front\order@accept_payment_kiosk_notification');
    Route::post('accept_payment_kiosk_notification', 'front\order@accept_payment_kiosk_notification');


});

