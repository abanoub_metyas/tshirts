<?php

Route::group([
    'middleware' => 'check_youtuber',
    'prefix'=>'youtuber',
    'namespace'=>'youtuber',
], function () {

    Route::get('dashboard', 'dashboard@index');

    Route::post('edit_data', 'dashboard@edit_data');
    Route::post('change_password', 'dashboard@change_password');

    Route::get('show_profit_and_loss', 'dashboard@show_profit_and_loss');



});

