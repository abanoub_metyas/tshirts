<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{

    protected $namespace = 'App\Http\Controllers';

    public function boot()
    {
        //

        parent::boot();
    }


    public function map()
    {
//        $this->mapApiRoutes();
//
//        $this->mapWebRoutes();
//

        Route::group(['namespace' => $this->namespace,'middleware' => ['web']],function(){
            require base_path('routes/admin_routes.php');
            require base_path('routes/service_operator_routes.php');
            require base_path('routes/printing_operator_routes.php');

            require base_path('routes/user_routes.php');
            require base_path('routes/youtuber_routes.php');
            require base_path('routes/dashboard_routes.php');
            require base_path('routes/dev_routes.php');
            require base_path('routes/homepage_routes.php');
            require base_path('routes/product_site_routes.php');
        });

    }

    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/homepage_routes.php'));
    }

    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
