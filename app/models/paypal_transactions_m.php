<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class paypal_transactions_m extends Model
{

    protected $table = "paypal_transactions";
    protected $primaryKey = "paypal_id";
    public $timestamps = false;

    protected $fillable = [
        'paypal_user_id', 'paypal_payment_status', 'paypal_receiver_email',
        'paypal_sender_email', 'paypal_payment_gross', 'paypal_request_json','payment_date'
    ];

    static function get_paypals($where = "")
    {
        $res = DB::select("
            select paypal.*,user_obj.*

            from paypal_transactions as paypal
            INNER JOIN users as user_obj on (user_obj.user_id = paypal.user_id)
            $where
        ");

        return $res;
    }



}
