<?php

namespace App\models;

use App\models\attachments_m;
use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class cities_m extends \Eloquent
{
    use SoftDeletes;

    protected $table = "cities";

    protected $primaryKey = "city_id";

    protected $dates = ["deleted_at"];

    protected $fillable = [
        'country_id', 'city_name', 'aramex_shipping', 'fetchr_shipping'
    ];

    public static function get_data($country_id=""){

        $data=self::
        select(DB::raw("
            countries.*,
            cities.*,
            concat(countries.country_name,'_',cities.city_name) as 'country_city_name'
        "))->
        join("countries","countries.country_id","=","cities.country_id");

        if(!empty($country_id)){
            $data=$data->where("cities.country_id",$country_id);
        }

        return $data->get();
    }


}
