<?php

namespace App\models\printing;

use App\models\attachments_m;
use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class printing_requests_m extends \Eloquent
{
    use SoftDeletes;

    protected $table = "printing_requests";

    protected $primaryKey = "print_id";

    protected $dates = ["deleted_at"];

    protected $fillable = [
        'material_id', 'bill_id',
        'bill_order_id', 'printing_operator_id', 'print_status', 'request_quantity'
    ];

    public static function get_requests($request_data=[]){

        $print_status="";
        if(isset($request_data["print_status"])){
            $print_status=$request_data["print_status"];
        }

        $start_date="";
        if(isset($request_data["start_date"])){
            $start_date=$request_data["start_date"];
        }

        $end_date="";
        if(isset($request_data["end_date"])){
            $end_date=$request_data["end_date"];
        }


        $requests=self::
        join("materials","materials.m_id","=","printing_requests.material_id");


        if($print_status!=""&&$print_status!="all"){
            $requests=$requests->where("printing_requests.print_status",$print_status);
        }

        if(!empty($start_date)){
            $requests=$requests->where("printing_requests.created_at",">=",$start_date);
        }

        if(!empty($end_date)){
            $requests=$requests->where("printing_requests.created_at","<=",$end_date);
        }


        $requests=$requests->orderBy("printing_requests.created_at","desc");
        $requests=$requests->get();

        return $requests;
    }


}
