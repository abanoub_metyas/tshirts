<?php

namespace App\models\printing;

use App\models\attachments_m;
use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class printed_tshirts_m extends \Eloquent
{
    use SoftDeletes;

    protected $table = "printed_tshirts";

    protected $primaryKey = "pt_id";

    protected $dates = ["deleted_at"];

    protected $fillable = [
        'pro_id','pt_color','pt_size','pt_quantity'
    ];


    public static function get_data($paras=[]){
        $rest=self::
        select(\DB::raw("
            product.pro_material_type,
            product_translate.pro_name,
            printed_tshirts.*,
            users.email as 'youtuber_email',
            users.full_name as 'youtuber_name'
            
        "))->
        join("product","product.pro_id","=","printed_tshirts.pro_id")->
        join("product_translate","product_translate.pro_id","=","product.pro_id")->
        join("users","users.user_id","=","product.youtuber_id")->
        where("product_translate.lang_id","=","1");

        if(isset($paras["pt_id"])&&!empty($paras["pt_id"])){
            $rest=$rest->where("printed_tshirts.pt_id",$paras["pt_id"]);
        }

        if(isset($paras["youtuber_name"])&&!empty($paras["youtuber_name"])){
            $rest=$rest->whereRaw("users.full_name like '%".$paras["youtuber_name"]."%'");
        }

        if(isset($paras["product_name"])&&!empty($paras["product_name"])){
            $rest=$rest->whereRaw("product_translate.pro_name like '%".$paras["product_name"]."%'");
        }


        return $rest->get();
    }

    public static function add_quantity_from_return_order($return_order_obj){


        $printed_order_obj=self::where([
            'pro_id'=>$return_order_obj->pro_id,
            'pt_color'=>$return_order_obj->ro_color,
            'pt_size'=>$return_order_obj->ro_size
        ])->get()->first();

        if(is_object($printed_order_obj)){
            $printed_order_obj->update([
                "pt_quantity"=>($printed_order_obj->pt_quantity+$return_order_obj->ro_quantity)
            ]);
        }
        else{
            self::create([
                'pro_id'=>$return_order_obj->pro_id,
                'pt_color'=>$return_order_obj->ro_color,
                'pt_size'=>$return_order_obj->ro_size,
                'pt_quantity'=>$return_order_obj->ro_quantity
            ]);
        }


    }


}
