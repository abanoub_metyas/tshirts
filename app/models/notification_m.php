<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class notification_m extends Model
{

    protected $table = "notification";
    protected $primaryKey = "not_id";
    public $timestamps = true;

    protected $fillable = [
        'not_title', 'not_type', 'not_to_userid'
    ];


    static function get_notifications($additional_where = "")
    {
        $results = DB::select("
             select *
             from notification
             #where
             $additional_where ");

        return $results;

    }

}
