<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class product_translate_m extends Model
{


    use SoftDeletes;

    protected $table = "product_translate";

    protected $primaryKey = "id";

    protected $dates = ["deleted_at"];

    protected $fillable = [
        'pro_id', 'pro_name','pro_short_desc' , 'pro_desc',
        'pro_search_tags',
        'pro_meta_title','pro_meta_desc', 'pro_meta_keywords',
        'lang_id',
    ];


}
