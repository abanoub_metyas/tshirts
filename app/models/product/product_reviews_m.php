<?php

namespace App\models\product;

use App\models\attachments_m;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class product_reviews_m extends \Eloquent
{
    use SoftDeletes;

    protected $table = "product_reviews";

    protected $primaryKey = "review_id";

    protected $dates = ["deleted_at"];

    protected $fillable = [
        'pro_id', 'review_name', 'review_text', 'review_rate'
    ];

    public static function get_pro_avg($pro_id){

        $avg=self::
        where("pro_id",$pro_id)->
        avg("review_rate");

        return round($avg,1);
    }


}
