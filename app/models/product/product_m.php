<?php

namespace App\models\product;

use App\models\attachments_m;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class product_m extends \Eloquent
{
    use SoftDeletes;

    protected $table = "product";

    protected $primaryKey = "pro_id";

    protected $dates = ["deleted_at"];

    protected $fillable = [
        'cat_id',
        'youtuber_id',
        'small_img_id',
        'pro_material_type',
        'pro_slider',
        'youtuber_profit_per_tshirt','youtuber_lost_per_return_single_tshirt',
    ];

    public static $default_lang_id=1;

    static function get_all_pros($additional_where = ""){

        $products = DB::select("
            SELECT 
            pro.*,
            pro_translate.*,
            
            small_img.path as 'small_img_path' ,
            small_img.alt as 'small_img_alt' ,
            small_img.title as 'small_img_title',
            user_obj.email as 'youtuber_email',
            user_obj.full_name as 'youtuber_name'
    
            FROM `product` as pro

            inner join product_translate as pro_translate on (pro.pro_id=pro_translate.pro_id AND pro_translate.lang_id=".self::$default_lang_id.")
            inner join users as user_obj on (user_obj.user_id=pro.youtuber_id)

            #imgs
            left outer join attachments as small_img on small_img.id=pro.small_img_id

            #where
            where pro.deleted_at is null $additional_where
"
        );


        $new_pros=array();
        if (collect($products)->groupBy("pro_id")->count()==1)
        {
            foreach($products as $key => $pro)
            {

                //get slider data
                $slider_ids = json_decode($pro->pro_slider);
                $pro->slider_imgs = array();
                if (is_array($slider_ids)&&  count($slider_ids) >0) {

                    $slider_imgs = attachments_m::whereIn("id",$slider_ids)->get()->all();
                    $pro->slider_imgs=$slider_imgs;
                }
                else{
                    $slider_item=new \stdClass();
                    $slider_item->path=$pro->big_img_path;
                    $slider_item->alt=$pro->big_img_alt;
                    $slider_item->title=$pro->big_img_title;
                    $pro->slider_imgs=[$slider_item];
                }

                $new_pros[]=$pro;
            }
            $products=$new_pros;
        }

        return $products;

    }



}
