<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class product_quantities_m extends \Eloquent
{

    protected $table = "product_quantities";

    protected $primaryKey = "pq_id";

    public $timestamps=false;

    protected $fillable = [
        'pro_id', 'egypt_or_outside',
        'pro_color', 'pro_available_sizes',
        'pro_price',
        'pro_discount_amount', 'pq_img_id'
    ];

    public static function get_product_quantities($pro_id,$egypt_or_not=""){

        $data=self::
        leftJoin("attachments","attachments.id","=","product_quantities.pq_img_id")->
        where("pro_id",$pro_id);

        if(!empty($egypt_or_not)){
            $data=$data->where("egypt_or_outside",$egypt_or_not);
        }

        return $data->get();
    }


}
