<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class user_address_list_m extends \Eloquent
{
    use SoftDeletes;

    protected $table = "user_address_list";
    protected $primaryKey = "address_id";

    protected $dates = ["deleted_at"];
    protected $fillable = [
        'user_id',
        'add_full_name','add_email',
        'add_governorate',
        'add_country', 'add_city', 'add_street', 'add_type',
        'add_tel_number', 'add_tel_number_verified',
        'add_post_code',
        'add_notes'
    ];

    
}
