<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class materials_m extends \Eloquent
{
    use SoftDeletes;

    protected $table = "materials";

    protected $primaryKey = "m_id";

    protected $dates = ["deleted_at"];

    protected $fillable = [
        'm_name',
        'm_size','m_color',
        'm_quantity','m_critical_limit',
        'm_name_color_size'
    ];


}
