<?php

namespace App\models\bills;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;

class bills_m extends \Eloquent
{
    use SoftDeletes;

    protected $table = "bills";
    protected $primaryKey = "bill_id";

    protected $dates = ["deleted_at"];
    protected $fillable = [
        'user_id' , 'youtuber_id','bill_amount', 'address_id',
        'bill_status','payment_method', 'coupon_id',
        'bill_paid_amount','api_order_id',
        'delivery_type','delivery_cost',
        'shipping_response','shipping_awb_link',
    ];


    public static function update_bill_cost($bill_id){
        $total_cost=bill_orders_m::get_bill_orders_total_cost($bill_id);

        $bill_obj=self::findOrFail($bill_id);

        $bill_obj->update([
            "bill_amount"=>($total_cost+$bill_obj->delivery_cost)
        ]);
    }

    public static function get_bill($bill_id){
        return
            self::
            select(DB::raw("
                bills.*,
                bills.created_at as 'bill_created_at',
                user_address_list.*
            "))->
            join(
                "user_address_list",
                "user_address_list.address_id",
                "=",
                "bills.address_id"
            )->
            where("bills.bill_id",$bill_id)->
            get()->first();
    }

    public static function get_user_bills($user_id){
        return
        self::
            select(DB::raw("
                bills.*,
                bills.created_at as 'bill_created_at',
                user_address_list.*
            "))->
            join(
                "user_address_list",
                "user_address_list.address_id",
                "=",
                "bills.address_id"
            )->
            where("bills.user_id",$user_id)->
            orderBy("bills.created_at","desc")->
            get();
    }

    public static function get_bills($request_data=[]){

        $bill_status="";
        if(isset($request_data["bill_status"])){
            $bill_status=$request_data["bill_status"];
        }

        $start_date="";
        if(isset($request_data["start_date"])){
            $start_date=$request_data["start_date"];
        }

        $end_date="";
        if(isset($request_data["end_date"])){
            $end_date=$request_data["end_date"];
        }

        $youtuber_id="";
        if(isset($request_data["youtuber_id"])){
            $youtuber_id=$request_data["youtuber_id"];
        }


        $bills=self::
        select(DB::raw("
                bills.*,
                bills.created_at as 'bill_created_at',
                user_address_list.*
            "))->
        join(
            "user_address_list",
            "user_address_list.address_id",
            "=",
            "bills.address_id"
        );

        if($bill_status!=""&&$bill_status!="all"){
            $bills=$bills->where("bill_status",$bill_status);
        }

        if(!empty($start_date)){
            $bills=$bills->where("bills.created_at",">=",$start_date);
        }

        if(!empty($end_date)){
            $bills=$bills->where("bills.created_at","<=",$end_date);
        }

        if(!empty($youtuber_id)){
            $bills=$bills->where("bills.youtuber_id",$youtuber_id);
        }



        $bills=$bills->orderBy("bills.created_at","desc");
        $bills=$bills->get();

        return $bills;
    }

}
