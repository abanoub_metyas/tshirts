<?php

namespace App\models\bills;

use Illuminate\Database\Eloquent\SoftDeletes;

class return_orders_m extends \Eloquent
{
    use SoftDeletes;

    protected $table = "return_orders";
    protected $primaryKey = "ro_id";

    protected $dates = ["deleted_at"];
    protected $fillable = [
        'pro_id',
        'ro_size', 'ro_color',
        'ro_quantity'
    ];

    public static function get_data($paras=[]){
        $rest=self::
        select(\DB::raw("
            product.pro_material_type,
            product_translate.pro_name,
            return_orders.*,
            users.email as 'youtuber_email',
            users.full_name as 'youtuber_name'
            
        "))->
        join("product","product.pro_id","=","return_orders.pro_id")->
        join("product_translate","product_translate.pro_id","=","product.pro_id")->
        join("users","users.user_id","=","product.youtuber_id")->
        where("product_translate.lang_id","=","1");

        if(isset($paras["ro_id"])&&!empty($paras["ro_id"])){
            $rest=$rest->where("return_orders.ro_id",$paras["ro_id"]);
        }

        if(isset($paras["start_date"])&&!empty($paras["start_date"])){
            $rest=$rest->where("return_orders.created_at",">=",$paras["start_date"]);
        }

        if(isset($paras["end_date"])&&!empty($paras["end_date"])){
            $rest=$rest->where("return_orders.created_at","<=",$paras["end_date"]);
        }

        if(isset($paras["youtuber_name"])&&!empty($paras["youtuber_name"])){
            $rest=$rest->whereRaw("users.full_name like '%".$paras["youtuber_name"]."%'");
        }

        if(isset($paras["product_name"])&&!empty($paras["product_name"])){
            $rest=$rest->whereRaw("product_translate.pro_name like '%".$paras["product_name"]."%'");
        }


        return $rest->get();
    }


}
