<?php

namespace App\models\bills;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class bill_orders_m extends \Eloquent
{
    use SoftDeletes;

    protected $table = "bill_orders";
    protected $primaryKey = "bill_order_id";

    protected $dates = ["deleted_at"];
    protected $fillable = [
        'bill_id',
        'pro_id', 'pro_quan_id','order_selected_size' ,
        'order_quantity'
    ];

    public static function get_bill_orders_total_cost($bill_id){

        $data=self::
        select(DB::raw("
            bill_orders.*,
            (
               bill_orders.order_quantity*(product_quantities.pro_price-product_quantities.pro_discount_amount)
            ) 
            as 'total_cost'
        "))->
        join("product","product.pro_id","=","bill_orders.pro_id")->
        join(
            "product_quantities",
            "product_quantities.pq_id",
            "=",
            "bill_orders.pro_quan_id"
        )->
        where("bill_orders.bill_id",$bill_id)->
        get();

        return array_sum($data->pluck("total_cost")->all());
    }

    public static function get_youtuber_total_orders($youtuber_id){

        $data=self::
        select(DB::raw("
            bill_orders.*,
            (
               bill_orders.order_quantity*(product_quantities.pro_price-product_quantities.pro_discount_amount)
            ) 
            as 'total_cost'
        "))->
        join("product","product.pro_id","=","bill_orders.pro_id")->
        join(
            "product_quantities",
            "product_quantities.pq_id",
            "=",
            "bill_orders.pro_quan_id"
        )->
        where("product.youtuber_id",$youtuber_id)->
        get();

        return array_sum($data->pluck("total_cost")->all());
    }

    public static function get_data(
        $order_id="",
        $bill_ids=[],
        $youtuber_id=""
    ){

        $data=self::
            select(DB::raw("
                product_translate.pro_name,
                bill_orders.*,
                product_quantities.pro_color,
                product.pro_material_type,
                
                CONCAT(
                    product.pro_id,
                    '_',
                    bill_orders.order_selected_size,
                    '_',
                    product_quantities.pro_color
                ) 
                as 'selected_product_color_size' 
            "))->
            join("product","product.pro_id","=","bill_orders.pro_id")->
            join("product_translate","product.pro_id","=","product_translate.pro_id")->
            join(
                "product_quantities",
                "product_quantities.pq_id",
                "=",
                "bill_orders.pro_quan_id"
            )->
            where("product_translate.lang_id","1");

        if(!empty($youtuber_id)){
            return $data->where("youtuber_id",$youtuber_id)->get();
        }

        if(isset_and_array($bill_ids)){
            return
                $data->
                whereIn("bill_orders.bill_id",$bill_ids)->
                get();
        }


        if(is_array($order_id)){
            return
            $data->
            whereIn("bill_order_id",$order_id)->
            get();
        }


        return $data->
        where("bill_order_id",$order_id)->
        get()->
        first();
    }


}
