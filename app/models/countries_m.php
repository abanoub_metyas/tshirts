<?php

namespace App\models;

use App\models\attachments_m;
use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class countries_m extends \Eloquent
{
    use SoftDeletes;

    protected $table = "countries";

    protected $primaryKey = "country_id";

    protected $dates = ["deleted_at"];

    protected $fillable = [
        'country_name','aramex_shipping'
    ];


}
