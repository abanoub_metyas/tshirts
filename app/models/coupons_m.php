<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class coupons_m extends \Eloquent
{
    use SoftDeletes;

    protected $table = "coupons";

    protected $primaryKey = "coupon_id";

    protected $dates = ["deleted_at"];

    protected $fillable = [
        'coupon_code', 'coupon_end_date',
        'is_rate', 'coupon_value','is_used'
    ];


}
