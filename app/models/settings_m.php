<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class settings_m extends Model
{
    use SoftDeletes;

    protected $table = "settings";
    protected $primaryKey = "set_id";
    protected $dates= ["deleted_at"];
    protected $fillable = [
        'default_currency','paypal_account_email','paypal_tax_percent',
        'current_temp','last_time_get_temp','packaging_product_price',
        'sending_expenses','pay_when_deliver_amount'
    ];

}
