<?php

namespace App\checkout;


use App\helpers\utility;
use App\models\bills\bill_orders_m;
use App\models\bills\bills_m;
use App\models\cities_m;
use App\models\countries_m;
use App\models\coupons_m;
use App\models\product\product_m;
use App\models\product\product_quantities_m;
use App\models\user_address_list_m;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class checkout
{


    public static function get_cart_items_data($user_or_visitor="user"){

        $cart_items=\Session::get("cart_items");

        $cart_items_collection=collect($cart_items);
        $cart_items_collection=$cart_items_collection->where("pro_id",">","0");

        if(!isset_and_array($cart_items_collection->all())){
            return \redirect(
                $user_or_visitor=="user"?
                "/user/dashboard":
                "/"
            )->with("msg",
                "<div class='alert alert-info'>
                    Add some items before enter to checkout page
                </div>"
            )->send();

            die();
        }


        $get_pro_ids=$cart_items_collection->pluck("pro_id")->all();
        $get_pro_ids=array_unique($get_pro_ids);

        $products=product_m::
        whereIn("pro_id",$get_pro_ids)->
        get()->groupBy("youtuber_id");

        if($products->count()>1){
            $product_ids=$products->first()->pluck("pro_id")->all();
            $cart_items_collection=$cart_items_collection->
            whereIn("pro_id",$product_ids);
        }

        if($cart_items_collection->groupBy("egypt_or_outside")->count()>1){
            return "egypt_and_outside";
        }


        $pro_ids=$cart_items_collection->pluck("pro_id")->all();
        $pro_ids=array_unique($pro_ids);

        $quantity_ids=$cart_items_collection->pluck("quantity_id")->all();
        $quantity_ids=array_unique($quantity_ids);

        $pro_ids=array_map("intval",$pro_ids);

        $pro_objs=product_m::get_all_pros("
             AND pro.pro_id in (".  implode(",", $pro_ids).")
        ");
        $pro_objs=collect($pro_objs)->groupBy("pro_id");


        $quantity_objs=product_quantities_m::
        whereIn("pq_id",$quantity_ids)->
        get()->groupBy("pq_id");

        $egypt_or_not="outside";
        if(
            isset(ip_info(get_client_ip())["country"])&&
            ip_info(get_client_ip())["country"]=="Egypt"
        ){
            $egypt_or_not="egypt";
        }

        if($cart_items_collection->first()["egypt_or_outside"]!=$egypt_or_not){
            return "egypt_and_outside";
        }


        if ($egypt_or_not=="egypt"){
            $countries=countries_m::where("country_name","Egypt")->get();
        }
        else{
            $countries=countries_m::where("country_name","!=","Egypt")->get();
        }

        $cities=cities_m::get_data();

        return [
            "cart_items_collection"=>$cart_items_collection,
            "cart_items"=>$cart_items_collection->all(),
            "pro_objs"=>$pro_objs,
            "quantity_objs"=>$quantity_objs,
            "countries"=>$countries,
            "cities"=>$cities,
            "egypt_or_not"=>$egypt_or_not,
            "currency_symbol"=>($egypt_or_not=="egypt")?"EGP":"USD",
        ];

    }

    public static function post_bill(
        Request $request,
        $user_id,
        $user_or_visitor="user"
    ){

        $cart_items_data=checkout::get_cart_items_data();

        $cart_items=$cart_items_data["cart_items"];
        $pro_objs=$cart_items_data["pro_objs"];
        $quantity_objs=$cart_items_data["quantity_objs"];

        if($user_or_visitor=="user"&&empty($request->get("selected_user_address"))){
            return \redirect('/user/checkout')->
            with("msg","<div class='alert alert-warning'>you should select address</div>")->
            send();
        }
        else if($user_or_visitor=="visitor"&&empty($request->get("add_email"))){
            return \redirect('/visitor/checkout')->
            with("msg","<div class='alert alert-warning'>you should add address fields</div>")->
            send();
        }


        //save bill and bill orders

        #region get orders


        $orders=[];
        $bill_amount=0;


        foreach($cart_items as $pro_key=>$cart_item){

            if(!isset($pro_objs[$cart_item["pro_id"]]))continue;
            if(!isset($quantity_objs[$cart_item["quantity_id"]]))continue;

            $pro_obj=$pro_objs[$cart_item["pro_id"]]->first();
            $pro_quantity_obj=$quantity_objs[$cart_item["quantity_id"]]->first();

            $pro_orginal_price=($pro_quantity_obj->pro_price - $pro_quantity_obj->pro_discount_amount);

            $bill_amount=$bill_amount+($pro_orginal_price*$cart_item["pro_quantity"]);


            $orders[]=[
                'bill_id'=>0,
                'pro_id'=>$pro_obj->pro_id,
                'pro_quan_id'=>$cart_item["quantity_id"],
                'order_selected_size'=>$cart_item["selected_product_size"],
                'order_quantity'=>$cart_item["pro_quantity"],
            ];

        }

        #endregion

        #region coupon
        $coupon=clean($request->get("check_coupon_text"));

        //check coupon is not used or not

        $coupon_obj=coupons_m::
        where("coupon_code",$coupon)->
        where("coupon_end_date",">=",Carbon::now())->
        where("is_used","0")->
        get()->first();

        if(is_object($coupon_obj)){

            if($coupon_obj->is_rate){
                $bill_amount=$bill_amount- (($bill_amount*$coupon_obj->coupon_value)/100);
            }
            else{
                $bill_amount=$bill_amount-$bill_amount*$coupon_obj->coupon_value;
            }

            $coupon_obj->update([
                "is_used"=>"1"
            ]);
        }

        if($bill_amount<0){
            $bill_amount=0;
        }

        $bill_amount=round($bill_amount,2);
        #endregion

        $selected_user_address=$request->get("selected_user_address");

        if($user_or_visitor=="visitor"){

            $add_data=$request->all();
            $add_data["user_id"]=$user_id;

            $user_address_obj=user_address_list_m::create($add_data);

            $selected_user_address=$user_address_obj->address_id;
        }

        $selected_address_obj=user_address_list_m::findOrFail($selected_user_address);

        $selected_country_obj=countries_m::
        where("country_name",$selected_address_obj->add_country)->
        get()->first();

        if(!is_object($selected_country_obj))abort(404);


        $delivery_cost=0;
        if ($cart_items_data['currency_symbol']=="EGP"){

            $selected_city_obj=cities_m::
            where("country_id",$selected_country_obj->country_id)->
            where("city_name",$selected_address_obj->add_governorate)->
            get()->first();

            if(!is_object($selected_city_obj))abort(404);

            $delivery_cost=$selected_city_obj->{$request->get("selected_shipping_company")."_shipping"};
        }
        else{
            $delivery_cost=$selected_country_obj->aramex_shipping;
        }

        //create bill
        $bill_obj=bills_m::create([
            'user_id'=>$user_id,
            'youtuber_id'=>$pro_objs->first()->first()->youtuber_id,
            'bill_amount'=>$bill_amount+$delivery_cost,
            'bill_paid_money'=>0,
            'address_id'=>$selected_user_address,
            'bill_status'=>"pending",
            'coupon_id'=>(is_object($coupon_obj)?$coupon_obj->coupon_id:""),
            "delivery_type"=>$request->get("selected_shipping_company"),
            "delivery_cost"=>$delivery_cost
        ]);

        foreach($orders as $order_key=>$order){
            $orders[$order_key]["bill_id"]=$bill_obj->bill_id;
        }

        bill_orders_m::insert($orders);


        //send notification to service_operator

        utility::send_notification_to_users(
            utility::get_admins(["admin","dev","service_operator"],true),
            "New order".date("Y-m-d H:i"),
            "checkout new order"
        );


        //get remain carts
        $all_products_at_cart=\Session::get("cart_items");
        $all_products_at_cart=collect($all_products_at_cart);

        $used_products=collect($cart_items)->pluck("pro_id")->all();
        $remain_products=$all_products_at_cart->whereNotIn("pro_id",$used_products)->all();

        $request->session()->put('cart_items',$remain_products);

        $selected_payment_method=$request->get("selected_payment_method");

        if($selected_payment_method=="payment_online"){
            $bill_obj->update([
                "payment_method"=>"payment online"
            ]);

            return redirect('/complete_payment?bill_id='.$bill_obj->bill_id)->send();
        }
        elseif($selected_payment_method=="masary_aman"){
            $bill_obj->update([
                "payment_method"=>"masary_aman"
            ]);

            $kiosk_number=self::online_payment_kiosk(bills_m::get_bill($bill_obj->bill_id));

            return redirect('/order_status?order_id='.$bill_obj->bill_id.'&order_status=done&msg=Pay from any aman or masary account for this number: '.$kiosk_number)->send();
        }
        elseif($selected_payment_method=="deliver") {
            $bill_obj->update([
                "payment_method"=>"pay on deliver"
            ]);

            return redirect('/order_status?order_id='.$bill_obj->bill_id.'&order_status=done')->send();
        }

    }

    public static function get_token(){

        $client = new Client();

        $api_key="ZXlKaGJHY2lPaUpJVXpVeE1pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnVZVzFsSWpvaWFXNXBkR2xoYkNJc0ltTnNZWE56SWpvaVRXVnlZMmhoYm5RaUxDSndjbTltYVd4bFgzQnJJam96TURVMGZRLk1lWGVGZmZ6VzBUM3N2NExjVS0xWk9jbnZnVkFOSXpMcjEtaktkaGw5X0lOMGt6RVhoemFlaEEtSmxpMzZCT01WNlk3aFdlZ2d0eklxTnFOc2RCemtB";
        $username="TeeArab";
        $password="DI_na1997";


        $get_token_res = $client->request('POST', 'https://accept.paymobsolutions.com/api/auth/tokens',
            [
                'json' => [
                    "api_key"=>$api_key,
                    "username"=>$username,
                    "password"=>$password,
                ],
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]
        );

        $get_token_data=\GuzzleHttp\json_decode($get_token_res->getBody());

        $token=$get_token_data->token;
        $merchant_id=$get_token_data->profile->id;

        return [
            "token"=>$token,
            "merchant_id"=>$merchant_id,
        ];
    }

    public static function online_payment($bill_obj){
        $client = new Client();

        $token_data=self::get_token();
        $token=$token_data["token"];
        $merchant_id=$token_data["merchant_id"];

        if(($bill_obj->bill_amount-$bill_obj->bill_paid_amount)<=0){
            return redirect('/order_status?order_id='.$bill_obj->bill_id.'&order_status=done')->
            with("msg","<div class='alert alert-info'>you are already paid this order $bill_obj->bill_paid_amount of ($bill_obj->bill_amount) EGP</div>")->send();
        }

        $order_total_amount=($bill_obj->bill_amount-$bill_obj->bill_paid_amount)*100;

        $merchant_order_id=$bill_obj->bill_id.rand(11111,99999);

        //step 2 makle order
        $set_order_res = $client->request('POST', 'https://accept.paymobsolutions.com/api/ecommerce/orders',
            [
                'json' => [
                    "auth_token"=>$token,
                    "delivery_needed"=> "false",
                    "merchant_id"=> $merchant_id,
                    "amount_cents"=> $order_total_amount,
                    "currency"=> "EGP",
                    "merchant_order_id"=>$merchant_order_id,
                    "items"=> [],
                ],
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]
        );

        $set_order_res=\GuzzleHttp\json_decode($set_order_res->getBody());

        $api_order_id=$set_order_res->id;

        $bill_obj->update([
            "api_order_id"=>$api_order_id
        ]);

        //step 3
        $set_order_res = $client->request('POST', 'https://accept.paymobsolutions.com/api/acceptance/payment_keys',
            [
                'json' => [
                    "auth_token"=> $token,
                    "amount_cents"=> $order_total_amount,
                    "expiration"=> 3600,
                    "order_id"=> $api_order_id,
                    "billing_data"=> [
                        "email"=> "abanoub.metyas.btm@gmail.com",
                        "first_name"=> "$bill_obj->bill_id",
                        "last_name"=> "$bill_obj->bill_id",
                        "apartment"=> "$bill_obj->bill_id",
                        "floor"=> "$bill_obj->bill_id",
                        "building"=> "$bill_obj->bill_id",
                        "street"=> "$bill_obj->bill_id",
                        "city"=> "$bill_obj->bill_id",
                        "phone_number"=> "$bill_obj->bill_id",
                        "country"=> "$bill_obj->bill_id",
                    ],
                    "currency"=> "EGP",
                    "integration_id"=> 4646,
                    "lock_order_when_paid"=> "false" // optional field (*)
                ],
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]
        );

        $set_order_res=\GuzzleHttp\json_decode($set_order_res->getBody());

        $payment_key_token=$set_order_res->token;

        return "https://accept.paymobsolutions.com/api/acceptance/iframes/7632?payment_token=$payment_key_token";
    }

    public static function get_check_hmac(Request $request){

        $fields=[
            "amount_cents", "created_at", "currency", "error_occured",
            "has_parent_transaction", "id", "integration_id", "is_3d_secure",
            "is_auth", "is_capture", "is_refunded", "is_standalone_payment",
            "is_voided","order","owner", "pending",
            "source_data_pan","source_data_sub_type","source_data_type","success"
        ];


        $contact_string="";

        foreach ($fields as $field){
            $contact_string.=$request->get($field);
        }

        $compare_mac=hash_hmac("SHA512",$contact_string,"E5CDA0E0090CE4E2BF79DB3FB5C80C54");

        $request_mac=$request->get("hmac");

        if(empty($request_mac))return abort(404);

        return hash_equals($compare_mac,$request_mac);
    }

    public static function post_check_hmac(Request $request){

        $fields_data=$request->get("obj");

        $fields=[
            "amount_cents", "created_at", "currency", "error_occured",
            "has_parent_transaction", "id", "integration_id", "is_3d_secure",
            "is_auth", "is_capture", "is_refunded", "is_standalone_payment",
            "is_voided",
            "order.id",
            "owner", "pending",
            "source_data.pan",
            "source_data.sub_type",
            "source_data.type",
            "success"
        ];


        $contact_string="";

        foreach ($fields as $field){
            $var_data="";

            if(strpos($field,".")!==false){
                $field=explode(".",$field);
                $var_data=$fields_data[$field[0]][$field[1]];
            }
            else{
                $var_data=$fields_data[$field];
            }

            if($var_data===true){
                $var_data="true";
            }

            if($var_data===false){
                $var_data="false";
            }

            $contact_string.=$var_data;
        }

        $compare_mac=hash_hmac("SHA512",$contact_string,"E5CDA0E0090CE4E2BF79DB3FB5C80C54");

        $request_mac=$request->get("hmac","");

        return hash_equals($compare_mac,$request_mac);
    }

    public static function online_payment_kiosk($bill_obj){
        $client = new Client();

        $token_data=self::get_token();
        $token=$token_data["token"];
        $merchant_id=$token_data["merchant_id"];

        if(($bill_obj->bill_amount-$bill_obj->bill_paid_amount)<=0){
            return redirect('/order_status?order_id='.$bill_obj->bill_id.'&order_status=done')->
            with("msg","<div class='alert alert-info'>you are already paid this order $bill_obj->bill_paid_amount of ($bill_obj->bill_amount) EGP</div>")->send();
        }

        $order_total_amount=($bill_obj->bill_amount-$bill_obj->bill_paid_amount)*100;

        $merchant_order_id=$bill_obj->bill_id.rand(11111,99999);

        //step 2 makle order
        $set_order_res = $client->request('POST', 'https://accept.paymobsolutions.com/api/ecommerce/orders',
            [
                'json' => [
                    "auth_token"=>$token,
                    "delivery_needed"=> "false",
                    "merchant_id"=> $merchant_id,
                    "amount_cents"=> $order_total_amount,
                    "currency"=> "EGP",
                    "merchant_order_id"=>$merchant_order_id,
                    "items"=> [],
                ],
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]
        );

        $set_order_res=\GuzzleHttp\json_decode($set_order_res->getBody());

        $api_order_id=$set_order_res->id;

        $bill_obj->update([
            "api_order_id"=>$api_order_id
        ]);

        //step 3
        $set_order_res = $client->request('POST', 'https://accept.paymobsolutions.com/api/acceptance/payment_keys',
            [
                'json' => [
                    "auth_token"=> $token,
                    "amount_cents"=> $order_total_amount,
                    "expiration"=> 3600,
                    "order_id"=> $api_order_id,
                    "billing_data"=> [
                        "email"=> $bill_obj->add_email,
                        "first_name"=> $bill_obj->add_full_name,
                        "last_name"=> $bill_obj->add_full_name,
                        "apartment"=> "$bill_obj->bill_id",
                        "floor"=> "$bill_obj->bill_id",
                        "building"=> "$bill_obj->bill_id",
                        "street"=> $bill_obj->add_street,
                        "city"=> $bill_obj->add_city,
                        "phone_number"=> "$bill_obj->bill_id",
                        "country"=> "$bill_obj->bill_id",
                    ],
                    "currency"=> "EGP",
                    "integration_id"=> 4909,
                    "lock_order_when_paid"=> "false" // optional field (*)
                ],
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]
        );

        $set_order_res=\GuzzleHttp\json_decode($set_order_res->getBody());

        $payment_key_token=$set_order_res->token;

        //step 4
        $pay_request = $client->request('POST', 'https://accept.paymobsolutions.com/api/acceptance/payments/pay',
            [
                'json' => [
                    "source"=> [
                        "identifier"=> "AGGREGATOR",
                        "subtype"=> "AGGREGATOR"
                    ],
                    "payment_token"=> $payment_key_token
                ],
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]
        );

        $pay_request=\GuzzleHttp\json_decode($pay_request->getBody());

        return $pay_request->data->bill_reference;
    }


}