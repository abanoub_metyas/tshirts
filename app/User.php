<?php

namespace App;

use App\models\bills\bill_orders_m;
use App\models\bills\return_orders_m;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{

    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'email', 'provider','password',
        'user_type','username','full_name',
        'logo_id','cover_id',
        'user_active','user_can_login',
        'address','user_desc','youtuber_views',
        'meta_title', 'meta_desc' ,'meta_keywords'
    ];

    protected $primaryKey = 'user_id';

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ["deleted_at"];

    static function get_users($additional_where = "", $order_by = "" , $limit = "")
    {
        $users = DB::select("
             select user_obj.*
             , attach.id, attach.path, attach.alt , attach.title
             
             #joins
             from users as user_obj
             LEFT OUTER JOIN attachments as attach on (user_obj.logo_id = attach.id)

             #where
             where user_obj.deleted_at is null $additional_where
             
             #order by
             $order_by
             
             #limit
             $limit ");

        return $users;
    }

    public static function get_youtuber_profit_and_loss($youtuber_id,$filter_year){

        $profit=bill_orders_m::
            select(DB::raw("
                 product.pro_id,
                 SUM(order_quantity)*(product.youtuber_profit_per_tshirt) as 'profit',
                 MONTH(bill_orders.created_at) as 'bills_month'
            "))->
            join("product","product.pro_id","=","bill_orders.pro_id")->
            where("product.youtuber_id",$youtuber_id)->
            whereYear("bill_orders.created_at",$filter_year)->
            groupBy(DB::raw("
                pro_id,
                bills_month
            "))->get()->
            groupBy("bills_month");

        $loss=return_orders_m::
            select(DB::raw("
                     product.pro_id,
                     SUM(ro_quantity)*(product.youtuber_lost_per_return_single_tshirt) as 'loss',
                     MONTH(return_orders.created_at) as 'bills_month'
                "))->
            join("product","product.pro_id","=","return_orders.pro_id")->
            where("product.youtuber_id",$youtuber_id)->
            whereYear("return_orders.created_at",$filter_year)->
            groupBy(DB::raw("
                pro_id,
                bills_month
            "))->
            get()->groupBy("bills_month");


        return [
            "profit"=>$profit,
            "loss"=>$loss,
        ];
    }


}
