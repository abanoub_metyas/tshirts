<?php

namespace App\Http\Controllers\user;

use App\helpers\utility;
use App\Http\Controllers\user_controller;
use App\models\bills\bill_orders_m;
use App\models\bills\bills_m;
use App\models\product\product_m;
use App\models\product\product_quantities_m;
use App\models\coupons_m;
use App\models\user_address_list_m;
use App\User;
use Auth;
use Carbon\Carbon;
use App\checkout\checkout;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;

class order extends user_controller
{

    public function __construct(){
        parent::__construct();
    }

    public function show_user_orders(){

        $this->data["user_bills"] = collect([]);
        $this->data["bill_orders"] = collect([]);
        $this->data["user_products"] = collect([]);

        $get_bills = bills_m::get_user_bills($this->user_id);

        if($get_bills->count()>0)
        {
            $this->data["user_bills"] = $get_bills;
            $get_bills_ids = $get_bills->pluck("bill_id")->all();

            $get_orders = bill_orders_m::get_data([],$get_bills_ids);

            if($get_orders->count()>0)
            {

                $this->data["bill_orders"] = $get_orders->groupBy("bill_id");

                $pro_ids = $get_orders->pluck('pro_id')->all();

                array_unique($pro_ids);
                $pro_ids = implode(',',$pro_ids);
                $user_products = product_m::get_all_pros(" AND pro.pro_id in ($pro_ids) ");
                $user_products = collect($user_products)->groupBy("pro_id")->all();
                $this->data["user_products"] = $user_products;
            }

        }

        return view("user.subviews.order.show_all_orders",$this->data);
    }

    public function checkout_orders(Request $request) {

        //accumulator
        $this->data["checkout_page"]="";



        $cart_items_data=checkout::get_cart_items_data();

        if($cart_items_data=="egypt_and_outside"){
            $request->session()->put("cart_items",[]);
            $request->session()->save();
            return \redirect("/")->with("msg",
                "<div class='alert alert-info'>unexpected error happen.pleas re add product to the cart</div>"
            )->send();
        }

        $this->data["items_count"]=array_sum($cart_items_data["cart_items_collection"]->pluck("quantity_id")->all());

        $this->data=array_merge($this->data,$cart_items_data);


        if($cart_items_data["currency_symbol"]=="USD"){
            $this->data["all_user_addresses"]=
            user_address_list_m::
            where("user_id",$this->user_id)->
            where("add_country","!=","Egypt")->
            get();

        }
        else{
            $this->data["all_user_addresses"]=user_address_list_m::
            where("user_id",$this->user_id)->
            where("add_country","=","Egypt")->
            get();
        }

        return view("user.subviews.order.checkout",$this->data);
    }

    public function add_new_address(Request $request){
        $output=[];

        $validator=\Validator::make(
            [
                "add_email"=>$request->get("add_email"),
                "add_full_name"=>$request->get("add_full_name"),
                "add_governorate"=>$request->get("add_governorate"),
                "add_city"=>$request->get("add_city"),
                "add_country"=>$request->get("add_country"),
                "add_street"=>$request->get("add_street"),
                "add_type"=>$request->get("add_type"),
                "add_tel_number"=>$request->get("add_tel_number"),
                "add_notes"=>$request->get("add_notes"),
            ],
            [
                "add_email"=>"required",
                "add_full_name"=>"required",
                "add_governorate"=>"required",
                "add_city"=>"required",
                "add_country"=>"required",
                "add_street"=>"required",
                "add_type"=>"required",
                "add_tel_number"=>"required",
            ]
        );

        if($validator->messages()->count()>0){
            $output["msg"]="<div class='alert alert-danger'>".implode(".<br>",$validator->messages()->all())."</div>";
            echo json_encode($output);
            return;
        }


        $request["user_id"]=$this->user_id;
        $address=user_address_list_m::create($request->all());

        $currency_symbol=($address->add_country=="Egypt"?"EGP":"USD");

        $output["new_address_html"]=View::
        make("user.subviews.order.checkout_components.address_radio")->with([
            "address"=>$address,
            "key"=>"1",
            "currency_symbol"=>$currency_symbol
        ])->render();


        $output["msg"]="";
        $output["fire_class"]=".select_add_outside_country_list_".$currency_symbol;
        echo json_encode($output);
    }

    public function post_bill(Request $request){
        checkout::post_bill($request,$this->user_id,"user");
    }

}
