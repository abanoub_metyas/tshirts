<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\user_controller;
use App\models\bills\bill_orders_m;
use App\models\bills\bills_m;
use App\models\order_m;
use App\models\product\product_m;
use App\models\subscribe_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class dashboard extends user_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        return view("user.subviews.dashboard",$this->data);
    }

    public function edit_data(Request $request){
        $user_obj = $this->data["current_user"];

        if ($request->method() == "POST") {
            $edit_arr = [];

            $this->validate($request, [
                "full_name" => "required",
                "email" => "required|email|unique:users,email," . $user_obj->user_id . ",user_id,deleted_at,NULL",
            ]);


            $edit_arr["full_name"] = ($request["full_name"]);
            $edit_arr["username"] = ($request["full_name"]);
            $edit_arr["email"] = ($request["email"]);


            // make edit
            User::find($user_obj->user_id)->update($edit_arr);

            return Redirect::to("user/dashboard")->with(
                ["msg" => "<div class='alert alert-success'>Done</div>"]
            )->send();
        }
    }


    public function change_password(Request $request){
        $user_obj = $this->data["current_user"];

        if ($request->method() == "POST") {

            $this->validate($request,[
                "old_password" => "required",
                "password" => "required|min:3|confirmed",
            ]);

            $old_password = clean($request->get("old_password"));
            $new_password = clean($request->get("password"));


            //check old password is correct or not
            if(crypt($old_password, $user_obj->password)!=$user_obj->password)
            {
                return Redirect::to("user/dashboard")->with(
                    ["msg"=>"<div class='alert alert-danger'>invalid old password</div>"]
                )->send();
            }

            $edit_arr["password"] = bcrypt(clean($request["password"]));

            // make edit
            User::find($user_obj->user_id)->update($edit_arr);

            return Redirect::to("user/dashboard")->with(
                ["msg" => "<div class='alert alert-success'>Done</div>"]
            )->send();
        }
    }


    public function orders()
    {

        $slider_arr = array();

        $this->general_get_content([
            "account_orders_page"
        ],$slider_arr);

        $this->data["meta_title"]=$this->data["account_orders_page"]->meta_title;
        $this->data["meta_desc"]="";
        $this->data["meta_keywords"]="";

        $user_obj = $this->data["current_user"];
        $lang_url_segment = $this->data["lang_url_segment"];
        $user_id = $user_obj->user_id;

        $this->data["user_products"] = [];
        $this->data["user_orders"] = [];

//        $get_user_orders = order_m::where("user_id",$user_id)->get();
//        $get_user_orders = $get_user_orders->all();

        $get_user_orders=[];
        if (is_array($get_user_orders) && count($get_user_orders))
        {
            $this->data["user_orders"] = collect($get_user_orders)->groupBy("pro_id");

            $get_user_order_pro_ids = convert_inside_obj_to_arr($get_user_orders,"pro_id");
            $get_user_order_products = product_m::get_all_pros(
                $additional_where = " AND pro.pro_id in (".implode(',',$get_user_order_pro_ids).") ",
                $order_by = " order by pro.created_at desc " ,
                $limit = "",
                $make_it_hierarchical=false,
                $default_lang_id=$this->lang_id);

            $this->data["user_products"] = $get_user_order_products;
        }

        return view("user.subviews.account.orders",$this->data);
    }


}
