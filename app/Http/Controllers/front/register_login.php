<?php

namespace App\Http\Controllers\front;


use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;


class register_login extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function login(Request $request)
    {

        if (is_object(Auth::user())) {

            if (in_array(Auth::user()->user_type, ["admin", "dev"])) {
                return Redirect::to('admin/dashboard')->send();
            }
            elseif (in_array(Auth::user()->user_type, ["user","printing_operator","service_operator","youtuber"])) {
                return Redirect::to(Auth::user()->user_type.'/dashboard')->send();
            }
        }

        if($request->method()=="POST"){


            $email_login = \Auth::attempt([
                "email" => $request->get("email"),
                "password" => $request->get("password"),
            ], $request->get("remember"));


            if ($email_login) {
                Auth::login(\Auth::user());
                $user_obj = \Auth::user();

                if(!$user_obj->user_active  || $user_obj->pause_user == 1){
                    \Auth::logout();
                    $msg = "<div class='alert alert-danger'>Your Account Not Active Or Pause</div>";
                    $this->data['msg'] = $msg;
                    return view("front.subviews.login_view", $this->data);
                }

                $request->session()->save();


                if (in_array($user_obj->user_type, ["admin", "dev"])) {
                    return redirect()->intended('admin/dashboard');
                }
                elseif (in_array($user_obj->user_type, ["user","printing_operator","service_operator","youtuber"])) {
                    return redirect()->intended($user_obj->user_type.'/dashboard');
                }

            }
            else
            {
                \Auth::logout();
                $msg = "<div class='alert alert-danger'>Invalid username or Password!</div>";
                $this->data['msg'] = $msg;
                return view("front.subviews.login_view", $this->data);
            }

        }


        return view("front.subviews.login_view", $this->data);
    }


    public function register(Request $request)
    {
        if($request->method()=="POST"){


            $this->validate($request,
                [
                    "password" => 'required|confirmed',
                    "email" => "required|email|unique:users,email,null,user_id,deleted_at,NULL",
                    "full_name" => "required",
                ]);


            $request["username"]=$request["full_name"];
            $request["password"] = bcrypt($request["password"]);

            $request["user_type"] = "user";
            $request["user_active"] = "1";
            $request["user_can_login"] = "1";

            $check = User::create($request->all());

            $this->data["msg"] = "<div class='alert alert-info'> you can login now </div>";

            return Redirect::to('login')->with([
                "msg"=>$this->data["msg"]
            ])->send();
        }


        return view("front.subviews.register", $this->data);
    }


}
