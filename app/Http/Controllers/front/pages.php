<?php

namespace App\Http\Controllers\front;

use App\models\attachments_m;
use App\models\category\category_m;
use App\models\product\product_m;
use App\User;
use Illuminate\Http\Request;
use App\models\langs_m;
use App\models\pages\pages_m;
use App\models\pages\pages_translate_m;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class pages extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function homepage(){

        //get all sub cats
        $product_sub_cats=category_m::get_all_cats("
            AND parent_cat.deleted_at is null
            AND cat.deleted_at is null
            AND cat.parent_id > 0
            AND cat.hide_cat=0
            AND parent_cat.hide_cat=0
            
            order by cat.cat_order
        ");

        $product_sub_cats=collect($product_sub_cats);

        $products=collect([]);

        if($product_sub_cats->count()>0){
            $products=product_m::get_all_pros("
                AND pro.cat_id in (".implode(",",$product_sub_cats->pluck("cat_id")->all()).")
            ");

            $products=collect($products)->groupBy("cat_id");
        }


        $this->data["product_sub_cats"]=array_chunk($product_sub_cats->all(),2);
        $this->data["products"]=$products;

        return view('front.subviews.index', $this->data);
    }

    public function search(Request $request){

        $search_keyword=$request->get("search_keyword");

        if(empty($search_keyword)){
            return redirect()->back();
        }

        $this->data["search_keyword"]=$search_keyword;

        $product_objs=product_m::get_all_pros("
            AND 
            (
                pro_translate.pro_name like '%".$search_keyword."%'
                or pro_translate.pro_search_tags like '%".$search_keyword."%'
            )
        ");

        $this->data["products"]=$product_objs;

        return view('front.subviews.pages.search_page',$this->data);
    }

    public function show_page(Request $request){

        $page_id=$request->get("page_id");

        if(empty($page_id))return abort(404);

        $page_data=pages_m::get_pages(
            " 
                AND page.page_id = '$page_id' 
            ",
            $order_by = "" ,
            $limit = "",
            $check_self_translates = false,
            $default_lang_id=$this->lang_id
        );


        if (!isset_and_array($page_data)) {
            return abort(404);
        }
        $page_data = $page_data[0];


        $this->data["page_data"] = $page_data;

        $this->data["meta_title"] = $page_data->page_meta_title;
        $this->data["meta_desc"] = $page_data->page_meta_desc;
        $this->data["meta_keywords"] = $page_data->page_meta_keywords;

        return view('front.subviews.pages.index', $this->data);
    }

    public function youtuber_profile(Request $request,$name){

        $youtuber_obj=User::where("full_name",$name)->get()->first();

        if(!is_object($youtuber_obj))return abort(404);

        $youtuber_obj->update([
            "youtuber_views"=>($youtuber_obj->youtuber_views+1)
        ]);

        $youtuber_obj->cover_obj=attachments_m::find($youtuber_obj->cover_id);
        $youtuber_obj->logo_obj=attachments_m::find($youtuber_obj->logo_id);


        $products=product_m::get_all_pros("
            AND pro.youtuber_id=user_id
        ");

        $products=collect($products);

        $this->data["youtuber_obj"]=$youtuber_obj;
        $this->data["products"]=$products;

        $this->data["meta_title"]=$youtuber_obj->meta_title;
        $this->data["meta_desc"]=$youtuber_obj->meta_desc;
        $this->data["meta_keywords"]=$youtuber_obj->meta_keywords;

        return view('front.subviews.youtuber_profile', $this->data);
    }

}
