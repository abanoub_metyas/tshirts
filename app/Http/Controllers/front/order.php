<?php

namespace App\Http\Controllers\front;

use App\models\bills\bill_orders_m;
use App\models\bills\bills_m;
use App\models\confirmation_numbers_m;
use App\models\notification_m;
use App\models\order_m;
use App\models\product\product_m;
use App\models\product\product_quantities_m;
use App\models\settings_m;
use App\models\coupons_m;
use App\models\user_addresses_m;
use App\User;
use Auth;
use Carbon\Carbon;
use App\checkout\checkout;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;

class order extends Controller
{

    public function __construct(){
        parent::__construct();
    }

    public function add_to_old_bill($bill_id,Request $request){

        $pro_id=$request->get("pro_id");
        $pro_quantity=$request->get("pro_quantity");
        $quantity_id=$request->get("quantity_id");
        $selected_product_size=$request->get("selected_product_size");

        $bill_obj=bills_m::findOrFail($bill_id);

        $pro_obj=product_m::findOrFail($pro_id);

        if($pro_obj->youtuber_id!=$bill_obj->youtuber_id){
            return "You can not add this product to this bill because it it not belong to the same youtuber";
        }

        $bill_order_obj=bill_orders_m::where([
            'bill_id'=>$bill_id,
            'pro_id'=>$pro_id,
            'pro_quan_id'=>$quantity_id,
            'order_selected_size'=>$selected_product_size,
        ])->get()->first();

        if(is_object($bill_order_obj)){
            $bill_order_obj->update([
                "order_quantity"=>($bill_order_obj->order_quantity+$pro_quantity)
            ]);
        }
        else{
            bill_orders_m::create([
                'bill_id'=>$bill_id,
                'pro_id'=>$pro_id,
                'pro_quan_id'=>$quantity_id,
                'order_selected_size'=>$selected_product_size,
                'order_quantity'=>$pro_quantity
            ]);
        }



        bills_m::update_bill_cost($bill_id);

        return "added to order";
    }

    public function order_item(Request $request) {
        $output=[];

        $add_to_old_bill=$request->session()->get("add_product_to_bill");
        if($add_to_old_bill>0){
            $output["add_to_old_bill"]=$this->add_to_old_bill($add_to_old_bill,$request);

            if($output["add_to_old_bill"]=="added to order"){
                $request->session()->forget('add_product_to_bill');
            }

            echo json_encode($output);
            return ;
        }

        $pro_id=$request->get("pro_id");
        $pro_quantity=$request->get("pro_quantity");
        $quantity_id=$request->get("quantity_id");
        $selected_product_size=$request->get("selected_product_size");

        // get pro_data
        $pro_obj = product_m::findOrFail($pro_id);
        $product_quantity=product_quantities_m::findOrFail($quantity_id);


        $cart_items=$request->session()->get("cart_items");
        $cart_index=$pro_id."_".$selected_product_size."_".$quantity_id;

        if(isset($cart_items[$cart_index])){
            $cart_items[$cart_index]["pro_quantity"]=$cart_items[$cart_index]["pro_quantity"]+1;
        }
        else{
            $cart_items[$cart_index]=[
                "pro_id"=>$pro_id,
                "pro_quantity"=>$pro_quantity,
                "quantity_id"=>$quantity_id,
                "selected_product_size"=>$selected_product_size,
                "egypt_or_outside"=>$product_quantity->egypt_or_outside
            ];
        }

        $output["done"]="yes";

        $cart_items_collection=collect($cart_items);
        if($cart_items_collection->groupBy("egypt_or_outside")->count()>1){
            $cart_items=[];
            $output["done"]="unexpected error happen.pleas re add product to the cart";
        }

        $request->session()->put("cart_items",$cart_items);
        $request->session()->save();

        echo json_encode($output);
    }

    public function order_status(Request $request){

        $order_id=$request->get("order_id");
        $order_status=$request->get("order_status");

        $this->data["order_id"]=$order_id;
        $this->data["order_status"]=$order_status;
        $this->data["order_msg"]=$request->get("msg");


        return view("front.subviews.order.order_status",$this->data);
    }

    public function remove_item(Request $request){

        $pro_id=$request->get("pro_id");
        $quantity_id=$request->get("quantity_id");
        $selected_size=$request->get("selected_size");

        $cart_items=\Session::get("cart_items");

        $cart_index=$pro_id."_".$selected_size."_".$quantity_id;

        if(!isset($cart_items[$cart_index]))return;

        unset($cart_items[$cart_index]);

        $request->session()->put("cart_items",$cart_items);
        $request->session()->save();
    }

    public function change_pro_quantity(Request $request){
        $pro_id=clean($request->get("pro_id"));
        $new_quantity=(int)$request->get("new_quantity");
        $quantity_id=$request->get("quantity_id");
        $selected_size=$request->get("selected_size");

        $output=[];
        $output["msg"]="";


        $cart_items=\Session::get("cart_items");

        $cart_index=$pro_id."_".$selected_size."_".$quantity_id;

        if(!isset($cart_items[$cart_index]))return;

        $cart_items[$cart_index]["pro_quantity"]=$new_quantity;

        $request->session()->put("cart_items",$cart_items);
        $request->session()->save();

        echo json_encode($output);
    }

    public function check_coupon(Request $request){

        $output=[];

        $coupon=clean($request->get("check_coupon_text"));

        //check coupon is not used or not

        $coupon_poj=coupons_m::
        where("coupon_code",$coupon)->
        where("coupon_end_date",">=",Carbon::now())->
        where("is_used","0")->
        get()->first();

        if(!is_object($coupon_poj)){
            $output["msg"]="<div class='alert alert-danger'>Coupon is invalid</div>";
            echo json_encode($output);
            return;
        }


        $output["msg"]="
                    <div class='alert alert-success'>
                    Coupon is valid,Coupon Value is 
                    $coupon_poj->coupon_value ".(($coupon_poj->is_rate)?" %":" USD").
            "</div>";
        $output["coupon"]=$coupon;

        $output["coupon_type"]=$coupon_poj->is_rate?"percentage":"amount";
        $output["coupon_value"]=$coupon_poj->coupon_value;

        echo json_encode($output);
    }

    public function checkout_orders(Request $request) {

        if(
            is_object($this->data["current_user"])&&
            in_array($this->data["current_user"]->user_type,["user","service_operator"])

        ){
            return \redirect("user/checkout")->send();
        }


        $this->data["checkout_page"]="";

        $cart_items_data=checkout::get_cart_items_data("visitor");

        if($cart_items_data=="egypt_and_outside"){
            $request->session()->put("cart_items",[]);
            $request->session()->save();
            return \redirect("/")->with("msg",
                "<div class='alert alert-info'>unexpected error happen.pleas re add product to the cart</div>"
            )->send();
        }

        $this->data["items_count"]=array_sum($cart_items_data["cart_items_collection"]->pluck("quantity_id")->all());

        $this->data=array_merge($this->data,$cart_items_data);

        return view("front.subviews.order.checkout",$this->data);
    }

    public function post_bill(Request $request){

        //get visitor obj
        $vistor_obj=User::where("email","vistor@site.com")->
        get()->first();

        if(!is_object($vistor_obj)){
            $vistor_obj=User::create([
                'email'=>"vistor@site.com",
                'user_type'=>"user",
                'username'=>"visitor",
                'full_name'=>"visitor",
            ]);
        }

        checkout::post_bill($request,$vistor_obj->user_id,"visitor");
    }

    public function complete_payment(Request $request){

        $bill_id=$request->get("bill_id");
        $bill_obj=bills_m::findOrFail($bill_id);

        $iframe_link=checkout::online_payment($bill_obj);
        $this->data["iframe_link"]=$iframe_link;

        return view("front.subviews.order.complete_payment",$this->data);
    }

    public function accept_payment_notification(Request $request){

        if($request->method()=="POST"){
            if(!checkout::post_check_hmac($request)){
                abort(404);
            }

            $data_arr=$request->get("obj");
            $order_id=$data_arr["order"]["id"];

            $bil_obj=bills_m::
            where("api_order_id",$order_id)->
            get()->first();

            if(!is_object($bil_obj))return abort(404);

            $order_status=$data_arr["success"];
            $amount_cents=($data_arr["amount_cents"])/100;

            if($order_status==true){
                $bil_obj->update([
                    "bill_paid_amount"=>($bil_obj->bill_paid_amount+$amount_cents)
                ]);
            }

        }
        else{
            if(!checkout::get_check_hmac($request)){
                abort(404);
            }

            //order.id
            $order_id=$request->get("order");

            $bil_obj=bills_m::
            where("api_order_id",$order_id)->
            get()->first();

            if(!is_object($bil_obj))return abort(404);

            $order_status=$request->get("success");

            return redirect('/order_status?order_id='.$bil_obj->bill_id.'&order_status='.($order_status?"done":"failed"))->
            with("msg","<div class='alert alert-info'>Thank you</div>")->send();
        }


    }

    public function accept_payment_kiosk_notification(Request $request){

        \Log::useFiles(base_path() . '/storage/logs/payment_method_2.log', 'info');
        \Log::info($request->all());


        if($request->get("test")=="test"){
            return checkout::online_payment_kiosk(bills_m::get_bill(1));
        }


        if($request->method()=="POST"){
            if(!checkout::post_check_hmac($request)){
                abort(404);
            }

            $data_arr=$request->get("obj");
            $order_id=$data_arr["order"]["id"];

            $bil_obj=bills_m::
            where("api_order_id",$order_id)->
            get()->first();

            if(!is_object($bil_obj))return abort(404);

            $order_status=$data_arr["success"];
            $amount_cents=($data_arr["amount_cents"])/100;

            if($order_status==true){
                $bil_obj->update([
                    "bill_paid_amount"=>($bil_obj->bill_paid_amount+$amount_cents)
                ]);
            }

        }
        else{
            if(!checkout::get_check_hmac($request)){
                abort(404);
            }

            //order.id
            $order_id=$request->get("order");

            $bil_obj=bills_m::
            where("api_order_id",$order_id)->
            get()->first();

            if(!is_object($bil_obj))return abort(404);

            $order_status=$request->get("success");

            return redirect('/order_status?order_id='.$bil_obj->bill_id.'&order_status='.($order_status?"done":"failed"))->
            with("msg","<div class='alert alert-info'>Thank you</div>")->send();
        }


    }

}
