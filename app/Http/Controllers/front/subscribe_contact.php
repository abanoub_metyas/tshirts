<?php

namespace App\Http\Controllers\front;

use App\helpers\utility;
use App\models\notification_m;
use App\models\subscribe_m;
use App\models\support_messages_m;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class subscribe_contact extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $slides=[];
        $slides["contact_us"]=["slider1"];
        $this->general_get_content(["contact_us"],$slides);

        $this->data["meta_title"]=show_content($this->data["contact_us"],"header");
        $this->data["meta_desc"]=show_content($this->data["contact_us"],"header");
        $this->data["meta_keywords"]=show_content($this->data["contact_us"],"header");

        return view("front.subviews.support",$this->data);
    }

    public function subscribe(Request $request)
    {
        $output["error"] = "";

        $request["email"] = clean($request["email"]);

        $validator = Validator::make(
            [
                "email" => $request["email"]
            ],
            [
                "email" => "email|required|unique:subscribe,email"
            ]
        );

        if (count($validator->messages()) == 0)
        {
            $check = subscribe_m::create($request->all());
            if(is_object($check))
            {
                $output["error"] = "";
                $output["error_msg"] = "Thank You";
            }
            else{
                $output["error"] = "error";
            }
        }
        else{
            $output["error"] = "error";
            $output["error_msg"] = $validator->messages();
        }

        return json_encode($output);

    }

    public function make_a_contact(Request $request)
    {

        $output=array();
        $output["type"] = "info";
        $output["msg"] = "";
        $output["request_id"] = "";
        \Debugbar::disable();


        $validator = Validator::make(

            [
                "email" => $request["email"],
                "message" => $request["message"],
            ],

            [
                "email" => "required|email",
                "message" => "required",
            ]
        );

        if (count($validator->messages()) == 0)
        {
            $inputs=Input::all();

            try{
                $ip=get_client_ip();
                $inputs["country"] = ip_info($ip)['country'];
            }catch(Exception $e){
                $inputs["country"]="";
            }

            $inputs["source"]=Cookie::get('source');
            if($inputs["source"]==null){
                $inputs["source"]="";
            }

            $inputs["first_name"]=$request->get("first_name","");
            $inputs["last_name"]=$request->get("last_name","");
            $inputs["name"]=$inputs["first_name"]." ".$inputs["last_name"];

            $inputs["telephone"]=(isset($inputs["tel"]))?clean($inputs["tel"]):"";
            $inputs["message"]=(isset($inputs["message"]))?clean($inputs["message"]):"";
            $inputs["title"]=(isset($inputs["title"]))?clean($inputs["title"]):"";
            $inputs["phone"]=(isset($inputs["phone"]))?clean($inputs["phone"]):"";
            $inputs["message_type"]=(isset($inputs["msg_type"]))?clean($inputs["msg_type"]):"";
            $inputs["current_url"]=(isset($inputs["current_url"]))?clean($inputs["current_url"]):"";
            $inputs["source"]=(isset($inputs["source"]))?clean($inputs["source"]):"";
            $inputs["other_data"]="";



            $support_message_obj=support_messages_m::create($inputs);
            $output["request_id"] = $support_message_obj->id;

            unset($inputs["_token"],$inputs["source"]);

            //send email to admins and to user email

            if(is_object($support_message_obj)){

//                unset($inputs["g_recaptcha_response"]);

                // send notification to admins
                utility::send_notification_to_users(
                    utility::get_admins(true),
                    $inputs["name"]." make a contact request... - ".$inputs["message_type"],
                    "info"
                );

                //1)admin
                $admin_email_body=\View::make("email.admin.send_contact_us_info")->with([
                    "contact_us_data"=>$inputs,
                    "email_main_layout"=>$this->data['email_main_layout']
                ])->render();


                utility::send_email_to_custom(
                    $emails = utility::get_admins()->pluck("email")->all(),
                    $data = $admin_email_body,
                    $subject = "GICT - Contact Us Message  N.$support_message_obj->id"." - ".$inputs["message_type"]
                );

                //2)to user
                $user_email_body=\View::make("email.user.contact_us_confirmation")->with([
                    "contact_us_data"=>$inputs,
                    "email_main_layout"=>$this->data['email_main_layout']
                ])->render();

                utility::send_email_to_custom(
                    $emails = [$support_message_obj->email],
                    $data = $user_email_body ,
                    $subject = "GICT - Your Message Is Sent Successfully"
                );

                $output["type"] = "success";
                $output["msg"] = "<div class='alert alert-success'>".show_content($this->data["homepage"],"contact_us_success_message")."</div>";
            }

        }
        else{
            $output["type"] = "warning";

            foreach ($validator->messages()->all() as $key => $msg) {
                $output["msg"].=$msg."<br>";
            }

        }

        echo json_encode($output);
    }

}
