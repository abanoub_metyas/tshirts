<?php

namespace App\Http\Controllers\front;

use App\models\category\category_m;
use App\models\product\product_m;
use App\models\product\product_quantities_m;
use App\models\product\product_reviews_m;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class category extends Controller
{

    public function __construct(){
        parent::__construct();
    }


    public function show_parent_cat(Request $request){

        $cat_id=$request->get("cat_id");
        if(empty($cat_id))return abort(404);


        $cat_obj=category_m::get_all_cats(" 
            AND cat.cat_id=$cat_id
            AND cat.hide_cat=0
        ");


        if(!isset_and_array($cat_obj))return abort(404);
        $cat_obj=$cat_obj[0];


        $this->data["cat_obj"]=$cat_obj;

        $this->data["child_cats"]=category_m::get_all_cats("
            AND cat.parent_id=$cat_id;
            AND cat.hide_cat=0
        ");


        $this->data["meta_title"]=$cat_obj->cat_meta_title;
        $this->data["meta_desc"]=$cat_obj->cat_meta_desc;
        $this->data["meta_keywords"]=$cat_obj->cat_meta_keywords;


        return view('front.subviews.category.show_parent_cat',$this->data);
    }

    public function show_child_cat(Request $request){

        $cat_id=$request->get("cat_id");
        if(empty($cat_id))return abort(404);


        $cat_obj=category_m::get_all_cats(" 
            AND cat.cat_id=$cat_id
        ");


        if(!isset_and_array($cat_obj))return abort(404);
        $cat_obj=$cat_obj[0];


        $this->data["cat_obj"]=$cat_obj;

        $this->data["cat_products"]=product_m::get_all_pros("
            AND pro.cat_id=$cat_id;
        ");


        $this->data["meta_title"]=$cat_obj->cat_meta_title;
        $this->data["meta_desc"]=$cat_obj->cat_meta_desc;
        $this->data["meta_keywords"]=$cat_obj->cat_meta_keywords;


        return view('front.subviews.category.show_child_cat',$this->data);
    }

    public function show_product(Request $request){


        $pro_id=$request->get("pro_id");
        if(empty($pro_id))return abort(404);

        //pro_data
        $pro_data=product_m::get_all_pros(
            $additional_where = " AND pro.pro_id=$pro_id",
            $order_by = "" ,
            $limit = "",
            $make_it_hierarchical=false,
            $default_lang_id=$this->lang_id
        );

        if(is_array($pro_data)&&count($pro_data)){
            $pro_data=$pro_data[0];
        }
        else{
            return abort(404);
        }


        $pro_data->pro_stars=product_reviews_m::get_pro_avg($pro_id);

        $this->data["pro_data"]=$pro_data;

        //check if user is enter from egypt or not

        $egypt_or_not="outside";
        if(
            isset(ip_info(get_client_ip())["country"])&&
            ip_info(get_client_ip())["country"]=="Egypt"
        ){
            $egypt_or_not="egypt";
        }

        $this->data["currency_symbol"]=($egypt_or_not=="egypt")?"EGP":"USD";

        // product quantities
        $product_quantities = product_quantities_m::get_product_quantities($pro_id,$egypt_or_not);
        $this->data['pro_quantities']=$product_quantities;


        $this->data["product_reviews"]=product_reviews_m::
        where("pro_id",$pro_id)->get();


        $this->data["related_products"] = product_m::get_all_pros(
            $additional_where = " 
                AND pro.youtuber_id=$pro_data->youtuber_id
                AND pro.pro_id!=$pro_id
            ",
            $order_by = " group by pro.pro_id",
            $limit = "limit 12",
            $make_it_hierarchical = false,
            $default_lang_id = $this->lang_id
        );



        $this->data["meta_title"]=$pro_data->pro_meta_title;
        $this->data["meta_desc"]=$pro_data->pro_meta_desc;
        $this->data["meta_keywords"]=$pro_data->pro_meta_keywords;


        return view('front.subviews.category.product',$this->data);
    }

    public function submit_review(Request $request){

        $pro_id=$request->get("pro_id");
        if(empty($pro_id))return abort(404);

        product_m::findOrFail($pro_id);

        $rev_name=$request->get("rev_name");
        $rev_text=$request->get("rev_text");
        $rev_rate=$request->get("rev_rate");

        if(
            empty($rev_name)||
            empty($rev_text)
        ){
            return abort(404);
        }

        product_reviews_m::create([
            'pro_id'=>$pro_id,
            'review_name'=>$rev_name,
            'review_text'=>$rev_text,
            'review_rate'=>$rev_rate
        ]);

        return redirect("show_product?pro_id=$pro_id")->with([
            "msg"=>"<div class='alert alert-info'>Done</div>"
        ])->send();
    }

}
