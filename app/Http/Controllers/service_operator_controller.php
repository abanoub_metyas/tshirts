<?php

namespace App\Http\Controllers;

use App\helpers\network_utilities;
use App\models\attachments_m;
use App\models\notification_m;
use App\models\pages\pages_m;
use App\models\support_messages_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use SSP;

class service_operator_controller extends dashbaord_controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware("check_service_operator");
    }

}
