<?php

namespace App\Http\Controllers\printing_operator;

use App\Http\Controllers\printing_operator_controller;
use App\models\materials_m;
use Illuminate\Http\Request;
use App\User;

class dashboard extends printing_operator_controller
{

    public function __construct(){
        parent::__construct();
    }

    public function index(Request $request)
    {


        //get critical materials
        $critical_materials=
            materials_m::
            whereRaw("m_critical_limit >= m_quantity")->
            get();

        $this->data["critical_materials"]=$critical_materials;


        return view("printing_operator.subviews.dashboard",$this->data);
    }



}
