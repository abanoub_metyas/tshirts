<?php

namespace App\Http\Controllers\printing_operator;

use App\Http\Controllers\admin_controller;
use App\Http\Controllers\printing_operator_controller;
use App\models\bills\bill_orders_m;
use App\models\bills\bills_m;
use App\models\category_m;
use App\Http\Controllers\dashbaord_controller;
use App\models\category_translate_m;
use App\models\materials_m;
use App\models\order_m;
use App\models\printing\printed_tshirts_m;
use App\models\printing\printing_requests_m;
use App\models\user_address_list_m;
use App\User;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class orders extends printing_operator_controller
{


    public function __construct()
    {
        parent::__construct();


        $this->data["order_status"]=$this->order_status;
        $this->data["print_status"]=$this->print_status;
    }

    public function index(Request $request)
    {

        $this->data["post_data"]=(object)$request->all();
        $this->data["all_bills"]=bills_m::get_bills($request->all());

        return view("printing_operator.subviews.orders.show")->with($this->data);
    }

    public function bill_orders($bill_id){
        $bill_obj=bills_m::findOrFail($bill_id);

        $this->data["bill_obj"] = $bill_obj;
        $this->data["user_obj"] = User::findOrFail($bill_obj->user_id);
        $this->data["youtuber_obj"] = User::findOrFail($bill_obj->youtuber_id);
        $address_obj = user_address_list_m::findOrFail($bill_obj->address_id);
        $this->data["address_obj"] = $address_obj;

        $bill_orders=
            bill_orders_m::
            select(DB::raw("
                bill_orders.*,
                product_translate.pro_name,
                product_quantities.pro_color,
                product_quantities.pro_available_sizes,
                product_quantities.pro_price,
                product.pro_material_type,
                attachments.path as 'small_img_path'
            "))->
            join("product","product.pro_id","=","bill_orders.pro_id")->
            join("product_translate",function($join){
                $join->on("product.pro_id","=","product_translate.pro_id")->
                where("product_translate.lang_id","=","1");
            })->
            join("product_quantities","product_quantities.pq_id","=","bill_orders.pro_quan_id")->
            leftJoin("attachments","attachments.id","=","product.small_img_id")->
            where("bill_id",$bill_id)->
            get();


        $materials=materials_m::
        whereIn("m_name",$bill_orders->pluck("pro_material_type")->all())->
        whereIn("m_size",$bill_orders->pluck("order_selected_size")->all())->
        whereIn("m_color",$bill_orders->pluck("pro_color")->all())->
        get()->groupBy("m_name");


        $printing_requests=
            printing_requests_m::
            whereIn("bill_order_id",$bill_orders->pluck("bill_order_id")->all())->
            get()->groupBy("bill_order_id");


        $printed_tshirts=printed_tshirts_m::
        whereIn("pro_id",$bill_orders->pluck("pro_id")->all())->
        whereIn("pt_size",$bill_orders->pluck("order_selected_size")->all())->
        whereIn("pt_color",$bill_orders->pluck("pro_color")->all())->
        get()->groupBy("pro_id");

        $this->data["bill_orders"]=$bill_orders;
        $this->data["materials"]=$materials;
        $this->data["printing_requests"]=$printing_requests;
        $this->data["printed_tshirts"]=$printed_tshirts;

        return view("printing_operator.subviews.orders.show_bill_orders")->with($this->data);
    }

    public function remove_order(Request $request){

        $this->general_remove_item($request,'App\models\order_m');
    }

}
