<?php

namespace App\Http\Controllers\printing_operator;

use App\helpers\utility;
use App\Http\Controllers\printing_operator_controller;
use App\models\bills\bills_m;
use App\models\materials_m;
use App\models\printing\printing_requests_m;
use Illuminate\Http\Request;
use App\User;

class printing_requests extends printing_operator_controller
{

    public function __construct(){
        parent::__construct();
    }

    public function show_all(Request $request)
    {
        $this->data["post_data"]=(object)$request->all();
        $this->data["all_requests"]=printing_requests_m::get_requests($request->all());

        return view("printing_operator.subviews.printing_requests.show_all",$this->data);
    }

    public function change_status(Request $request){

        $request_id=$request->get("request_id");
        $printing_request_obj=printing_requests_m::findOrFail($request_id);

        $this->data["printing_request_obj"]=$printing_request_obj;

        if($request->method()=="POST"){

            $req_status = $request->get("print_status");

            if(!in_array($req_status,$this->print_status)){
                return abort(404);
            }

            if(
                $req_status=="pending"&&
                in_array($printing_request_obj->print_status,["accepted","done"])
            ){
                return \redirect("/printing_operator/printing_requests/change_status?request_id=$request_id")->with("msg","
                    <div class='alert alert-info'>you can not make it pending because you've accepted it before</div>
                ")->send();
            }


            if(in_array($req_status,["accepted","done"])&&$printing_request_obj->print_status=="pending"){
                $material_obj=materials_m::findOrFail($printing_request_obj->material_id);

                $material_obj->update([
                    "m_quantity"=>($material_obj->m_quantity-$printing_request_obj->request_quantity)
                ]);


                if($material_obj->m_critical_limit>$material_obj->m_quantity){
                    //send notification to admin and printing operators
                    utility::send_notification_to_users(
                        $user_ids=utility::get_admins(["admin","dev","printing_operator"],true),
                        $not_title="material is in low quantity",
                        $not_type="info"
                    );

                }

            }


            //send notification to the customer service
            utility::send_notification_to_users(
                $user_ids=utility::get_admins(["service_operator"],true),
                $not_title="printing operator made an action at bill number ".$printing_request_obj->bill_id,
                $not_type="info"
            );

            $printing_request_obj->update([
                "print_status"=>$req_status
            ]);

            return \redirect("/printing_operator/printing_requests/change_status?request_id=$request_id")->send();
        }


        return view("printing_operator.subviews.printing_requests.change_status")->with($this->data);
    }


}
