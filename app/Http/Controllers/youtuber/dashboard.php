<?php

namespace App\Http\Controllers\youtuber;

use App\Http\Controllers\user_controller;
use App\Http\Controllers\youtuber_controller;
use App\models\attachments_m;
use App\models\bills\bill_orders_m;
use App\models\bills\bills_m;
use App\models\order_m;
use App\models\product\product_m;
use App\models\product\product_reviews_m;
use App\models\subscribe_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class dashboard extends youtuber_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $logo_id=$this->data["current_user"]->logo_id;
        $cover_id=$this->data["current_user"]->cover_id;

        $this->data["current_user"]->user_img_file=attachments_m::find($logo_id);
        $this->data["current_user"]->cover_img_file=attachments_m::find($cover_id);

        //get statistics
        $youtuber_products_ids=product_m::
        where("youtuber_id",$this->user_id)->get()->
        pluck("pro_id")->all();

        $total_boughts=bill_orders_m::get_youtuber_total_orders($this->user_id);

        $this->data["yotuber_views_count"]=$this->current_user_data->youtuber_views;

        $this->data["total_boughts"]=$total_boughts;

        $this->data["total_reviews_count"]=product_reviews_m::
        whereIn("pro_id",$youtuber_products_ids)->get()->count();

        return view("youtuber.subviews.dashboard",$this->data);
    }

    public function edit_data(Request $request){
        $user_obj = $this->data["current_user"];

        $logo_id=$this->data["current_user"]->logo_id;
        $cover_id=$this->data["current_user"]->cover_id;


        if ($request->method() == "POST") {
            $edit_arr = [];

            $this->validate($request, [
                "full_name" => "required",
                "email" => "required|email|unique:users,email," . $user_obj->user_id . ",user_id,deleted_at,NULL",
            ]);


            $edit_arr["full_name"] = ($request["full_name"]);
            $edit_arr["username"] = ($request["full_name"]);
            $edit_arr["email"] = ($request["email"]);

            $edit_arr["logo_id"] = $this->general_save_img(
                $request ,
                $item_id=$this->user_id,
                "user_img_file",
                $new_title = "",
                $new_alt = "",
                $upload_new_img_check = $request["user_img_checkbox"],
                $upload_file_path = "/admins",
                $width = 0,
                $height = 0,
                $photo_id_for_edit = $logo_id
            );

            $edit_arr["cover_id"] = $this->general_save_img(
                $request ,
                $item_id=$this->user_id,
                "cover_img_file",
                $new_title = "",
                $new_alt = "",
                $upload_new_img_check = $request["cover_img_checkbox"],
                $upload_file_path = "/admins",
                $width = 0,
                $height = 0,
                $photo_id_for_edit = $cover_id
            );


            // make edit
            User::find($user_obj->user_id)->update($edit_arr);

            return Redirect::to("youtuber/dashboard")->with(
                ["msg" => "<div class='alert alert-success'>Done</div>"]
            )->send();
        }
    }

    public function change_password(Request $request){
        $user_obj = $this->data["current_user"];

        if ($request->method() == "POST") {

            $this->validate($request,[
                "old_password" => "required",
                "password" => "required|min:3|confirmed",
            ]);

            $old_password = clean($request->get("old_password"));
            $new_password = clean($request->get("password"));


            //check old password is correct or not
            if(crypt($old_password, $user_obj->password)!=$user_obj->password)
            {
                return Redirect::to("youtuber/dashboard")->with(
                    ["msg"=>"<div class='alert alert-danger'>invalid old password</div>"]
                )->send();
            }

            $edit_arr["password"] = bcrypt(clean($request["password"]));

            // make edit
            User::find($user_obj->user_id)->update($edit_arr);

            return Redirect::to("youtuber/dashboard")->with(
                ["msg" => "<div class='alert alert-success'>Done</div>"]
            )->send();
        }
    }

    public function orders()
    {

        $slider_arr = array();

        $this->general_get_content([
            "account_orders_page"
        ],$slider_arr);

        $this->data["meta_title"]=$this->data["account_orders_page"]->meta_title;
        $this->data["meta_desc"]="";
        $this->data["meta_keywords"]="";

        $user_obj = $this->data["current_user"];
        $lang_url_segment = $this->data["lang_url_segment"];
        $user_id = $user_obj->user_id;

        $this->data["user_products"] = [];
        $this->data["user_orders"] = [];

//        $get_user_orders = order_m::where("user_id",$user_id)->get();
//        $get_user_orders = $get_user_orders->all();

        $get_user_orders=[];
        if (is_array($get_user_orders) && count($get_user_orders))
        {
            $this->data["user_orders"] = collect($get_user_orders)->groupBy("pro_id");

            $get_user_order_pro_ids = convert_inside_obj_to_arr($get_user_orders,"pro_id");
            $get_user_order_products = product_m::get_all_pros(
                $additional_where = " AND pro.pro_id in (".implode(',',$get_user_order_pro_ids).") ",
                $order_by = " order by pro.created_at desc " ,
                $limit = "",
                $make_it_hierarchical=false,
                $default_lang_id=$this->lang_id);

            $this->data["user_products"] = $get_user_order_products;
        }

        return view("youtuber.subviews.account.orders",$this->data);
    }

    public function show_profit_and_loss(Request $request){

        if(empty($request["filter_year"])){
            $request["filter_year"]=date("Y");
        }
        $this->data["post_data"]=(object)$request->all();

        $youtuber_id=$this->user_id;
        $youtuber_obj=User::findOrFail($youtuber_id);

        $profit_and_loss=User::get_youtuber_profit_and_loss($youtuber_id,$request->get("filter_year"));

        $this->data["youtuber_obj"]=$youtuber_obj;
        $this->data["youtuber_profit"]=$profit_and_loss["profit"];
        $this->data["youtuber_loss"]=$profit_and_loss["loss"];

        return view("youtuber.subviews.profit_and_loss",$this->data);
    }

}
