<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin_controller;
use App\Http\Controllers\dashbaord_controller;
use App\models\materials_m;
use Illuminate\Http\Request;

use App\models\product\product_materials_m;
use App\models\product\products_m;
use App\User;

class dashboard extends admin_controller
{

    public function __construct(){
        parent::__construct();
    }

    public function index(Request $request)
    {

        //get critical materials
        $critical_materials=
            materials_m::
            whereRaw("m_critical_limit >= m_quantity")->
            get();

        $this->data["critical_materials"]=$critical_materials;


        return view("admin.subviews.dashboard",$this->data);
    }



}
