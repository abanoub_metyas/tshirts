<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin_controller;
use App\models\category_m;
use App\Http\Controllers\dashbaord_controller;
use App\models\langs_m;
use App\models\materials_m;
use App\models\printing\printing_requests_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class materials extends admin_controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        if (!check_permission($this->user_permissions,"admin/materials","show_action"))
        {
            return  Redirect::to('admin/dashboard')->
            with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->
            send();
        }

        $this->data["all_materials"] = materials_m::all();

        return view("admin.subviews.materials.show")->with($this->data);
    }

    public function save_material(Request $request , $material_id = null)
    {

        if($material_id==null){
            if (!check_permission($this->user_permissions,"admin/materials","add_action"))
            {
                return  Redirect::to('admin/dashboard')->
                with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->
                send();
            }
        }
        else{
            if (!check_permission($this->user_permissions,"admin/materials","edit_action"))
            {
                return Redirect::to('admin/dashboard')->
                with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->
                send();
            }
        }



        $this->data["material_data"] = "";
        $edit_operation=false;

        if ($material_id != null){
            $this->data["material_data"]=materials_m::findOrFail($material_id);
            $edit_operation=true;
        }

        if ($request->method() == "POST")
        {
            $request["m_name_color_size"]=
                $request["m_name"]."_".
                $request["m_color"]."_".
                $request["m_size"];

            $this->validate($request,[
                "m_name" => "required",
                "m_name_color_size" => "unique:materials,m_name_color_size,".$material_id.",m_id,deleted_at,NULL",
            ]);

            $request = clean($request->all());

            if ($material_id!=null)
            {
                materials_m::find($material_id)->update($request);
                $msg = "done";
            }
            else{
                // insert
                $new_obj = materials_m::create($request);
                $material_id = $new_obj->m_id;
                $msg = "done";
            }

            return Redirect::to('admin/materials/save_material/'.($edit_operation?$material_id:""))->
            with(["msg"=>"<div class='alert alert-success'>$msg</div>"])->send();

        }//end submit


        return view("admin.subviews.materials.save")->with($this->data);
    }

    public function remove_material(Request $request){

        if (!check_permission($this->user_permissions,"admin/materials","delete_action"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"]);
            return;
        }

        $this->general_remove_item($request,'App\models\materials_m');
    }


    public function material_usage(Request $request){

        $this->data["post_data"]=(object)$request->all();

        $printing_requests=printing_requests_m::where("print_status","done");

        $start_date=$request->get("start_date");
        $end_date=$request->get("end_date");

        if(!empty($start_date)){
            $printing_requests=$printing_requests->where("printing_requests.created_at",">=",$start_date);
        }

        if(!empty($end_date)){
            $printing_requests=$printing_requests->where("printing_requests.created_at","<=",$end_date);
        }


        $printing_requests=$printing_requests->get()->groupBy("material_id");
        $material_objs=collect([]);

        if($printing_requests->count()){
            $material_ids=array_keys($printing_requests->all());

            $material_objs=materials_m::whereIn("m_id",$material_ids)->
            get()->groupBy("m_id");

        }

        $this->data["printing_requests"]=$printing_requests;
        $this->data["material_objs"]=$material_objs;

        return view("admin.subviews.materials.material_usage")->with($this->data);
    }


}
