<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin_controller;
use App\models\category_m;
use App\Http\Controllers\dashbaord_controller;
use App\models\langs_m;
use App\models\coupons_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class coupons extends admin_controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        if (!check_permission($this->user_permissions,"admin/coupons","show_action"))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->send();
        }

        $this->data["all_coupons"] = coupons_m::all();

        return view("admin.subviews.coupons.show")->with($this->data);
    }

    public function save_coupon(Request $request , $coupon_id = null)
    {

        if($coupon_id==null){
            if (!check_permission($this->user_permissions,"admin/coupons","add_action"))
            {
                return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->send();
            }
        }
        else{
            if (!check_permission($this->user_permissions,"admin/coupons","edit_action"))
            {
                return Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->send();
            }
        }



        $this->data["coupon_data"] = "";


        if ($coupon_id != null){

            $this->data["coupon_data"]=coupons_m::where("coupon_id",$coupon_id)->get()->first();

            if(!is_object($this->data["coupon_data"])){
                return Redirect::to('admin/coupons/save_coupon')->with(["msg"=>"<div class='alert alert-danger'>error</div>"])->send();
            }

        }

        if ($request->method() == "POST")
        {

            $this->validate($request,[

                "coupon_code" => "required|unique:coupons,coupon_code,".$coupon_id.",coupon_id,deleted_at,NULL",
                "coupon_end_date" => "required|date",
                "is_rate" => "required|numeric|min:0",
                "coupon_value" => "required|numeric|min:0",

            ]);


            $request = clean($request->all());

            $request["coupon_end_date"]=date("Y-m-d",strtotime($request["coupon_end_date"]));


            if (isset($coupon_id))
            {
                // update
                coupons_m::find($coupon_id)->update($request);
                $msg = "done";
            }
            else{
                // insert
                $new_obj = coupons_m::create($request);
                $coupon_id = $new_obj->coupon_id;
                $msg = "done";
            }

            return Redirect::to('admin/coupons/save_coupon/'.$coupon_id)->with(["msg"=>"<div class='alert alert-success'>$msg</div>"])->send();

        }//end submit


        return view("admin.subviews.coupons.save")->with($this->data);
    }

    public function remove_coupon(Request $request){

        if (!check_permission($this->user_permissions,"admin/coupons","delete_action"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"]);
            return;
        }

        $this->general_remove_item($request,'App\models\coupons_m');
    }

}
