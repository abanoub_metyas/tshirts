<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin_controller;
use App\Http\Controllers\dashbaord_controller;
use App\models\attachments_m;
use App\models\category\category_m;
use App\models\countries_m;
use App\models\langs_m;
use App\models\materials_m;
use App\models\product\product_m;
use App\models\product\product_quantities_m;
use App\models\product\product_reviews_m;
use App\models\product\product_translate_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class product extends admin_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {

        if (!check_permission($this->user_permissions,"admin/products","show_action"))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->send();
        }

        $cat_id=$request->get("cat_id");
        $youtuber_id=$request->get("youtuber_id");

        $additional_where=[];

        if($cat_id>0){
            $additional_where[]=" AND pro.cat_id=$cat_id ";
        }

        if($youtuber_id>0){
            $additional_where[]=" AND pro.youtuber_id=$youtuber_id";
        }


        $this->data["all_pros"] = product_m::get_all_pros(implode(" ",$additional_where));

        return view("admin.subviews.products.show")->with($this->data);
    }

    public function save_pro(Request $request , $pro_id = null)
    {

        if($pro_id==null){
            if (!check_permission($this->user_permissions,"admin/products","add_action"))
            {
                return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->send();
            }
        }
        else{
            if (!check_permission($this->user_permissions,"admin/products","edit_action"))
            {
                return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->send();
            }
        }

        $this->data["all_youtubers"]=User::get_users(" AND user_obj.user_type='youtuber'");
        $this->data["all_youtubers"]=collect($this->data["all_youtubers"]);

        $this->data["all_parent_cats"]=category_m::get_all_cats(" AND cat.parent_id=0 AND cat.cat_type='product'");
        $this->data["all_child_cats"]=category_m::get_all_cats(" AND cat.parent_id>0 AND cat.cat_type='product'");

        $this->data["all_countries"]=countries_m::all();

        $all_materials=materials_m::all();

        $this->data["all_material_types"]=array_unique($all_materials->pluck("m_name")->all());
        $this->data["all_sizes"]=array_unique($all_materials->pluck("m_size")->all());
        $this->data["all_colors"]=array_unique($all_materials->pluck("m_color")->all());

        $this->data["msg"]="";

        if(count($this->data["lang_ids"])==0){
            redirect("/admin/langs/save_lang");
        }


        $pro_data = "";
        $pro_data_translate_rows=collect([]);
        $small_img_id=0;
        $product_quantities=collect([]);

        if ($pro_id != null){
            $pro_data=product_m::get_all_pros(" AND pro.pro_id=$pro_id");

            if(!isset_and_array($pro_data)){
                abort(404);
            }

            $pro_data=$pro_data[0];

            $child_cat_obj=category_m::find($pro_data->cat_id);

            $pro_data->child_cat_id=$pro_data->cat_id;
            $pro_data->parent_cat_id=$child_cat_obj->parent_id;

            $pro_data->pro_small_img=attachments_m::find($pro_data->small_img_id);

            $pro_data_translate_rows=product_translate_m::where("pro_id",$pro_id)->get();

            $small_img_id=$pro_data->small_img_id;

            $product_quantities =product_quantities_m::
            where("pro_id",$pro_id)->
            leftJoin("attachments","attachments.id","=","product_quantities.pq_img_id")->
            get();
        }


        $this->data["pro_data"]=$pro_data;
        $this->data["pro_data_translate_rows"]=$pro_data_translate_rows;
        $this->data["product_quantities"]=$product_quantities;

        if ($request->method()=="POST")
        {

            $rules_values=[
                "youtuber_id" => $request["youtuber_id"],
                "cat_id" => $request["cat_id"],
                "pro_name" => $request["pro_name"],
                "pro_material_type" => $request["pro_material_type"],
            ];

            $rules_itself=[
                "youtuber_id" => "required",
                "cat_id" => "required",
                "pro_material_type" => "required",
                "pro_name.0" => "required",
            ];


            $request["json_values_of_sliderpro_slider_file"] = json_decode(clean($request->get("json_values_of_sliderpro_slider_file")));

            $request["pro_slider"] = $this->general_save_slider(
                $request,
                $field_name="pro_slider_file",
                $width=0,
                $height=0,
                $new_title_arr = $request["pro_slider_file_title"],
                $new_alt_arr = $request["pro_slider_file_alt"],
                $json_values_of_slider=clean($request->get("json_values_of_sliderpro_slider_file")),
                $old_title_arr = "",
                $old_alt_arr = "",
                $path="/products/slider"
            );

            $request["pro_slider"] = json_encode($request["pro_slider"]);


            $validator = Validator::make($rules_values,$rules_itself);

            if (count($validator->messages()) == 0)
            {

                $inputs=$request->all();

                $inputs["small_img_id"] = $this->general_save_img(
                    $request ,
                    $item_id=$pro_id,
                    "small_img",
                    $new_title = $request["small_imgtitle"],
                    $new_alt = $request["small_imgalt"],
                    $upload_new_img_check = $request->get("small_img_checkbox"),
                    $upload_file_path = "/product",
                    $width = 0, $height = 0,
                    $photo_id_for_edit = $small_img_id
                );

                $pro_obj="";

                if ($pro_id != null){
                    // update
                    $pro_obj=product_m::find($pro_id);
                    $pro_obj->update($inputs);

                    $this->data["msg"] = "<div class='alert alert-success'> done </div>";
                }
                else{
                    // insert
                    $pro_obj = product_m::create($inputs);

                    $this->data["msg"] = "<div class='alert alert-success'> done </div>";
                }

                $this->save_product_quantities($request,$pro_obj->pro_id);





                #region add || edit product_translate

                $pro_names=$inputs["pro_name"];
                $pro_short_descs=$inputs["pro_short_desc"];
                $pro_descs=$inputs["pro_desc"];

                $pro_search_tags=$request->get("pro_search_tags");

                $pro_meta_title=$request->get("pro_meta_title");
                $pro_meta_desc=$request->get("pro_meta_desc");
                $pro_meta_keywords=$request->get("pro_meta_keywords");

                foreach ($this->data["lang_ids"] as $key => $lang_item) {

                    $pro_name=array_shift($pro_names);
                    $pro_meta_title = array_shift($pro_meta_title);
                    $pro_meta_desc = array_shift($pro_meta_desc);
                    $pro_meta_keywords = array_shift($pro_meta_keywords);


                    $translate_inputs=[
                        "pro_id"=>$pro_obj->pro_id,
                        "pro_name"=>$pro_name,
                        "pro_slug"=>trim(string_safe($pro_name)),
                        "pro_short_desc"=>array_shift($pro_short_descs),
                        "pro_desc"=>array_shift($pro_descs),
                        "pro_search_tags"=>array_shift($pro_search_tags),
                        "pro_meta_title"=>$pro_meta_title,
                        "pro_meta_desc"=>$pro_meta_desc,
                        "pro_meta_keywords"=>$pro_meta_keywords,
                        "lang_id"=>$lang_item->lang_id,
                    ];


                    if(empty($translate_inputs["pro_name"])){
                        continue;
                    }


                    $current_row = $pro_data_translate_rows->filter(function ($value, $key) use($lang_item) {
                        if ($value->lang_id == $lang_item->lang_id)
                        {
                            return $value;
                        }

                    });


                    if(is_object($current_row->first())){
                        //edit_translation row
                        $current_row->first()->update($translate_inputs);
                    }
                    else{
                        //add translation row
                       // dd($translate_inputs);
                        product_translate_m::create($translate_inputs);
                    }

                }
                #endregion

                return Redirect::to("admin/product/save_pro/$pro_obj->pro_id")->with(["msg"=>$this->data["msg"]])->send();

            }
            else{
                $this->data["errors"]=$validator->messages();
            }


        }//end submit


        return view("admin.subviews.products.save")->with($this->data);
    }

    private function save_product_quantities(Request $request,$pro_id){

        $old_quantities=product_quantities_m::
        where("pro_id",$pro_id)->
        leftJoin("attachments","attachments.id","=","product_quantities.pq_img_id")->
        get();

        foreach($old_quantities as $old_quantity){
            $this->edit_old_quantity($request,$old_quantity);
        }


        $this->add_new_quantity($request,$pro_id,0);
    }

    private function edit_old_quantity(Request $request,$old_quantity){
        $pq_img_id= $this->general_save_img(
            $request ,
            $item_id=$old_quantity->pro_id,
            $old_quantity->pq_id."_pq_img",
            $new_title = $request->get($old_quantity->pq_id."_pq_imgtitle"),
            $new_alt =  $request->get($old_quantity->pq_id."_pq_imgalt"),
            $upload_new_img_check = $request->get($old_quantity->pq_id."_pq_img_checkbox"),
            $upload_file_path = "/products/pq_product",
            $width = 0, $height = 0,
            $photo_id_for_edit = $old_quantity->pq_img_id
        );

        $old_quantity->update([
            'egypt_or_outside'=>$request->get($old_quantity->pq_id."_egypt_or_outside"),
            'pro_color'=>$request->get($old_quantity->pq_id."_pro_color"),
            'pro_available_sizes'=>json_encode($request->get($old_quantity->pq_id."_pro_available_sizes")),
            'pro_price'=>$request->get($old_quantity->pq_id."_pro_price"),
            'pro_discount_amount'=>$request->get($old_quantity->pq_id."_pro_discount_amount"),
            'pq_img_id'=>$pq_img_id
        ]);
    }

    private function add_new_quantity(Request $request,$pro_id,$key){

        if(empty($request->get("new_$key"."_pro_price"))){
            return;
        }

        $pq_img_id= $this->general_save_img(
            $request ,
            null,
            "new_$key"."_pq_img",
            $new_title = $request->get("new_$key"."_pq_imgtitle"),
            $new_alt = $request->get("new_$key"."_pq_imgalt"),
            $upload_new_img_check = $request->get("new_$key"."_pq_img_checkbox"),
            $upload_file_path = "/products/pq_product",
            $width = 0, $height = 0,
            $photo_id_for_edit = 0
        );

        product_quantities_m::create([
            'pro_id'=>$pro_id,
            'egypt_or_outside'=>$request->get("new_$key"."_egypt_or_outside"),
            'pro_color'=>$request->get("new_$key"."_pro_color"),
            'pro_available_sizes'=>json_encode($request->get("new_$key"."_pro_available_sizes")),
            'pro_price'=>$request->get("new_$key"."_pro_price"),
            'pro_discount_amount'=>$request->get("new_$key"."_pro_discount_amount"),
            'pq_img_id'=>$pq_img_id
        ]);

        $this->add_new_quantity($request,$pro_id,$key+1);

    }

    public function show_product_reviews(Request $request){

        $pro_id=$request->get("pro_id");

        $product_reviews=
            product_reviews_m::
            where("pro_id",$pro_id)->get();

        $this->data["product_reviews"]=$product_reviews;

        return view("admin.subviews.products.show_reviews")->with($this->data);
    }

    public function remove_product(Request $request){

        if (!check_permission($this->user_permissions,"admin/products","delete_action"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"]);
            return;
        }

        $this->general_remove_item($request,'App\models\product\product_m');
    }

}


