<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin_controller;
use App\Http\Controllers\dashbaord_controller;
use App\models\attachments_m;
use App\models\langs_m;
use App\models\permissions\permission_pages_m;
use App\models\permissions\permissions_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class users extends admin_controller
{

    public function __construct(){
        parent::__construct();
    }

    public $allowed_types=["admin","youtuber","service_operator","printing_operator"];

    public function get_all_users(Request $request)
    {

        if(!$this->check_user_permission("admin/admins","show_action")){
            return Redirect::to('admin/dashboard')->
            with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->
            send();
        }

        $user_type=$request->get("user_type");
        $this->data["user_type"]=$user_type;

        if(!in_array($user_type,$this->allowed_types)){
            return abort(404);
        }

        $addtional_where=" AND user_obj.user_type='$user_type'";

        if ($user_type=="admin"){
            $addtional_where="
                AND (
                    user_obj.user_type='admin' or user_obj.user_type='dev'
                ) 
            ";
        }

        $this->data["users"]=User::get_users($addtional_where);

        return view("admin.subviews.users.show_users",$this->data);
    }

    public function save_user(Request $request, $user_id = null)
    {

        if($user_id==null){
            if(!$this->check_user_permission("admin/admins","add_action")){
                return Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->send();
            }
        }
        else{
            if(!$this->check_user_permission("admin/admins","edit_action")){
                return Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->send();
            }
        }


        $user_type=$request->get("user_type");
        $this->data["user_type"] = $user_type;

        if(!in_array($user_type,$this->allowed_types)){
            return abort(404);
        }



        $this->data["user_data"] = "";
        $password_required = "required";
        $logo_id=0;
        $cover_id=0;

        if ($user_id != null)
        {
            $password_required = "";
            $this->data["user_data"] = User::get_users(" AND user_obj.user_id=$user_id");

            if(!isset_and_array($this->data["user_data"])){
                return Redirect::to('admin/dashboard')->send();
            }
            $this->data["user_data"]=$this->data["user_data"][0];
            $logo_id=$this->data["user_data"]->logo_id;
            $cover_id=$this->data["user_data"]->cover_id;

            $this->data["user_data"]->user_img_file=attachments_m::find($logo_id);
            $this->data["user_data"]->cover_img_file=attachments_m::find($cover_id);
        }

        if ($request->method()=="POST")
        {

            $user_name_rules="required";

            if($request->get("user_type")!=""){
                $user_name_rules.="|unique:users,full_name,".$user_id.",user_id,deleted_at,NULL";
            }

            $this->validate($request,
                [
                    "password" => $password_required,
                    "email" => "required|email|unique:users,email,".$user_id.",user_id,deleted_at,NULL",
                    "username" => $user_name_rules,
                ]);


            $request["full_name"]=$request["username"];

            if (isset($request["password"]) && !empty($request["password"]))
            {
                $request["password"] = bcrypt($request["password"]);
            }
            else{
                $request["password"] = $this->data["user_data"]->password;
            }


            $request["logo_id"] = $this->general_save_img(
                $request ,
                $item_id=$user_id,
                "user_img_file",
                $new_title = $request->get("user_img_filetitle"),
                $new_alt = $request->get("user_img_filealt"),
                $upload_new_img_check = $request["user_img_checkbox"],
                $upload_file_path = "/admins",
                $width = 0,
                $height = 0,
                $photo_id_for_edit = $logo_id
            );

            $request["cover_id"] = $this->general_save_img(
                $request ,
                $item_id=$user_id,
                "cover_img_file",
                $new_title = $request->get("cover_img_filetitle"),
                $new_alt = $request->get("cover_img_filealt"),
                $upload_new_img_check = $request["cover_img_checkbox"],
                $upload_file_path = "/admins",
                $width = 0,
                $height = 0,
                $photo_id_for_edit = $cover_id
            );


            // update
            if ($user_id != null)
            {
                $check = User::find($user_id)->update($request->all());
            }
            else{
                $request["user_type"] = $user_type;
                $request["user_active"] = "1";
                $request["user_can_login"] = "1";

                $check = User::create($request->all());

                $user_id=$check->user_id;
            }

            $this->data["msg"] = "<div class='alert alert-success'> Done </div>";


            return Redirect::to('admin/users/save/'.$user_id."?user_type=$user_type")->with([
                "msg"=>$this->data["msg"]
            ])->send();
        }


        return view("admin.subviews.users.save")->with($this->data);
    }

    public function assign_permission(Request $request,$user_id){

        if(!$this->check_user_permission("admin/admins","manage_permissions")){
            return Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->send();
        }

        $user_obj=User::where("user_id",$user_id)->get()->first();

        if(!is_object($user_obj)){
            return Redirect::to('admin/dashboard')->send();
        }

        $this->data["user_obj"]=$user_obj;

        //get all permission pages
        $all_permission_pages=permission_pages_m::where("sub_sys","admin")->get()->all();
        $all_permission_pages=array_combine(convert_inside_obj_to_arr($all_permission_pages,"per_page_id"),$all_permission_pages);

        //get all user permissions
        $all_user_permissions=permissions_m::where("user_id",$user_id)->get()->all();
        $all_user_permissions=array_combine(convert_inside_obj_to_arr($all_user_permissions,"per_page_id"),$all_user_permissions);

        $this->data["all_permission_pages"]=$all_permission_pages;
        $this->data["all_user_permissions"]=$all_user_permissions;


        foreach($all_user_permissions as $user_per_key=>$user_per_val){
            unset($all_permission_pages[$user_per_key]);
        }


        if(isset_and_array($all_permission_pages)){
            foreach($all_permission_pages as $page_key=>$page_val){
                permissions_m::create([
                    "user_id"=>"$user_id",
                    "per_page_id"=>"$page_key"
                ]);
            }

            return Redirect::to('admin/users/assign_permission/'.$user_id)->send();
        }


        if($request->method()=="POST"){

            foreach($all_user_permissions as $user_per_key=>$user_per_val){
                $new_perms=$request->get("additional_perms_new".$user_per_val->per_id);
                permissions_m::where("per_id",$user_per_val->per_id)->update([
                    "additional_permissions"=>json_encode($new_perms)
                ]);
            }


            return Redirect::to('admin/users/assign_permission/'.$user_id)->with([
                "msg"=>"<div class='alert alert-success'>Edit اذونات المستخدمين للصفحات</div>"
            ])->send();

        }


        return view("admin.subviews.users.user_permissions",$this->data);
    }

    public function remove_admin(Request $request){

        if(!$this->check_user_permission("admin/admins","delete_action")){
            echo json_encode(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"]);
            return;
        }

        $this->general_remove_item($request,'App\User');

    }

    public function show_profit_and_loss(Request $request){


        if(empty($request["filter_year"])){
            $request["filter_year"]=date("Y");
        }
        $this->data["post_data"]=(object)$request->all();

        $youtuber_id=$request->get("youtuber_id");
        $youtuber_obj=User::findOrFail($youtuber_id);

        $profit_and_loss=User::get_youtuber_profit_and_loss($youtuber_id,$request->get("filter_year"));

        $this->data["youtuber_obj"]=$youtuber_obj;
        $this->data["youtuber_profit"]=$profit_and_loss["profit"];
        $this->data["youtuber_loss"]=$profit_and_loss["loss"];

        return view("admin.subviews.users.show_profit_and_loss",$this->data);
    }

}
