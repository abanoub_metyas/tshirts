<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin_controller;
use App\models\countries_m;
use Illuminate\Http\Request;
use Redirect;

class country extends admin_controller
{

    public function __construct(){
        parent::__construct();
    }

    public function country_list()
    {
        $check_permission="admin/countries";
        have_permission_or_redirect($this->user_permissions,$check_permission,"show_action");
        $this->data["all_data"] = countries_m::get();
        return view("admin.subviews.country.show", $this->data);
    }


    public function save_country(Request $request, $country_id = null)
    {

        $check_permission="admin/countries";
        if($country_id==null){
            have_permission_or_redirect($this->user_permissions,$check_permission,"add_action");
        }
        else{
            have_permission_or_redirect($this->user_permissions,$check_permission,"edit_action");
        }


        $this->data["data_object"] = "";
       
        if ($country_id != null) {
            $this->data["data_object"] = countries_m::find($country_id);
        }

        if ($request->method() == "POST") {
            $this->validate($request,
                [
                    "country_name" => "required",
                ]);

            // update
            if ($country_id != null) {
                $check = countries_m::find($country_id)->update($request->all());

                if ($check == true) {
                    $this->data["msg"] = "<div class='alert alert-success'> Data Successfully Edit </div>";
                } else {
                    $this->data["msg"] = "<div class='alert alert-danger'> Something Is Wrong !!!!</div>";
                }


            } else {
                $request = $request->all();
                $check = countries_m::create($request);

                if (is_object($check)) {
                    $this->data["msg"] = "<div class='alert alert-success'> Data Successfully Inserted </div>";
                } else {
                    $this->data["msg"] = "<div class='alert alert-danger'> Something Is Wrong !!!!</div>";
                }

                $country_id = $check->country_id;
            }

            return Redirect::to('admin/countries/save/' . $country_id)->with([
                "msg" => $this->data["msg"]
            ])->send();
        }


        return view("admin.subviews.country.save")->with($this->data);
    }
}