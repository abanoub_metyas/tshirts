<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin_controller;
use App\Http\Controllers\dashbaord_controller;
use App\models\support_messages_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class support_messages extends admin_controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        if (!check_permission($this->user_permissions,"admin/support_messages","show_action"))
        {
            return  Redirect::to('admin/dashboard')->with(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"])->send();
        }

        $this->data["all_messages"] = support_messages_m::all();
        return view("admin.subviews.support_messages.show")->with($this->data);
    }

    public function remove_msg(Request $request){
        if (!check_permission($this->user_permissions,"admin/support_messages","show_action"))
        {
            echo json_encode(["msg"=>"<div class='alert alert-danger'>you can not access to this page</div>"]);
            return;
        }

        $this->general_remove_item($request,'App\models\support_messages_m');
    }


}
