<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin_controller;
use App\models\cities_m;
use App\models\countries_m;
use Illuminate\Http\Request;
use Redirect;

class city extends admin_controller
{

    public function __construct(){
        parent::__construct();
    }

    public function city_list(Request $request)
    {
        $check_permission="admin/cities";
        have_permission_or_redirect($this->user_permissions,$check_permission,"show_action");

        $this->data["all_data"] = cities_m::get_data($request->get("country_id"));
        return view("admin.subviews.city.show", $this->data);
    }


    public function save_city(Request $request, $city_id = null)
    {

        $check_permission="admin/countries";
        if($city_id==null){
            have_permission_or_redirect($this->user_permissions,$check_permission,"add_action");
        }
        else{
            have_permission_or_redirect($this->user_permissions,$check_permission,"edit_action");
        }

        $this->data["all_countries"]=countries_m::all();

        $this->data["data_object"] = "";
       
        if ($city_id != null) {
            $this->data["data_object"] = cities_m::findOrFail($city_id);
        }

        if ($request->method() == "POST") {
            $this->validate($request,
                [
                    "city_name" => "required",
                ]);

            // update
            if ($city_id != null) {
                cities_m::findOrFail($city_id)->update($request->all());

                $this->data["msg"] = "<div class='alert alert-success'> Data Successfully Edit </div>";
            }
            else {
                $city_id=cities_m::create($request->all())->city_id;

                $this->data["msg"] = "<div class='alert alert-success'> Data Successfully Inserted </div>";
            }

            return Redirect::to('admin/cities/save/' . $city_id)->with([
                "msg" => $this->data["msg"]
            ])->send();
        }


        return view("admin.subviews.city.save")->with($this->data);
    }
}