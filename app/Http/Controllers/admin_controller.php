<?php

namespace App\Http\Controllers;

use App\models\attachments_m;
use App\models\bills\bills_m;
use App\models\notification_m;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use SSP;

class admin_controller extends dashbaord_controller
{

    public $current_user_data;
    public $user_permissions;


    public function __construct()
    {
        parent::__construct();
        $this->middleware("check_admin");
        $this->current_user_data=$this->data["current_user"];



        $date = date('Y-m-d');

        $this->user_permissions = $this->get_user_permissions();
        $this->data["user_permissions"] = $this->user_permissions;
        $this->data["notifications"] = notification_m::get_notifications(
            " where 
            not_to_userid = $this->user_id 
            order by created_at desc"
        );


        $this->data["display_lang"]=\Session::get("display_lang","en");


        $this->data["header_all_bills"] = bills_m::all()->groupBy("bill_status")->all();

    }

    public function check_new_notifications(Request $request){

        $user_id=$request->get("user_id");
        $last_not_id=$request->get("last_not_id");

        $output=[];
        $output["items"]="";
        $output["items_count"]="0";

        $notifications= notification_m::get_notifications(
            " where
            not_id > $last_not_id
            AND not_to_userid =$user_id 
            order by created_at asc"
        );

        $output["items_count"]=count($notifications);

        foreach($notifications as $not){
            $output["items"].='<li class="not_item" data-not_id="'.$not->not_id.'">';
                $output["items"].='<a>';
                    $output["items"].='<div class="notification_desc alert alert-'.$not->not_type.'">';
                        $output["items"].='<p>'.$not->not_title.'</p>';
                        $output["items"].='<p><span>'.\Carbon\Carbon::createFromTimestamp(strtotime($not->created_at))->diffForHumans().'</span></p>';
                    $output["items"].='</div>';
                    $output["items"].='<div class="clearfix"></div>';
                $output["items"].='</a>';
            $output["items"].='</li>';
        }


        echo json_encode($output);
    }


}
