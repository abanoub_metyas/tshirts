<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback()
    {

        $googleUser = Socialite::driver('google')->stateless()->user();
        $email = $googleUser->getEmail();

        $existUser = User::where([['email',$email], ['provider','google']])->first();
        if($existUser){
            auth()->login($existUser, true);
        }
        else {
            $user = new User;
            $user->username = $googleUser->name;
            $user->full_name = $googleUser->name;
            $user->email = $googleUser->email;
            $user->user_type = 'user';
            $user->provider = 'google';
            $user->password = md5(rand(1,10000));
            $user->save();
            auth()->login($user);
        }
        return redirect()->to('login');
    }


    public function redirectToProviderFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallbackFacebook()
    {
        $facebookUser = Socialite::driver('facebook')->stateless()->user();
        $email = $facebookUser->getEmail();

        $existUser = User::where([['email',$email], ['provider','facebook']])->first();
        if($existUser){
            auth()->login($existUser, true);
        }
        else {
            $user = new User;
            $user->username = $facebookUser->name;
            $user->full_name = $facebookUser->name;
            $user->email = $facebookUser->email;
            $user->user_type = 'user';
            $user->provider = 'facebook';
            $user->password = md5(rand(1,10000));
            $user->save();
            auth()->login($user);
        }
        return redirect()->to('login');
    }



}
