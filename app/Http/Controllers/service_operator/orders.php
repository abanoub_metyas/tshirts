<?php

namespace App\Http\Controllers\service_operator;

use App\helpers\utility;
use App\Http\Controllers\admin_controller;
use App\Http\Controllers\printing_operator_controller;
use App\Http\Controllers\service_operator_controller;
use App\models\bills\bill_orders_m;
use App\models\bills\bills_m;
use App\models\category_m;
use App\Http\Controllers\dashbaord_controller;
use App\models\category_translate_m;
use App\models\materials_m;
use App\models\order_m;
use App\models\printing\printed_tshirts_m;
use App\models\printing\printing_requests_m;
use App\models\product\product_m;
use App\models\user_address_list_m;
use App\User;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use function GuzzleHttp\Psr7\uri_for;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class orders extends service_operator_controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {

        $this->data["post_data"]=(object)$request->all();
        $this->data["all_bills"]=bills_m::get_bills($request->all());

        return view("service_operator.subviews.orders.show")->with($this->data);
    }

    public function change_status(Request $request){

        $bill_id=$request->get("bill_id");
        $bill_obj=bills_m::findOrFail($bill_id);

        $this->data["bill_obj"]=$bill_obj;

        if($request->method()=="POST"){

            $bill_status = $request->get("bill_status");

            if(!in_array($bill_status,$this->order_status)){
                return abort(404);
            }

            //get user email
            $user_obj=User::findOrFail($bill_obj->user_id);
            $user_address_obj=user_address_list_m::findOrFail($bill_obj->address_id);

            //send email to the client

            $email_body=\View::make("email.msg")->with([
                "header"=>"Order Status for order number ".$bill_id,
                "body"=>"Stauts: ".$bill_status,
                "homepage_content"=>$this->data["homepage_content"]
            ])->render();

            if (!empty($user_address_obj->add_email)){

                utility::send_email_to_custom(
                    $emails = array(
                        $user_address_obj->add_email,
                    ) ,
                    $data = $email_body ,
                    $subject = "Order Status Changed"
                );
            }


            $bill_obj->update([
                "bill_status"=>$bill_status
            ]);

            return \redirect("/service_operator/orders/change_status?bill_id=$bill_id")->send();
        }


        return view("service_operator.subviews.orders.change_status")->with($this->data);
    }

    public function bill_orders($bill_id){
        $bill_obj=bills_m::findOrFail($bill_id);

        $this->data["bill_obj"] = $bill_obj;
        $this->data["user_obj"] = User::findOrFail($bill_obj->user_id);
        $this->data["youtuber_obj"] = User::findOrFail($bill_obj->youtuber_id);
        $address_obj = user_address_list_m::findOrFail($bill_obj->address_id);
        $this->data["address_obj"] = $address_obj;

        $bill_orders=
            bill_orders_m::
            select(DB::raw("
                bill_orders.*,
                product_translate.pro_name,
                product_quantities.pro_color,
                product_quantities.pro_available_sizes,
                product_quantities.pro_price,
                product_quantities.pro_discount_amount,
                product.pro_material_type,
                attachments.path as 'small_img_path'
            "))->
            join("product","product.pro_id","=","bill_orders.pro_id")->
            join("product_translate",function($join){
                $join->on("product.pro_id","=","product_translate.pro_id")->
                where("product_translate.lang_id","=","1");
            })->
            join("product_quantities","product_quantities.pq_id","=","bill_orders.pro_quan_id")->
            leftJoin("attachments","attachments.id","=","product.small_img_id")->
            where("bill_id",$bill_id)->
            get();


        $materials=materials_m::
        whereIn("m_name",$bill_orders->pluck("pro_material_type")->all())->
        whereIn("m_size",$bill_orders->pluck("order_selected_size")->all())->
        whereIn("m_color",$bill_orders->pluck("pro_color")->all())->
        get()->groupBy("m_name");


        $printing_requests=
            printing_requests_m::
            whereIn("bill_order_id",$bill_orders->pluck("bill_order_id")->all())->
            get()->groupBy("bill_order_id");


        $printed_tshirts=printed_tshirts_m::
        whereIn("pro_id",$bill_orders->pluck("pro_id")->all())->
        whereIn("pt_size",$bill_orders->pluck("order_selected_size")->all())->
        whereIn("pt_color",$bill_orders->pluck("pro_color")->all())->
        get()->groupBy("pro_id");


        $this->data["bill_orders"]=$bill_orders;
        $this->data["materials"]=$materials;
        $this->data["printing_requests"]=$printing_requests;
        $this->data["printed_tshirts"]=$printed_tshirts;

        return view("service_operator.subviews.orders.show_bill_orders")->with($this->data);
    }

    public function add_single_product(Request $request){

        $request->session()->put("add_product_to_bill",$request->get("bill_id"));

        echo " 
            go to the product you want select size, color 
            then add to cart it will added to the order automatically.
            if you want to add another product to the order you should do
            this whole operation again.
        ";
    }

    public function edit_bill_orders(Request $request){

        $bill_id=$request->get("bill_id");
        if(empty($bill_id))return abort(404);

        $bill_obj=bills_m::findOrFail($bill_id);

        $user_obj = User::findOrFail($bill_obj->user_id);

        $bill_orders=
            bill_orders_m::
            select(DB::raw("
                bill_orders.*,
                product_translate.pro_name,
                product_quantities.pro_color,
                product_quantities.pro_available_sizes,
                product_quantities.pro_price,
                product_quantities.pro_discount_amount,
                product.pro_material_type
            "))->
            join("product","product.pro_id","=","bill_orders.pro_id")->
            join("product_translate",function($join){
                $join->on("product.pro_id","=","product_translate.pro_id")->
                where("product_translate.lang_id","=","1");
            })->
            join("product_quantities","product_quantities.pq_id","=","bill_orders.pro_quan_id")->
            where("bill_id",$bill_id)->
            get();

        $this->data["bill_obj"] = $bill_obj;
        $this->data["bill_orders"]=$bill_orders;
        $this->data["user_obj"]=$user_obj;


        if($request->method()=="POST"){

            $order_ids=$request->get("order_id");
            $order_quantity=$request->get("order_quantity");

            $bill_orders=$bill_orders->groupBy("bill_order_id");

            foreach($order_ids as $key=>$order_id){
                $bill_orders[$order_id]->first()->update([
                    "order_quantity"=>$order_quantity[$key]
                ]);
            }

            bills_m::update_bill_cost($bill_id);

            return \redirect("/service_operator/orders/edit_bill_orders?bill_id=$bill_id")->
            with("msg","<div class='alert alert-info'>Updated</div>")->send();
        }

        return view("service_operator.subviews.orders.edit_bill_orders")->with($this->data);
    }

    public function send_to_printing_operator(Request $request){

        $order_id=$request->get("order_id");
        $order_obj=bill_orders_m::get_data($order_id);

        if(!is_object($order_obj))return abort(404);

        $this->data["order_obj"]=$order_obj;

        $printing_request_obj=printing_requests_m::
        where("bill_order_id",$order_id)->
        where("bill_id",$order_obj->bill_id)->get()->first();

        if(is_object($printing_request_obj)){
            return \redirect("/service_operator/orders/bill_orders/$order_obj->bill_id")->with("msg","
                <div class='alert alert-info'>you have made a print request before</div>
            ")->send();
        }

        //check material quantity
        $material_obj=materials_m::
        where("m_name",$order_obj->pro_material_type)->
        where("m_color",$order_obj->pro_color)->
        where("m_size",$order_obj->order_selected_size)->
        get()->first();

        if(!is_object($material_obj)){
            return \redirect("/service_operator/orders/bill_orders/$order_obj->bill_id")->with("msg",
                "<div class='alert alert-info'>
                    there is no materials matching color: ".$order_obj->pro_color.
                    " - size: ".$order_obj->order_selected_size.
                    " - type: ".$order_obj->pro_material_type.
                "</div>"
            )->send();
        }

        if($order_obj->order_quantity > $material_obj->m_quantity){
            return \redirect("/service_operator/orders/bill_orders/$order_obj->bill_id")->with("msg",
                "<div class='alert alert-info'>
                    there is no sufficient quantity for the material matching color: ".$order_obj->pro_color." - size: ".$order_obj->order_selected_size.
                "</div>"
            )->send();
        }

        if($request->method()=="POST"){

            $request_quantity=$request->get("request_quantity");

            printing_requests_m::create([
                'material_id'=>$material_obj->m_id,
                'bill_id'=>$order_obj->bill_id,
                'bill_order_id'=>$order_obj->bill_order_id,
                'print_status'=>"pending",
                'request_quantity'=>$request_quantity
            ]);

            //send notification to printing operators
            utility::send_notification_to_users(
                $user_ids=utility::get_admins(["printing_operator"],true),
                $not_title="New printing request",
                $not_type="info"
            );

            return \redirect("/service_operator/orders/bill_orders/$order_obj->bill_id")->with("msg","
                <div class='alert alert-info'> print request is sent</div>
            ")->send();
        }


        return view("service_operator.subviews.orders.send_to_printing_operator")->with($this->data);
    }

    public function remove_order(Request $request){
        $order_obj=bill_orders_m::findOrFail($request->get("item_id"));

        $this->general_remove_item($request,'App\models\bills\bill_orders_m');

        bills_m::update_bill_cost($order_obj->bill_id);
    }

    public function send_to_fetchr(Request $request){

        $bill_id=$request->get("bill_id");
        $bill_obj=bills_m::get_bill($bill_id);

        if(!is_object($bill_obj))return abort(404);

        if($bill_obj->delivery_type!="fetchr"){
            return \redirect("/service_operator/orders/bill_orders/$bill_obj->bill_id")->with("msg","
                <div class='alert alert-info'>this order's user want to user another shipping service</div>
            ")->send();
        }

        if(!empty($bill_obj->shipping_awb_link)){
            return \redirect("/service_operator/orders/bill_orders/$bill_obj->bill_id")->with("msg","
                <div class='alert alert-info'>it's sent to fetchr already</div>
            ")->send();
        }

        $products_desc="";
        $bill_orders=bill_orders_m::get_data("",[$bill_id]);
        $bill_items=[];

        foreach ($bill_orders as $bill_order){
            $products_desc.=
                $bill_order->pro_name." ".
                $bill_order->pro_material_type." ".
                $bill_order->order_selected_size." ".
                $bill_order->pro_color." ,";

            $bill_items[]=[
                "description" => $bill_order->pro_name,
                "quantity" => $bill_order->order_quantity,
                "order_value_per_unit" => ($bill_order->pro_price- $bill_order->pro_discount_amount)
            ];
        };


        $client = new Client();

        $post_order_link="https://api.order.fetchr.us/order";

        //COD => cash on deliver
        //CCOD => pay on deliver with credit card
        //Credit Card => paid already

        $payment_type="COD";

        $bill_total_amount=$bill_obj->bill_amount;

        if(
            $bill_obj->payment_method=="payment online"&&
            $bill_obj->bill_amount==$bill_obj->bill_paid_amount

        ){
            $payment_type="Credit Card";
        }
        else{
            $bill_total_amount=$bill_obj->bill_amount-$bill_obj->bill_paid_amount;
        }

        try {
            $response = $client->request('POST', $post_order_link,
                [
                    'json' => [
                        "client_address_id" => "ADDR23173240_30431",
                        "data" => [
                            [
                                "order_reference" => $bill_id."_teearab_".rand(1111,9999)."_".time(),
                                "name" => $bill_obj->add_full_name,
                                "email" => $bill_obj->add_email,
                                "phone_number" => $bill_obj->add_tel_number,
                                "address" => $bill_obj->add_street,
                                "receiver_country" => $bill_obj->add_country,
                                "receiver_city" => $bill_obj->add_governorate,
                                "payment_type" => $payment_type,
                                "bag_count" => 1,
                                "weight" => 2.312,
                                "description" => $products_desc,
                                "total_amount" => $bill_total_amount,
                                "items" =>  $bill_items
                            ]
                        ]
                    ],
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer 10023549-909a-4124-9f6c-31675dbc4677',
                    ]
                ]
            );

            $response=\GuzzleHttp\json_decode($response->getBody());

            if(
                $response->status!="success"||
                $response->data[0]->status!="success"
            ){
                return var_dump($response);
            }

            $bill_obj->update([
                "shipping_response"=>json_encode($response),
                "shipping_awb_link"=>$response->data[0]->awb_link
            ]);

            return \redirect("/service_operator/orders/bill_orders/$bill_obj->bill_id")->with("msg","
                <div class='alert alert-info'>Sent to fetchr</div>
            ")->send();

        }
        catch (GuzzleException $e) {
            abort(404);
        }


    }

    public function cancel_order_fetchr(Request $request){

        $bill_id=$request->get("bill_id");
        $bill_obj=bills_m::get_bill($bill_id);

        if(!is_object($bill_obj))return abort(404);

        if($bill_obj->delivery_type!="fetchr"){
            return \redirect("/service_operator/orders/bill_orders/$bill_obj->bill_id")->with("msg","
                <div class='alert alert-info'>this order is not from fetchr</div>
            ")->send();
        }

        if(empty($bill_obj->shipping_awb_link)){
            return \redirect("/service_operator/orders/bill_orders/$bill_obj->bill_id")->with("msg","
                <div class='alert alert-info'>you didn't send to fetchr yet</div>
            ")->send();
        }


        $client = new Client();
        $post_order_link="https://api.order.fetchr.us/order/status";

        $tracking_num=json_decode($bill_obj->shipping_response)->data[0]->tracking_no;

        try {
            $response = $client->request('PUT', $post_order_link,
                [
                    'json' => [
                        "tracking_no" => ["$tracking_num"],
                        "status"=> "cancel"
                    ],
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer 10023549-909a-4124-9f6c-31675dbc4677',
                    ]
                ]
            );



            $response=json_decode($response->getBody());


            if(
                $response->status!="success"

            ){
                return var_dump($response);
            }

            if($response->data[0]->status=="success"){
                $bill_obj->update([
                    "shipping_response"=>"",
                    "shipping_awb_link"=>""
                ]);
            }


            return \redirect("/service_operator/orders/bill_orders/$bill_obj->bill_id")->with("msg","
                <div class='alert alert-info'>
                ".$response->data[0]->message."
                </div>
            ")->send();

        }
        catch (GuzzleException $e) {
            abort(404);
        }
    }

    public function send_to_aramex(Request $request){

        $bill_id=$request->get("bill_id");
        $bill_obj=bills_m::get_bill($bill_id);

        if(!is_object($bill_obj))return abort(404);

        if($bill_obj->delivery_type!="aramex"){
            return \redirect("/service_operator/orders/bill_orders/$bill_obj->bill_id")->with("msg","
                <div class='alert alert-info'>this order's user want to user another shipping service</div>
            ")->send();
        }

        $aramex_couyntry_code=$request->get("aramex_couyntry_code");
        $aramex_city=$request->get("aramex_city");
        $PostCode=$request->get("aramex_post_code");

        if (empty($aramex_couyntry_code)||empty($aramex_city)){
            return \redirect("/service_operator/orders/bill_orders/$bill_obj->bill_id")->with("msg","
                <div class='alert alert-info'>you should add aramex country code, city</div>
            ")->send();
        }

        $products_desc="";
        $bill_orders=bill_orders_m::get_data("",[$bill_id]);
        $bill_items=[];

        foreach ($bill_orders as $bill_order){
            $products_desc.=
                $bill_order->pro_name." ".
                $bill_order->pro_material_type." ".
                $bill_order->order_selected_size." ".
                $bill_order->pro_color." ,";

            $bill_items[]=[
                "description" => $bill_order->pro_name,
                "Quantity" => $bill_order->order_quantity,
                "order_value_per_unit" => ($bill_order->pro_price- $bill_order->pro_discount_amount)
            ];
        };

        //EXP = Express DOM = Domestic
        // domestic => CDS , Express = > EXP


        $payment_services="CODS";
        $payment_currency="EGP";
        $CashOnDeliveryAmount=$bill_obj->bill_amount;
        $ProductType="CDS";
        $ProductGroup="DOM";

        if (
            $bill_obj->payment_method=="payment online"&&
            $bill_obj->bill_amount==$bill_obj->bill_paid_amount
        ){
            $CashOnDeliveryAmount=0;
            $payment_services="";
        }
        else{
            $CashOnDeliveryAmount=$bill_obj->bill_amount-$bill_obj->bill_paid_amount;
            $payment_services="CODS";
        }

        if($bill_obj->add_country!="Egypt"){
            $payment_currency="USD";
            $ProductType="EXP";
            $ProductGroup="EXP";
        }


        $soapClient = new \SoapClient('resources/assets/Shipping.wsdl');

        $params = array(
            'Shipments' => array(
                'Shipment' => array(
                    'Shipper' => array(
                        'AccountNumber' => '161599',
                        'PartyAddress' => array(
                            'Line1' => 'tanta,Alrajdiah',
                            'City' => 'Gharbia',
                            'CountryCode' => 'EG'
                        ),
                        'Contact' => array(
                            'PersonName' => 'Ahmed',
                            'CompanyName' => 'Teearab',
                            'PhoneNumber1' => '01064939132',
                            'CellPhone' => '01064939132',
                            'EmailAddress' => 'engalgamal96@gmail.com',
                        ),
                    ),

                    'Consignee' => array(
                        'PartyAddress' => array(
                            'Line1' => $bill_obj->add_street,
                            'Line2' => $bill_obj->add_notes,
                            'Line3' => $bill_obj->add_city,
                            'City' => $aramex_city,
                            'CountryCode' => $aramex_couyntry_code,
                            'PostCode' => $PostCode,
                        ),

                        'Contact' => array(
                            'PersonName' => $bill_obj->add_full_name,
                            'CompanyName' => 'Teearab',
                            'PhoneNumber1' => $bill_obj->add_tel_number,
                            'CellPhone' => $bill_obj->add_tel_number,
                            'EmailAddress' => $bill_obj->add_email,
                        ),
                    ),

                    'Reference1' => 'Shpt 0001',
                    'TransportType' => 0,
                    'ShippingDateTime' => time(),
                    'DueDate' => time(),
                    'PickupLocation' => 'Reception',
                    'PickupGUID' => '',
                    'Comments' => 'Shpt 0001',
                    'AccountingInstrcutions' => '',
                    'OperationsInstructions' => '',

                    'Details' => array(
                        'Dimensions' => array(
                            'Length' => 10,
                            'Width' => 10,
                            'Height' => 10,
                            'Unit' => 'cm',
                        ),

                        'ActualWeight' => array(
                            'Value' => 0.5,
                            'Unit' => 'Kg'
                        ),

                        'ProductGroup' => $ProductGroup,
                        'ProductType' => $ProductType,
                        'PaymentType' => 'P',
                        'Services' => $payment_services,
                        'NumberOfPieces' => count($bill_items),
                        'DescriptionOfGoods' => $products_desc,
                        'GoodsOriginCountry' => 'EG',

                        'CashOnDeliveryAmount' => array(
                            'Value' => $CashOnDeliveryAmount,
                            'CurrencyCode' => $payment_currency
                        ),

                        'InsuranceAmount' => array(
                            'Value' => 0,
                            'CurrencyCode' => $payment_currency
                        ),

                        'CollectAmount' => array(
                            'Value' => 0,
                            'CurrencyCode' => $payment_currency
                        ),

                        'CashAdditionalAmount' => array(
                            'Value' => 0,
                            'CurrencyCode' => $payment_currency
                        ),

                        'CashAdditionalAmountDescription' => '',

                        'CustomsValueAmount' => array(
                            'Value' => 0,
                            'CurrencyCode' => $payment_currency
                        ),

                        'Items' => $bill_items
                    ),
                ),
            ),

            'ClientInfo' => array(
                'AccountCountryCode' => 'EG',
                'AccountEntity' => 'CAI',
                'AccountNumber' => '161599',
                'AccountPin' => '654154',
                'UserName' => 'engalgamal96@gmail.com',
                'Password' => 'DI_na1997',
                'Version' => '1.0'
            ),

            'Transaction' => array(
                'Reference1' => '001',
            ),
            'LabelInfo' => array(
                'ReportID' => 9729,
                'ReportType' => 'URL',
            ),
        );

        $params['Shipments']['Shipment']['Details']['Items'][] = array(
            'PackageType' => 'Box',
            'Quantity' => 1,
            'Weight' => array(
                'Value' => 0.5,
                'Unit' => 'Kg',
            ),
            'Comments' => $products_desc,
        );


        try {
            $auth_call = $soapClient->CreateShipments($params);

            if($auth_call->HasErrors){
                $error_msgs="";


                if(is_array($auth_call->Shipments->ProcessedShipment->Notifications->Notification)){
                    $error_msgs=collect($auth_call->Shipments->ProcessedShipment->Notifications->Notification)->pluck("Message")->all();
                    $error_msgs=implode(",",$error_msgs);
                }
                else{
                    $error_msgs.=$auth_call->Shipments->ProcessedShipment->Notifications->Notification->Message;
                }


                return \redirect("/service_operator/orders/bill_orders/$bill_obj->bill_id")->with("msg","
                    <div class='alert alert-info'>Error. ".
                        $error_msgs
                    ." </div>
                ")->send();
            }
            else{
                $bill_obj->update([
                    "shipping_response"=>json_encode($auth_call),
                    "shipping_awb_link"=>$auth_call->Shipments->ProcessedShipment->ShipmentLabel->LabelURL
                ]);

                return \redirect("/service_operator/orders/bill_orders/$bill_obj->bill_id")->with("msg","
                    <div class='alert alert-info'>Sent to Aramex</div>
                ")->send();
            }

        } catch (SoapFault $fault) {
            die('Error : ' . $fault->faultstring);
        }

    }


}
