<?php

namespace App\Http\Controllers\service_operator;

use App\Http\Controllers\service_operator_controller;
use Illuminate\Http\Request;

use App\User;

class dashboard extends service_operator_controller
{

    public function __construct(){
        parent::__construct();
    }

    public function index(Request $request)
    {

        
        return view("service_operator.subviews.dashboard",$this->data);
    }



}
