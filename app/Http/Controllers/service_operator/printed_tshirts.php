<?php

namespace App\Http\Controllers\service_operator;

use App\Http\Controllers\service_operator_controller;
use App\models\printing\printed_tshirts_m;
use App\models\product\product_m;
use App\models\product\product_quantities_m;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class printed_tshirts extends service_operator_controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function show_all(Request $request)
    {
        $this->data["post_data"]=(object)$request->all();
        $this->data["all_printed_tshirts"]=printed_tshirts_m::get_data($request->all());

        return view("service_operator.subviews.printed_tshirts.show_all")->with($this->data);
    }

    public function select_product(Request $request){
        $this->data["post_data"]=(object)$request->all();

        $confitions=[];

        if(!empty($request->get("youtuber_name"))){
            $confitions[]=" AND user_obj.full_name like '%".$request->get("youtuber_name")."%'";
        }

        if(!empty($request->get("product_name"))){
            $confitions[]=" AND pro_translate.pro_name like '%".$request->get("product_name")."%'";
        }

        $this->data["all_products"]=product_m::get_all_pros(implode(
            " ",$confitions
        ));

        return view("service_operator.subviews.printed_tshirts.select_product")->with($this->data);
    }

    public function save(Request $request ,$pro_id ,$row_id = null)
    {

        $this->data["row_id"]=$row_id;
        $this->data["data_obj"] = "";

        if ($row_id != null){
            $this->data["data_obj"]=printed_tshirts_m::findOrFail($row_id);
        }

        $this->data["pro_id"]=$pro_id;

        $product_quantities = product_quantities_m::get_product_quantities($pro_id);


        $this->data['all_colors']=array_unique($product_quantities->pluck("pro_color")->all());
        $array_sizes=call_user_func_array(
            "array_merge",
            array_map("json_decode",$product_quantities->pluck("pro_available_sizes")->flatten()->all())
        );
        $this->data['all_sizes']=array_unique($array_sizes);


        if ($request->method() == "POST")
        {

            $request["pro_id"]=$pro_id;

            $this->validate($request,[
                "pro_id" => "required",
            ]);


            if ($row_id!=null)
            {
                printed_tshirts_m::find($row_id)->update($request->all());
            }
            else{
                // insert
                $new_obj = printed_tshirts_m::create($request->all());
                $row_id = $new_obj->pt_id;

                //add this quantity to printed t-shirts
                $return_obj=printed_tshirts_m::get_data([
                    "pt_id"=>$row_id
                ])->first();


            }

            return Redirect::to("service_operator/printed_tshirts/save/$pro_id/$row_id")->
            with(["msg"=>"<div class='alert alert-success'>Done</div>"])->send();

        }


        return view("service_operator.subviews.printed_tshirts.save")->with($this->data);
    }




}
