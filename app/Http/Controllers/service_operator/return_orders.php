<?php

namespace App\Http\Controllers\service_operator;

use App\helpers\utility;
use App\Http\Controllers\admin_controller;
use App\Http\Controllers\printing_operator_controller;
use App\Http\Controllers\service_operator_controller;
use App\models\bills\bill_orders_m;
use App\models\bills\bills_m;
use App\models\bills\return_orders_m;
use App\models\category_m;
use App\Http\Controllers\dashbaord_controller;
use App\models\category_translate_m;
use App\models\materials_m;
use App\models\order_m;
use App\models\printing\printed_tshirts_m;
use App\models\printing\printing_requests_m;
use App\models\product\product_m;
use App\models\product\product_quantities_m;
use App\models\user_address_list_m;
use App\User;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use function GuzzleHttp\Psr7\uri_for;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class return_orders extends service_operator_controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function show_all(Request $request)
    {
        $this->data["post_data"]=(object)$request->all();
        $this->data["all_return_orders"]=return_orders_m::get_data($request->all());

        return view("service_operator.subviews.return_orders.show_all")->with($this->data);
    }

    public function select_product(Request $request){
        $this->data["post_data"]=(object)$request->all();

        $confitions=[];

        if(!empty($request->get("youtuber_name"))){
            $confitions[]=" AND user_obj.full_name like '%".$request->get("youtuber_name")."%'";
        }

        if(!empty($request->get("product_name"))){
            $confitions[]=" AND pro_translate.pro_name like '%".$request->get("product_name")."%'";
        }

        $this->data["all_products"]=product_m::get_all_pros(implode(
            " ",$confitions
        ));

        return view("service_operator.subviews.return_orders.select_product")->with($this->data);
    }

    public function save(Request $request ,$pro_id ,$row_id = null)
    {

        $this->data["row_id"]=$row_id;
        $this->data["data_obj"] = "";

        if ($row_id != null){
            $this->data["data_obj"]=return_orders_m::findOrFail($row_id);
        }

        $this->data["pro_id"]=$pro_id;

        $product_quantities = product_quantities_m::get_product_quantities($pro_id);


        $this->data['all_colors']=array_unique($product_quantities->pluck("pro_color")->all());
        $array_sizes=call_user_func_array(
            "array_merge",
            array_map("json_decode",$product_quantities->pluck("pro_available_sizes")->flatten()->all())
        );
        $this->data['all_sizes']=array_unique($array_sizes);


        if ($request->method() == "POST")
        {

            $request["pro_id"]=$pro_id;

            $this->validate($request,[
                "pro_id" => "required",
            ]);


            if ($row_id!=null)
            {
                return_orders_m::find($row_id)->update($request->all());
            }
            else{
                // insert
                $new_obj = return_orders_m::create($request->all());
                $row_id = $new_obj->ro_id;

                //add this quantity to printed t-shirts
                $return_obj=return_orders_m::get_data([
                    "ro_id"=>$row_id
                ])->first();

                printed_tshirts_m::add_quantity_from_return_order($return_obj);

            }

            return Redirect::to("service_operator/return_orders/save/$pro_id/$row_id")->
            with(["msg"=>"<div class='alert alert-success'>Done</div>"])->send();

        }


        return view("service_operator.subviews.return_orders.save")->with($this->data);
    }




}
