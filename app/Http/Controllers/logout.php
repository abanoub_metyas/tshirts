<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class logout extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        User::findOrFail($this->user_id)->update([
            "user_is_online"=>"0"
        ]);

        \Auth::logout();
        return Redirect::to('/')->send();
    }

}
