<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    protected $except = [
        'upload',
        'login',
        'paypal/paypalNotify/*',
        'accept_payment_notification',
        'accept_payment_kiosk_notification',
    ];
}
