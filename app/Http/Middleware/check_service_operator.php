<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class check_service_operator
{

    public function handle($request, Closure $next)
    {
        $user=Auth::user();
        if(
            isset($user) &&
            $user->user_active == 1 &&
            in_array($user->user_type,["service_operator","admin","dev"])&&
            $user->pause_user == 0
        ){
            return $next($request);
        }

        return redirect("/login")->
        with("msg",
            "<div class='alert alert-info'>You should login first</div>"
        )->send();
    }
}
