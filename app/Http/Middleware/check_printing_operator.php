<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class check_printing_operator
{

    public function handle($request, Closure $next)
    {
        $user=Auth::user();
        if(
            isset($user) &&
            $user->user_active == 1 &&
            in_array($user->user_type,["dev","admin","printing_operator"])
        ){
            return $next($request);
        }


        return redirect("/login")->
        with("msg",
            "<div class='alert alert-info'>You should login first</div>"
        )->send();
    }
}
