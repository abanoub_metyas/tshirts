<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class check_youtuber
{

    public function handle($request, Closure $next)
    {
        $user=Auth::user();
        if(
            isset($user) &&
            $user->user_active == 1 &&
            $user->user_type=="youtuber" &&
            $user->pause_user == 0
        ){
            return $next($request);
        }

        return redirect("/login")->
        with("msg",
            "<div class='alert alert-info'>You should login first</div>"
        )->send();
    }
}
