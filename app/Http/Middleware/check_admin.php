<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class check_admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=Auth::user();
        if(
            isset($user) && $user->user_active == 1 &&
            in_array($user->user_type,["dev","admin"])
        ){
            return $next($request);
        }

        return redirect("/login")->
        with("msg",
            "<div class='alert alert-info'>You should login first</div>"
        )->send();
    }
}
