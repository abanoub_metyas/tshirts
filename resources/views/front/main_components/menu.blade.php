<div class="container-fluid">
    <nav class="navbar navbar-expand-sm   navbar-light navexpand">
        <a class="navbar-brand" href="{{url("/")}}">
            <img
                    src="{{show_content($homepage_content,"site_logo",true)}}"
                    class="img-fluid"
                    {{get_image_alt_title(is_object($homepage_content)?$homepage_content->site_logo:"")}}
            >
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                <?php foreach($parent_pro_cats as $key=>$pro_cat): ?>
                <?php
                if(!isset($child_pro_cats[$pro_cat->cat_id]))continue;
                ?>
                <li class="nav-item dropdown dmenu">
                    <a class="nav-link dropdown-toggle" href="{{url("/show_parent_cat?cat_id=$pro_cat->cat_id")}}" data-toggle="dropdown">
                        {{$pro_cat->cat_name}}
                    </a>
                    <div class="dropdown-menu sm-menu">
                        <?php foreach($child_pro_cats[$pro_cat->cat_id] as $key=>$hild_cat): ?>
                        <a class="dropdown-item" href="{{url("/show_child_cat?cat_id=$hild_cat->cat_id")}}" >
                            {{$hild_cat->cat_name}}
                        </a>
                        <?php endforeach; ?>
                    </div>
                </li>
                <?php endforeach; ?>

                <?php foreach($menu_pages_header as $key=>$page): ?>
                <li class="nav-item">
                    <a class="nav-link" href="{{url("/pages/show_page?page_id=".$page->page_id)}}">
                        {{$page->page_title}}
                    </a>
                </li>
                <?php endforeach; ?>

                <?php if(isset_and_array($menu_cart_items)): ?>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url("/visitor/checkout")}}">
                            Checkout
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
            <div class="social-part">
                <ul class="list-unstyled">
                    <?php if(is_object($current_user)): ?>

                    <li class="">
                        <a class="" href="{{url("/login")}}">
                            Dashboard
                        </a>
                    </li>

                    <?php else: ?>
                    <li class="">
                        <a class="" href="{{url("/register")}}">
                            Register
                        </a>
                    </li>
                    <li class="">
                        <a class="login" href="{{url("/login")}}">
                            Login
                        </a>
                    </li>
                    <?php endif; ?>

                </ul>
            </div>
        </div>
    </nav>
</div>