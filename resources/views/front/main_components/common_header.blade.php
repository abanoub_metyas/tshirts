<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>{{$meta_title}}</title>
    <meta name="description" content="<?php echo $meta_desc ?>"/>
    <meta name="keywords" content="<?php echo $meta_keywords ?>"/>

    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="icon" href="{{url("/")}}/public/front/images/icon.png" type="image/png" sizes="16x16">
    <link rel="stylesheet" type="text/css" href="{{url("/")}}/public/front/css/jquery.fancybox-plus.css"/>
    <link rel="stylesheet" type="text/css" href="{{url("/")}}/public/front/css/jquery.ez-plus.css"/>
    <link rel="stylesheet" href="{{url("/")}}/public/front/css/animate.css">
    <link href="{{url("/")}}/public/front/css/animate.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{url("/")}}/public/front/css/bootstrap.min.css" integrity="" crossorigin="anonymous">
    <link href="{{url("/")}}/public/front/css/bootstrap-better-nav.css" rel="stylesheet">
    <link rel="stylesheet" href="{{url("/")}}/public/front/css/styles.css" >
    <link rel="stylesheet" href="{{url("/")}}/public/custom.css" >
    <link href="{{url("/")}}/public/front/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">

    <script type="text/javascript" src="{{url("/")}}/public/front/query_files/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="{{url("/")}}/public/front/query_files/jquery.easing.min.js"></script>
    <script type="text/javascript" src="{{url("/")}}/public/front/query_files/jquery.fancybox-plus.js"></script>
    <script type="text/javascript" src="{{url("/")}}/public/front/query_files/jquery.ez-plus.js"></script>
    <script type="text/javascript" src="{{url("/")}}/public/front/query_files/web.js?m=20100203"></script>
    <script src="{{url("/")}}/public/front/query_files/popper.min.js" integrity="" crossorigin="anonymous"></script>
    <script src="{{url("/")}}/public/front/query_files/bootstrap.min.js" integrity="" crossorigin="anonymous"></script>
    <script src="{{url("/")}}/public/front/query_files/wow.min.js"></script>
    <script src="{{url("/")}}/public/front/query_files/simplePlayer.js"></script>
    <script src="{{url("/")}}/public/front/query_files/jquery.fancybox.min.js"></script>
    <script src="{{url("/")}}/public/front/query_files/bootstrap-better-nav.js"></script>
    <script src="{{url("/")}}/public/front/query_files/custom.js" integrity="" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="{{url("/")}}/public/toastr/toastr.css" >
    <script src="{{url("/")}}/public/toastr/toastr.js"></script>

    <script src="{{url("/")}}/public/jscode/config.js"></script>
    <script src="{{url("/")}}/public/jscode/homepage.js"></script>
    <script src="{{url("/")}}/public/jscode/btm_form_helpers/form.js" type="text/javascript"></script>

    {!! show_content($add_scripts,"add_scripts") !!}

</head>
<body>

<input type="hidden" class="csrf_input_class" value="{{csrf_token()}}">
<input type="hidden" class="url_class" value="<?= url("/") ?>">
<input type="hidden" class="lang_url_class" value="<?= $lang_url_segment ?>">

<?php

if (empty($msg))
{
    $msg=\Session::get("msg");
}

if($msg==""){
    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        $msg=$dump;
    }
}

?>

<input type="hidden" class="get_flash_message" value="{!! $msg !!}">
