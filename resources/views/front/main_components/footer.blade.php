<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="list-unstyled footerlist">
                    <?php foreach($menu_pages_footer as $key=>$page): ?>
                    <li>
                        <a href="{{url("/pages/show_page?page_id=".$page->page_id)}}">
                            {{$page->page_title}}
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-md-2">

            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="copyright ">
                   {{show_content($homepage_content,"footer_copyright")}}
                </div>
            </div>
            <div class="col-md-2 text-center">
                <ul class="list-unstyled footersocial">
                    <?php if(
                        isset($homepage_content->social_icon_name)&&
                        is_array($homepage_content->social_icon_name)
                    ): ?>
                        <?php foreach($homepage_content->social_icon_name as $key=>$icon): ?>
                            <li>
                                <a href="{{$homepage_content->social_icon_link[$key]}}">
                                    <i class="fab fa-{{$icon}}">

                                    </i>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>

                </ul>
            </div>
        </div>
    </div>
</footer>

</body>
</html>