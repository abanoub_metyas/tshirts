@extends("front.main_layout")
@section("subview")



    <div class="profile_logo" style="background: url('{{get_image_from_obj($youtuber_obj->cover_obj)}}')">
        <div class="container">
            <img
                src="{{get_image_from_obj($youtuber_obj->logo_obj)}}"
                {!! get_image_alt_title($youtuber_obj->logo_obj) !!}
                class="rounded-circle"
            >
        </div>
    </div>

    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <h2>{{$youtuber_obj->full_name}}</h2>
                {!! $youtuber_obj->user_desc !!}
            </div>

            <?php foreach($products as $key=>$pro_obj): ?>
                <div class="col-md-3 col-6">
                    @include("front.subviews.category.pro_block")
                </div>
            <?php endforeach; ?>
        </div>


    </div>




@endsection