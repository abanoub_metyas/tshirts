@extends("front.main_layout")

@section('subview')

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <script src="{{url("/")}}/public/jscode/product.js"></script>
    <script src="{{url("/")}}/public/jscode/order.js"></script>


    <!--===================================end div of content ====================================================-->
    <div class="container pagepro">
        <div class="row">
            <div class="col-md-6 col-12">
                <div id="page" class="home">
                    <div id="col_wrap">
                        <?php if(isset($pro_data->slider_imgs)&&count($pro_data->slider_imgs)): ?>
                        <div>
                            <div class="allgallry">
                                <img style="width:411px" id="img_01"
                                     src="{{get_image_or_default($pro_data->slider_imgs[0]->path)}}"
                                     data-zoom-image="{{get_image_or_default($pro_data->slider_imgs[0]->path)}}"
                                />

                                <div id="gal1" style="width:500px">
                                    <?php foreach($pro_data->slider_imgs as $key => $slider_img): ?>
                                        <a href="#"
                                           class="elevatezoom-gallery"
                                           data-update=""
                                           data-image="{{get_image_or_default($slider_img->path)}}"
                                           data-zoom-image="{{get_image_or_default($slider_img->path)}}"
                                        >
                                            <img id="img_01" src="{{get_image_or_default($slider_img->path)}}" width="100"/>
                                        </a>
                                    <?php endforeach; ?>
                                </div>
                            </div>


                            <div style="clear:both;"></div>
                            <div class="clear"></div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-12 single-pro">
                <div class="product-form">
                    <h2>
                        {{$pro_data->pro_name}} - {{$pro_data->pro_material_type}}
                    </h2>
                    <ul class="list-unstyled product-section">
                        <li>
                            <span>{!! $pro_data->pro_short_desc !!}</span>
                        </li>

                        <li>
                            <strong>Availability:</strong>
                            <span class="stock">In Stock</span>
                        </li>
                    </ul>

                    <!--==========================-->
                    <div class="rating-section product-rating-status">

                        <div class="rating">

                            {!! draw_stars($pro_data->pro_stars) !!}

                            <span class="review-link review-link-show">
                                <a href="#">{{$product_reviews->count()}} Review(s)</a>
                            </span>
                            <span class="review-link review-link-write">
                                <a href="#tagrevie" class="searchbychar">Add your review</a>
                            </span>
                        </div>

                    </div>

                    @include("front.subviews.category.product_page_components.sizes_colors")

                    <div class="product_share">
                        <div id="share"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="clearfix">

    </div>

    <!--===================-->
    <div class="container review" id="tagrevie">
        <div class="row">
            <div class="col-12">
                <div class="content recont">
                    <ul class="nav nav-tabs" id="myTabbb" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="homeeerw-tab" data-toggle="tab" href="#homeeewe" role="tab" aria-controls="home" aria-selected="true">Description</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " id="profileeerw-tab" data-toggle="tab" href="#profileeerw" role="tab" aria-controls="profile" aria-selected="false">Review</a>
                        </li>

                    </ul>

                    <div class="tab-content" id="myTabContenttt">
                        <div class="tab-pane fade show active" id="homeeewe" role="tabpanel" aria-labelledby="home-tab">
                            <div class="dicription">
                                {!! $pro_data->pro_desc !!}
                            </div>

                        </div>
                        <div class="tab-pane fade " id="profileeerw" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="dicription">
                                @include("front.subviews.category.product_page_components.reviews")
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>



    <?php if(count($related_products)): ?>
        <div class="container">
            <div class="container-fluid">

                <div class="cat_pro">
                    <h4>
                        More Products from
                        <a href="#" class="category">
                            {{$pro_data->youtuber_name}}
                        </a>
                        you might like:
                    </span>
                    </h4>
                </div>

            </div>

            <div class="container">
                <div class="row">
                    <?php foreach($related_products as $key=>$pro_obj): ?>
                        <div class="col-md-3">
                            @include("front.subviews.category.pro_block")
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

@endsection

