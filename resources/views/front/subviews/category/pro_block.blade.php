<div class="product">
    <a href="{{url("show_product?pro_id=$pro_obj->pro_id")}}" title="test">
        <img src="{{get_image_or_default($pro_obj->small_img_path)}}" class="img-fluid">
        <div class="detail">
            <p class="title">
                {{$pro_obj->pro_name}}
            </p>
            <p class="desc">
                by {{$pro_obj->youtuber_name}}
            </p>
        </div>
    </a>
</div>