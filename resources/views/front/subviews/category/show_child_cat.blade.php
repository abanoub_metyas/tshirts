@extends("front.main_layout")
@section("subview")

    <div class="container-fluid">
        <div class="firstbackground" style="background-image: url('{{get_image_or_default($cat_obj->small_img_path)}}')">
            <div class="row">
                <div class="col-md-6">
                    <h1>{{$cat_obj->cat_name}}</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" style="margin-top: 20px;">
        <div class="firstbackground_margin">
            <div class="row">

                <div class="col-md-12" style="margin-top: 10px;">

                    {!! $cat_obj->cat_body !!}

                    <hr>
                </div>


                <?php foreach($cat_products as $key=>$pro_obj): ?>
                    <div class="col-md-3">
                        @include("front.subviews.category.pro_block")
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>



@endsection