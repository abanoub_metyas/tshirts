<div class="row">
    <div class="col s12 m4 l5">
        <?php if(isset_and_array($pro_data->slider_imgs)): ?>

        <div class="img-larg">
            <div id="surround">


                <img class="cloudzoom" alt="Cloud Zoom small image" id="zoom1"
                     src="{{url("/".$pro_data->slider_imgs[0]->path)}}"
                     data-cloudzoom='zoomSizeMode:"image",autoInside: 550'>
                <div id="slider1">
                    <div class="thumbelina-but horiz left">&#706;</div>
                    <ul>
                        <?php foreach($pro_data->slider_imgs as $key=>$img): ?>
                        <li>
                            <img class='cloudzoom-gallery' src="{{url("/$img->path")}}"
                                 data-cloudzoom="useZoom:'.cloudzoom', image:'{{url("/$img->path")}}' ">
                        </li>
                        <?php endforeach;?>
                    </ul>
                    <div class="thumbelina-but horiz right">&#707;</div>
                </div>
            </div>
        </div>

        <?php endif; ?>
    </div>

    <div class="col s12 m8 l7">

        <div class="dit-page-pro">

            <div class="col s12 m12 l12">
                <h1>{{$pro_data->pro_name}}</h1>
            </div>


            <div class="col s12 m12 l12">
                <div class="price-pro">{{($pro_data->sell_price - $pro_data->pro_discount)}}</div>
            </div>

            <div class="col s12 m12 l12">
                <div class="available">
                    <i class="material-icons dp48">done</i>
                    {{show_content($product_page_keywords,"avaliable_label")}}
                    <?php if($pro_data->pro_quantity > 0): ?>
                        {{show_content($product_page_keywords,"avaliable_text")}}
                    <?php else: ?>
                        {{show_content($product_page_keywords,"unavaliable_text")}}
                    <?php endif; ?>
                </div>
            </div>

            <?php if(false): ?>
            <div class="col s12 m12 l1">
                <h6>الكمية </h6>
            </div>

            <div class="col s6 m3 l2 total">
                <input placeholder="1" type="number">
            </div>
            <?php endif; ?>


            <div class="col s6 m3 l3">
                <div class="add-cart color-2 btn waves-effect waves-ligh"><a href="#" data-proid="{{$pro_data->pro_id}}" class="select_product_item"> {{show_content($keywords,"add_to_cart")}}</a></div>
            </div>

            <div class="col s6 m4 l4">
                <div class="add-cart color-1 btn waves-effect waves-light"><a href="#" data-pro_id="{{$pro_data->pro_id}}" class="add_to_wishlist"> {{show_content($keywords,"add_to_wishlist_text")}}</a></div>
            </div>

        </div>

    </div>
</div>