<form action="{{url("/submit_review?pro_id=$pro_data->pro_id")}}" method="post">
    {!! csrf_field() !!}

    <div class="rating">
        <span>Overall Rating {{$pro_data->pro_stars}}</span>
        {!! draw_stars($pro_data->pro_stars) !!}
    </div>

    <div id="review">
        <?php if($product_reviews->count()==0): ?>

            <p>There are no reviews for this product.</p>

        <?php else: ?>

            <?php foreach($product_reviews as $key=>$product_rev): ?>
                <p>
                    <strong>{{$product_rev->review_name}}</strong> -
                    <span class="auto-width">
                        {!! draw_stars($product_rev->review_rate)  !!}
                    </span>

                    <br>
                    {!! $product_rev->review_text !!}
                </p>
            <hr>
            <?php endforeach; ?>

        <?php endif; ?>

    </div>

    <div class="review-form-title">
        <span class="btn btn-primary close-tab" id="reviews_form_title">Write a review</span>
    </div>

    <div class="product-review-form" id="reviews_form">

        <div class="form-group required">
            <div class="col-sm-12">
                <label class="control-label" for="input-name">Your Name</label>
                <input type="text" name="rev_name" value="" id="input-name" class="form-control">
            </div>
        </div>
        <div class="form-group required">
            <div class="col-sm-12">
                <label class="control-label" for="input-review">Your Review</label>
                <textarea name="rev_text" rows="5" id="input-review" class="form-control"></textarea>

                <div class="help-block">
                    <span class="text-danger">Note:</span> HTML is not translated!
                </div>
            </div>
        </div>
        <div class="form-group required">
            <div class="col-sm-12 cont">
                <label class="control-label">Rating</label>
                Bad
                <input type="radio" name="rev_rate" value="1">

                <input type="radio" name="rev_rate" value="2">

                <input type="radio" name="rev_rate" value="3">

                <input type="radio" name="rev_rate" value="4">

                <input type="radio" name="rev_rate" value="5">
                Good
            </div>
        </div>

        <div class="buttons clearfix">
            <div class="pull-right">
                <button type="submit" id="button-review" data-loading-text="Loading..." class="btn btn-primary ani">
                    Continue
                    <span class="over">
                        Continue
                    </span>
                    <span class="out">

                    </span>
                </button>
            </div>
        </div>

    </div>
</form>