@include("front.subviews.category.product_page_components.checkout_modal")

<form>
    <input type="hidden" class="selected_product_quantity">
    <input type="hidden" class="selected_product_size">

    <label class="">
        Size
    </label>
    <select class="form-control pro_available_sizes">
        <option>
            --- select option ---
        </option>
    </select>
    <br>

    <label class="">
        Color
    </label>

    <div class="btn-group colors" data-toggle="buttons" >

        <?php foreach($pro_quantities as $key=>$pro_quantity): ?>

            <label
                data-pro_available_sizes="{{$pro_quantity->pro_available_sizes}}"
                data-pro_color="{{$pro_quantity->pro_color}}"
                data-pro_price="{{$currency_symbol=="USD"?($pro_quantity->pro_price*$usd_rate):$pro_quantity->pro_price}}"
                data-pro_discount_amount="{{$currency_symbol=="USD"?($pro_quantity->pro_discount_amount*$usd_rate):$pro_quantity->pro_discount_amount}}"
                data-pro_quantity_id="{{$pro_quantity->pq_id}}"
                class="select_color_btn btn {{$key==0?"active":""}}"
                style="background-image: url({{get_image_or_default($pro_quantity->path)}});"

            >
                <input type="radio" name="options" id="option3" autocomplete="off" checked>
                <span class="fas fa-check"></span>
                <br>
                <strong>
                    {{$pro_quantity->pro_color}}
                </strong>
            </label>
        <?php endforeach; ?>

    </div>

    <br><br>

    <label class="">
        QYT
    </label>
    <input type="number" class="fo_number order_quantity" min="1" value="1">
    <br><br>

    <div class="price load_price">
        <span class="price-old">

            {{$currency_symbol}}
            <span class="price_val"></span>
        </span>
        <span class="price-new">
            {{$currency_symbol}}
            <span class="price_val"></span>
        </span>
    </div>
    <br>

    <button type="button" class="btn btn-primary add_to_cart" data-pro_id="{{$pro_data->pro_id}}">
        ADD TO CART
        <span class="over">Add to Cart</span><span class="out">Add to Cart</span>
    </button>

</form>