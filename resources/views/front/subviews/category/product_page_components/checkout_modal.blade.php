<div class="lastreq modal fade " id="add_to_cart_modal" tabindex="-1" role="dialog" aria-labelledby="add_to_cart_modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 col-6">
                            <img src="{{get_image_or_default($pro_data->small_img_path)}}" class="img-fluid">
                        </div>
                        <div class="col-md-8 col-6 text-left">
                            <p>
                                <strong>{{$pro_data->pro_name}}</strong> has been added to your Cart !
                            </p>
                            <p>
                                <strong>
                                    Color :
                                </strong>
                                <span class="selected_color_name">

                                </span>
                            </p>
                            <p>
                                <strong>
                                    size :
                                </strong>
                                <span class="selected_size_name">

                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#" class="popupstyle">
                    <button type="button" class="btn btn-secondary popupstyle" data-dismiss="modal">
                        <span class="over">Continue Shopping</span>
                        <span class="out">Continue Shopping</span>
                    </button>
                </a>

                <a href="{{url(is_object($current_user)?"/user/checkout":"/visitor/checkout")}}" class="popupstyle">
                    <button type="button" class="btn btn-primary popupstyle">
                        <span class="over ofinal">
                            Buy Now
                            <img src="{{url("/")}}/public/front/images/cash.png" class="img-fluid">
                        </span>
                        <span class="out ofinal">
                            Buy Now
                            <img src="{{url("/")}}/public/front/images/cash.png" class="img-fluid">
                        </span>
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>