@extends('front.main_layout')

@section('subview')



    <!--=================================
page-title-->

    <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url({{url("/")}}/public/layout/images/bg/bg3.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title-name">
                        <h1>About us</h1>
                        <p>We know the secret of your success</p>
                    </div>
                    <ul class="page-breadcrumb">
                        <li><a href="{{url("/")}}"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><span>About us </span> </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!--=================================
page-title -->


    <!--=================================
About-->

    <section class="page-section-ptb">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-flex justify-content-center align-items-center fadeInLeftBig wow">
                    <img src="{{url("/")}}/public/layout/images/logo-blue-01.png" alt="gict logo" class="img-fluid">
                </div>
                <div class="col-lg-6 sm-mt-30 fadeInRightBig wow">
                    <div class="section-title mb-20">
                        <h6>Who we are and what we do </h6>
                        <h2>Get to know us better. </h2>

                    </div>


                    <p>Global ICT Connections (GICT) is a leading online trading platform for buying and selling Information and Communications Technology (ICT) equipment. We bring together buyers and sellers on a single platform, create high-value matches
                        utilising our proprietary analytics, and help international traders transact in a simple, secure, and swift manner.</p>

                    <p>Designed and built by industry experts, our platform boasts a wide range of features that make online trading of ICT equipment more efficient and value-adding for you. As a technology-driven, client-oriented firm, GICT values feedback
                        provided by traders and rolls out new features and upgrades regularly to offer a highly productive trading experience to its customers.</p>
                    <a class="button mt-20" href="#">Start your free trial today!</a>
                </div>
            </div>
        </div>
    </section>

    <!--=================================
About-->


    <!--=================================
Our office  -->


    <section class="theme-bg page-section-ptb">
        <div class="container">
            <div class="row about-items bounceInLeft wow">
                <div class="col-lg-6 col-md-6 col-sm-12 mb-80 about-text">
                    <h1 class="text-white">01<span class="text-black">.</span></h1>
                    <h3 class="text-white">HIGH-TECH FLEXIBLE BROADCAST SYSTEM</h3>
                    <p class="text-white">Our sophisticated broadcast system saves you time by delivering the most relevant broadcast lists right to your inbox. This offers great convenience to buyers as they don’t have to login into our website to check the latest listings.
                        Similarly, if you are a seller, you can categorise the ICT equipment you are looking to sell into the right lists without even logging into the website. Our system is highly adaptable, which means that you do not have to list
                        cables or TFTs in the Computers’ list. Just get in touch with us and we will list your equipment in the right category. What’s more, you can also target online traders according to their country, location, or activity.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 mb-80 about-img">
                    <img src="{{url("/")}}/public/layout/images/about/about-flexible-01.png" class="img-fluid" alt="Flexible Broadcast">
                </div>
            </div>
            <div class="row about-items bounceInRight wow">
                <div class="col-lg-6 col-md-6 col-sm-12 mb-80 about-img">
                    <img src="{{url("/")}}/public/layout/images/about/about-verification-01.png" class="img-fluid" alt="Flexible Broadcast">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 mb-80 about-text">
                    <h1 class="text-white">02<span class="text-black">.</span></h1>
                    <h3 class="text-white">ONLINE VAT NUMBER VERIFICATION & CURRENCY CONVERSION</h3>
                    <p class="text-white">As a trader, it is your legal obligation to ensure that the international buyer or seller you are trading with is subject to VAT as otherwise you may have to pay the VAT yourself. To help you make smarter, risk-free trading decisions,
                        we provide online VAT number verification services that help you determine if the VAT number provided by a trading partner is valid or not. In addition, with currency convertor, you can easily determine how much the equipment
                        you desire would cost you in your local currency, and then make the right purchasing decisions quickly.</p>
                </div>
            </div>
            <div class="row about-items bounceInLeft wow">
                <div class="col-lg-6 col-md-6 col-sm-12 mb-80 about-text">
                    <h1 class="text-white">03<span class="text-black">.</span></h1>
                    <h3 class="text-white">LOGISTICS SOLUTIONS ON BOARD</h3>
                    <p class="text-white">As a pioneer online trading platform for buying and selling ICT equipment, GICT has an extensive business network comprising of leading international logistics suppliers. Leveraging on our professional relationships, we are able
                        to provide our traders the most affordable pricing for dispatching ICT equipment all across the world. Whether you want to send a single parcel, ship a container, or need assistance with storage of goods, GICT has you covered.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 mb-80 about-img">
                    <img src="{{url("/")}}/public/layout/images/about/about-logistic-01.png" class="img-fluid" alt="Flexible Broadcast">
                </div>
            </div>
            <div class="row about-items bounceInRight wow">
                <div class="col-lg-6 col-md-6 col-sm-12 mb-80 about-img">
                    <img src="{{url("/")}}/public/layout/images/about/about-less-01.png" class="img-fluid" alt="Flexible Broadcast">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 mb-80 about-text">
                    <h1 class="text-white">04<span class="text-black">.</span></h1>
                    <h3 class="text-white">LOW-COST MEMBERSHIP</h3>
                    <p class="text-white">Despite being one of the most feature-rich online trading platforms, we offer a range of affordable membership packages so that every business, regardless of its size, can benefit from our expertise. This makes GICT the most affordable
                        B2B platform available for the trading of ICT equipment in the world – one that is highly secure, reliable, and adaptable to the needs of traders.</p>
                </div>
            </div>
            <div class="row about-items bounceInLeft wow">
                <div class="col-lg-6 col-md-6 col-sm-12 mb-80 about-text">
                    <h1 class="text-white">05<span class="text-black">.</span></h1>
                    <h3 class="text-white">EXTENSIVE TRADING COMMUNITY</h3>
                    <p class="text-white">Our network comprises of a larger number of buyers, sellers, distributors, and ICT professionals from all over the world. We value the trust of our customers in our services, and therefore, we have developed an extensive evaluation
                        process to safeguard our online trading community from any kind of risk and malpractices. In addition, to offer you better trading opportunities, we are making efforts to bring at least 100 universities and colleges from the
                        UK on-board.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 mb-80 about-img">
                    <img src="{{url("/")}}/public/layout/images/about/about-globe-01.png" class="img-fluid" alt="Flexible Broadcast">
                </div>
            </div>
        </div>

    </section>
    <!--=================================
 Our office -->










@endsection


