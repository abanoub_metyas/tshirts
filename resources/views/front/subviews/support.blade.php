@extends('front.main_layout')

@section('addJs')
    <script src="{{url("/")}}/public/layout/js/plugins-jquery.js"></script>
@endsection

@section('subview')





    <!--=================================
page-title -->

    <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url({{url("/")}}/public/layout/images/bg/bg.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title-name">
                        <h1>{{show_content($contact_us,"header")}}</h1>
                        <p>We know the secret of your success</p>
                    </div>
                    <ul class="page-breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i>
                        </li>
                        <li><span>{{show_content($contact_us,"header")}}</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!--=================================
page-title -->





    <!--=================================
contact -->

    <section class="contact-4 page-section-ptb clearfix o-hidden">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        {!! show_content($contact_us,"body") !!}
                    </div>
                    <div id="formmessage">
                        <?php if(isset($msg)):?>
                        {!! $msg !!}
                        <?php endif; ?>
                    </div>

                    <form id="contactform" role="form" method="" action="">
                        <div class="contact-form clearfix">
                            <div class='row'>
                                <div class="col-md-6">
                                    <input id="first_name" type="text" required
                                           placeholder="{{show_content($contact_us,"form_first_name")}}*"
                                           class="form-control check_valid">
                                </div>
                                <div class="col-md-6">
                                    <input id="last_name" type="text" required
                                           placeholder="{{show_content($contact_us,"form_last_name")}}*"
                                           class="form-control check_valid">
                                </div>
                                <div class="col-md-12">
                                    <input type="email" id="email"
                                           placeholder="{{show_content($contact_us,"form_email")}}*" required
                                           class="form-control check_valid">
                                </div>
                                <div class="col-md-6">
                                    <input id="subject" type="text"
                                           placeholder="{{show_content($contact_us,"form_subject")}}*"
                                           class="form-control check_valid">
                                </div>
                                <div class="col-md-6">
                                    <select class="form-control pt-0 pb-0 check_valid" id="reason_for_enquiry" required>
                                        <option disabled selected
                                                value="">{{show_content($contact_us,"reason_for_enquiry")}}</option>
                                        <option value="Customer Service">{{show_content($contact_us,"customer_service")}}</option>
                                        <option value="Suggest improvements">{{show_content($contact_us,"suggest_improvements")}}</option>
                                        <option value="General enquiry">{{show_content($contact_us,"general_enquiry")}}</option>
                                        <option value="Leave Feedback">{{show_content($contact_us,"leave_feedback")}}</option>
                                    </select>


                                </div>
                                <div class="col-md-12 textarea">
                                    <textarea class="input-message check_valid form-control"
                                              placeholder="{{show_content($contact_us,"form_message")}}*" rows="5"
                                              name="message"></textarea>
                                </div>

                            </div>
                            <!-- Google reCaptch-->
                            <!-- <div class="g-recaptcha section-field clearfix" data-sitekey="[Add your site key]"></div> -->
                            <div class="submit-button float-right">
                                <button class="button contact_us_btn" id="contact_us_btn" type="submit">
                                    <span>  {{show_content($contact_us,"button_text")}}</span>
                                    <i class="fa fa-paper-plane"></i>
                                </button>

                            </div>
                        </div>
                    </form>
                    <!-- <div id="ajaxloader" style="display:none"><img class="mx-auto mt-30 mb-30 d-block" src="images/pre-loader/loader-02.svg" alt=""></div> -->
                </div>
            </div>

        </div>
    </section>

    <!--=================================
ADDRESS -->







    <section class="page-section-ptb contact-2 gray-bg wow bounceInLeft" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: bounceInLeft;">
        <div class="container">

            <?php if(
                isset($contact_us->slider1) &&
                isset($contact_us->slider1->imgs) &&
                isset($contact_us->slider1->other_fields)
            ):?>
                    <?php foreach($contact_us->slider1->imgs as $key => $image): ?>
                        <?php if($key % 2 == 0 ):?>
                            <div class="row mb-30 p-5 white-bg wow bounceInRight" data-wow-delay=".{{$key+1}}s" style="visibility: visible; animation-delay: 0.2s; animation-name: bounceInRight;">
                            <div class="col-3">
                                <img src="{{$image->path}}" class="img-fluid" alt="Egypt Office">
                            </div>
                            <div class="col-9">
                                <div class="office-1 ">
                                    <h2 class="mb-30">{{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_name",$key)}}</h2>
                                    <div class="touch-in">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="media">
                                                    <div class="feature-icon media-icon mr-4">
                                                        <span class="{{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_address_icon",$key)}} theme-color"></span>
                                                    </div>
                                                    <div class="media-body">
                                                        <h5 class="text-back">{{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_address_title",$key)}}</h5>
                                                        <p class="mb-0">
                                                            {{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_address_content",$key)}}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="media">
                                                    <div class="feature-icon media-icon mr-4">
                                                        <span class="{{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_phone_icon",$key)}} theme-color"></span>
                                                    </div>
                                                    <div class="media-body">
                                                        <h5 class="text-back">{{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_phone_title",$key)}}</h5>
                                                        <p class="mb-0">{{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_phone_number",$key)}}</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php else:?>
                            <div class="row mb-30 p-5 white-bg wow bounceInLeft" data-wow-delay=".{{$key+1}}s" style="visibility: visible; animation-delay: 0.3s; animation-name: bounceInLeft;">
                            <div class="col-3">
                                <img src="{{$image->path}}" class="img-fluid" alt="New York">
                            </div>
                            <div class="col-9">
                                    <div class="office-1 ">
                                        <h2 class="mb-30">{{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_name",$key)}}</h2>
                                        <div class="touch-in">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="media">
                                                        <div class="feature-icon media-icon mr-4">
                                                            <span class="{{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_address_icon",$key)}} theme-color"></span>
                                                        </div>
                                                        <div class="media-body">
                                                            <h5 class="text-back">{{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_address_title",$key)}}</h5>
                                                            <p class="mb-0">
                                                                {{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_address_content",$key)}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="media">
                                                        <div class="feature-icon media-icon mr-4">
                                                            <span class="{{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_phone_icon",$key)}} theme-color"></span>
                                                        </div>
                                                        <div class="media-body">
                                                            <h5 class="text-back">{{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_phone_title",$key)}}</h5>
                                                            <p class="mb-0">{{show_content_for_other_fields($contact_us->slider1->other_fields,"branch_name",$key)}}</p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <?php endif;?>
                    <?php endforeach; ?>

            <?php endif;?>
        </div>
    </section>





@endsection


