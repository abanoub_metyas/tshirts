
<input type="hidden" class="country_shipping_values" value="{{json_encode($countries->groupBy("country_name")->all())}}">

<input type="hidden" class="city_shipping_city_rate" value="{{$currency_symbol=="USD"?$usd_rate:1}}">
<input type="hidden" class="city_shipping_values" value="{{json_encode($cities->groupBy("country_city_name")->all())}}">

<?php

    $city_names=$cities->pluck("city_name")->all();
    $city_dependences=$cities->pluck("country_name")->all();

    $empty_city_names=[];
    $empty_city_dependences=[];

    foreach($countries->pluck("country_name")->all() as $country){
        $empty_city_names[]="";
        $empty_city_dependences[]=$country;
    }

    $city_names=array_merge($empty_city_names,$city_names);
    $city_dependences=array_merge($empty_city_dependences,$city_dependences);

    if ($currency_symbol=="EGP"){
        echo generate_depended_selects(
            $field_name_1="add_country",
            $field_label_1="Select Country",
            $field_text_1=$countries->pluck("country_name")->all(),
            $field_values_1=$countries->pluck("country_name")->all(),
            $field_selected_value_1="",
            $field_required_1="required",
            $field_class_1="form-control select_add_country",
            $field_name_2="add_governorate",
            $field_label_2="Select Governorate",
            $field_text_2=$city_names,
            $field_values_2=$city_names,
            $field_selected_value_2="",
            $field_2_depend_values=$city_dependences,
            $field_required_2="required",
            $field_class_2="form-control select_add_governorate"
        );
    }
    else{
        echo generate_select_tags(
            $field_name="add_country",
            $label_name="Select Country",
            $text=$countries->pluck("country_name")->all(),
            $values=$countries->pluck("country_name")->all(),
            $selected_value=[""],
            $class="form-control select_add_outside_country",
            $multiple="",
            $required="",
            $disabled = "",
            $data = "",
            $grid = "col-md-6 pull-left",
            $hide_label=false,
            $remove_multiple = false
        );
    }


    $normal_tags=array(
        'add_city',
        'add_full_name',
        'add_email',
        'add_street',
        'add_tel_number',
        'add_post_code',
        'add_notes',
    );

    if ($currency_symbol=="USD"){
        $normal_tags=array_merge(["add_governorate"],$normal_tags);
    }



    $attrs = generate_default_array_inputs_html(
        $normal_tags,
        "",
        "yes",
        $required="required",
        " col-6 pull-left"
    );

    $attrs[0]["add_full_name"]="Full Name";
    $attrs[0]["add_email"]="Email";
    $attrs[0]["add_governorate"]="Governorate";
    $attrs[0]["add_city"]="City";
    $attrs[0]["add_street"]="Street";
    $attrs[0]["add_tel_number"]="Mobile number";
    $attrs[0]["add_post_code"]="Post code";
    $attrs[0]["add_notes"]="Notes";

    $attrs[3]["add_notes"]="textarea";

    if ($currency_symbol=="EGP"){
        $attrs[2]["add_post_code"]="";
    }


    echo
    generate_inputs_html_take_attrs($attrs);


    echo generate_select_tags(
        $field_name="add_type",
        $label_name="Address Type",
        $text=["home","work"],
        $values=["home","work"],
        $selected_value="",
        $class="form-control",
        $multiple="",
        $required="",
        $disabled = "",
        $data = "",
        $grid=" col-6 pull-left",
        $show_label_as_option=false
    );

?>