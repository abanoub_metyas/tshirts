<div class="card" style="margin-top: 20px;">
    <div class="card-header bg-primary text-white">
        Add new address
    </div>
    <div class="card-body text-black add_new_address ">


        <form action="" method="post" id="add_new_address_form">

            {!! csrf_field() !!}

            @include("front.subviews.order.checkout_components.add_address_inner_form")


        </form>


    </div>
</div>