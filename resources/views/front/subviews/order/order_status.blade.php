@extends('front.main_layout')

@section('subview')

    <div class="container container_height">
        <div class="row">
            <div class="col-12">

                <div class="card card_margin">
                    <div class="card-header bg-primary text-white">Order Status</div>
                    <div class="card-body text-black">

                        <?php if($order_status=="done"): ?>
                            <p style="text-align: center;display: block;width: 250px;height: 250px;border: 1px solid green;border-radius: 100%;font-size: 57px;color: green;margin:  0 auto;">
                                <i class="fa fa-check" aria-hidden="true" style="margin-top: 87px;"></i>
                            </p>

                            <h1 style="text-align: center;">
                                Thank you. <br>
                                Your order is waiting for customer service's approve <br>
                                your order id is <strong>{{$order_id}}</strong> <br>
                                <strong>{{$order_msg}}</strong>
                            </h1>

                        <?php else: ?>
                            <p style="text-align: center;display: block;width: 250px;height: 250px;border: 1px solid red;border-radius: 100%;font-size: 57px;color: red;margin:  0 auto;">
                                <i class="fa fa-thumbs-down" aria-hidden="true" style="margin-top: 87px;"></i>
                            </p>
                            <h1 style="text-align: center;">
                                Error happened!!
                            </h1>
                        <?php endif; ?>



                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection