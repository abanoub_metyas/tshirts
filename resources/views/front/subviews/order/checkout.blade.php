@extends('front.main_layout')

@section('subview')



    <script src="{{url("/")}}/public/jscode/order.js"></script>

    <form action="{{url("/visitor/checkout")}}" method="post">

        <div class="container container_height checkout_page" style="margin-top: 10px;margin-bottom: 10px;">
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-info">
                        <strong>You can login to make the checkout as user</strong>
                    </div>

                </div>

                <div class="col-12">
                    <div class="row">
                        <div class="col-6">
                            @include("user.subviews.order.checkout_components.list_orders")
                        </div>

                        <div class="col-6">
                            @include("front.subviews.order.checkout_components.add_address")

                        </div>

                        <div class="col-12">
                            @include("user.subviews.order.checkout_components.finish_order")

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>




@endsection

