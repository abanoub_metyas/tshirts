@extends('front.main_layout')

@section('subview')

    <div class="container container_height">
        <div class="row">
            <div class="col-12">

                <div class="card card_margin">
                    <div class="card-header bg-primary text-white">Complete Payment</div>
                    <div class="card-body text-black">
                        <iframe src="{{$iframe_link}}" frameborder="0" style="width: 100%;min-height: 600px;">
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection