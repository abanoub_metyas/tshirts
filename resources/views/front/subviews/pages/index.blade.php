@extends("front.main_layout")
@section("subview")

    <div class="container-fluid">
        <div class="firstbackground" style="background-image: url('{{get_image_or_default($page_data->big_img_path)}}')">
            <div class="row">
                <div class="col-md-6">
                    <h1>{{$page_data->page_title}}</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" style="margin-top: 20px;">
        <div class="firstbackground_margin">
            <div class="row">
                <div class="col-md-6">
                    {!! $page_data->page_body !!}
                </div>
            </div>
        </div>
    </div>



@endsection