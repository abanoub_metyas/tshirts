@extends("front.main_layout")
@section("subview")

    <div class="container-fluid">
        <div class="firstbackground" style="background:url('{{show_content($homepage_content,"search_background",true)}}')">
            <div class="row">
                <div class="col-md-6">
                    <h1>
                        {{show_content($homepage_content,"search_title")}}
                    </h1>
                    <div class="search">
                        <form method="get" action="{{url("/pages/search")}}">
                            <i class="fas fa-search"></i>
                            <input type="text" value="{{$search_keyword}}" name="search_keyword" placeholder="{{show_content($homepage_content,"search_placeholder")}}" class="form-control">
                            <button type="submit" class="btn btn-danger">
                                {{show_content($homepage_content,"search_button_text")}}
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid container_height" style="margin-top: 20px;">
        <div class="firstbackground_margin">
            <div class="row">

                <?php foreach($products as $key=>$pro_obj): ?>
                <div class="col-md-3">
                    @include("front.subviews.category.pro_block")
                </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>

@endsection