@extends("front.main_layout")
@section("subview")

    {{--search box--}}
    <div class="container-fluid">
        <div class="firstbackground" style="background:url('{{show_content($homepage_content,"search_background",true)}}')">
            <div class="row">
                <div class="col-md-6">
                    <h1>
                        {{show_content($homepage_content,"search_title")}}
                    </h1>
                    <div class="search">
                        <form method="get" action="{{url("/pages/search")}}">
                            <i class="fas fa-search"></i>
                            <input type="text" name="search_keyword" placeholder="{{show_content($homepage_content,"search_placeholder")}}" class="form-control">
                            <button type="submit" class="btn btn-danger">
                                {{show_content($homepage_content,"search_button_text")}}
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="startdesign">
            <div class="title">
                <span>
                    {!! show_content($homepage_content,"registration_text") !!}
                </span>
                <a href="{{url("/register")}}" class="btn btn-danger">
                    {{show_content($homepage_content,"registration_button_text")}}
                </a>
            </div>

        </div>
    </div>

    <?php foreach($product_sub_cats as $key=>$two_cats): ?>
        <?php foreach($two_cats as $key_2=>$cat_obj): ?>
            <?php
                if(!isset($products[$cat_obj->cat_id]))continue;
            ?>

            {{--category box--}}
            <div class="container-fluid">
                <div class="recommend">
                    <h2>
                        {{$cat_obj->cat_name}}
                    </h2>
                </div>

            </div>
            <div class="container">
                <div class="row">
                    <?php foreach($products[$cat_obj->cat_id] as $key_3=>$pro_obj): ?>
                        <?php
                            if($key>4)break;
                        ?>
                        <div class="col-md-3">
                            @include("front.subviews.category.pro_block")
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endforeach; ?>


        <?php if(
            is_object($homepage_content)&&
            isset($homepage_content->slider1)&&
            isset($homepage_content->slider1->other_fields->link[$key])&&
            isset($homepage_content->slider1->imgs)&&
            isset($homepage_content->slider1->imgs->all()[$key])&&
            is_object($homepage_content->slider1->imgs->all()[$key])
        ): ?>
        <div class="container-fluid">
            <div class="bannerone">
                <a href="{{$homepage_content->slider1->other_fields->link[$key]}}" class="">
                    <img
                        src="{!! get_image_from_obj($homepage_content->slider1->imgs->all()[$key]) !!}"
                        {!! get_image_alt_title($homepage_content->slider1->imgs->all()[$key]) !!}
                        class="img-fluid one"
                    >
                </a>
            </div>
        </div>
        <?php endif; ?>

    <?php endforeach; ?>


    {{--Video--}}
    <?php if(isset($homepage_content->video_iframe)&&!empty($homepage_content->video_iframe)): ?>
    <div class="container-fluid">
        <div class="bannerone video">
            <div class="row">
                <div class="col-md-6">
                    <div id="video" data-iframe="{{show_content($homepage_content,"video_iframe")}}">
                        <img
                            src="{{show_content($homepage_content,"video_initial_image",true)}}"
                            {{get_image_alt_title(is_object($homepage_content)?$homepage_content->video_initial_image:"")}}
                        >
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="last_vid">
                        <h2>
                            {{show_content($homepage_content,"video_title")}}
                        </h2>
                        <span>
                            {!! show_content($homepage_content,"video_text") !!}
                        </span>
                        <div class="">
                            <a href="{{show_content($homepage_content,"video_button_link")}}" class="btn btn-danger">
                                {{show_content($homepage_content,"video_button_text")}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php endif; ?>


@endsection