@extends("front.main_layout")
@section("subview")

    <section class="page-section-ptb contact-2 gray-bg wow bounceInLeft container_height" data-wow-delay=".5s">
        <div class="container">
            <div class="card login_card card-container">
                <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
                <p id="profile-name" class="profile-name-card"></p>
                <form method="post" action="{{url("/register")}}" method="post">

                    {!! csrf_field() !!}

                    <input name="full_name" placeholder="Full name" class="form-control " type="text" style="margin-bottom: 15px;">


                    <input name="email" placeholder="Email" class="form-control " type="text" style="margin-bottom: 15px;">

                    <input name="password" placeholder="Password" class="form-control " type="password" style="margin-bottom: 15px;">
                    <input name="password_confirmation" placeholder="Confirm Password" class="form-control " type="password" style="margin-bottom: 15px;">


                    <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Register</button>
                </form>

                <div class="row">
                    <div class="col-md-12" >
                        <a href="{{url("login/google")}}" class="btn btn-primary btn-block" style="margin-top:10px;background-color: #c71610;">
                            <i class="fab fa-google" style="margin-top: 3px;margin-right: 5px;"></i>
                            Sign in with Google
                        </a>
                    </div>
                    <div class="col-md-12" >
                        <a href="{{url("login/facebook")}}" class="btn btn-primary btn-block" style="margin-top:10px;">
                            <i class="fab fa-facebook"  style="margin-top: 3px;margin-right: 5px;"></i>
                            Sign in with Facebook
                        </a>
                    </div>
                </div>

            </div><!-- /card-container -->

        </div>

    </section>

@endsection




