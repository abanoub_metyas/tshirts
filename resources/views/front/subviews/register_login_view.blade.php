@extends("front.main_layout")

@section("subview")

    <div class="tag-line">
        <div class="container">
            <div class="row  text-center">

                <div class="col-lg-12  col-md-12 col-sm-12">

                    <h2 data-scroll-reveal="enter from the bottom after 0.1s">
                        <i class="fa fa-circle-o-notch"></i>
                        {{show_content($userpanel_register_user_page,"register_header")}}
                        <span style="font-size: 10px;">|</span>
                        {{show_content($homepage,"login_header")}}
                        <i class="fa fa-circle-o-notch"></i>
                    </h2>
                </div>
            </div>
        </div>
    </div>


    <div class="container inner_container">

        <div class="col-md-12">

            <div class="col-md-9">
                @include("blocks.register_block")
            </div>
            <div class="col-md-3">
                <h1 class="my_h1">{{show_content($homepage,"login_header")}}</h1>

                <div class="login_parent_div">

                    <form action="{{url("/login")}}" method="post">

                        {!! csrf_field() !!}

                        <div class="col-md-12 padd-left padd-right">
                            <div class="form-group">
                                <label for="">{{show_content($homepage,"login_email")}}</label>
                                <input type="email" class="form-control" name="email">
                            </div>
                        </div>

                        <div class="col-md-12 padd-left padd-right">
                            <div class="form-group">
                                <label for="">{{show_content($homepage,"login_password")}}</label>
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>


                        <div class="col-md-12 padd-left padd-right">
                            <div class="request_msgs"></div>
                            <button type="submit" class="btn btn-site-primary">
                                {{show_content($homepage,"login_button")}}
                            </button>

                            <a href="{{url("/password/reset")}}">
                                {{show_content($homepage,"forget_password")}}
                            </a>
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>



@endsection