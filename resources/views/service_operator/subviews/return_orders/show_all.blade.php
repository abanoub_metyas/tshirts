@extends('service_operator.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading" data-toggle="collapse" style="cursor: pointer;"  data-target=".collapse">
            Filtration
        </div>
        <div class="panel-body collapse in">

            <form action="" method="GET">

                <?php

                    $normal_tags=array(
                        'youtuber_name',
                        'product_name',
                        'start_date',
                        'end_date',
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $post_data,
                        "yes",
                        $required="",
                        "3"
                    );


                    $attrs[3]["start_date"]="date";
                    $attrs[3]["end_date"]="date";

                    echo generate_inputs_html_take_attrs($attrs);
                ?>

                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Filter</button>
                </div>

            </form>

        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            Return Orders
        </div>
        <div class="panel-body" style="overflow-x: scroll;">

            <div class="alert alert-info">
                <p>Edit|Delete will not effect at printed t-shirts quantities so take care when you will edit|delete row</p>
            </div>

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Youtuber</td>
                        <td>product Name</td>
                        <td>Material type</td>
                        <td>Color</td>
                        <td>Size</td>
                        <td>Quantity</td>
                        <td>Date</td>
                        <td>Edit</td>
                        <td>Remove</td>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($all_return_orders as $key => $return_order): ?>
                        <tr id="row<?= $return_order->ro_id ?>">
                            <td><?=$key+1?></td>
                            <td>
                                {{$return_order->youtuber_email}} <br>
                                {{$return_order->youtuber_name}}
                            </td>
                            <td>{{$return_order->pro_name}}</td>
                            <td>{{$return_order->pro_material_type}}</td>
                            <td>{{$return_order->ro_color}}</td>
                            <td>{{$return_order->ro_size}}</td>
                            <td>{{$return_order->ro_quantity}}</td>
                            <td>{{date("Y-m-d",strtotime($return_order->created_at))}}</td>
                            <td>
                                <a href="<?= url("service_operator/return_orders/save/$return_order->pro_id/$return_order->ro_id") ?>">
                                    <span class="label label-info">
                                        <i class="fa fa-edit"></i>
                                    </span>
                                </a>
                            </td>

                            <td>
                                <a
                                    href='#'
                                    class="general_remove_item"
                                    data-tablename="App\models\bills\return_orders_m"
                                    data-deleteurl="<?= url("/general_remove_item") ?>"
                                    data-itemid="<?= $return_order->ro_id  ?>"
                                >
                                    <span class="label label-danger">
                                        <i class="fa fa-remove"></i>
                                    </span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>

            </table>
        </div>
    </div>

@endsection
