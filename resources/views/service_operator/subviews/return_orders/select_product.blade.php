@extends('service_operator.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading" data-toggle="collapse" style="cursor: pointer;"  data-target=".collapse">
            Filtration
        </div>
        <div class="panel-body collapse in">

            <form action="" method="GET">

                <?php

                    $normal_tags=array(
                        'youtuber_name',
                        'product_name',

                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $post_data,
                        "yes",
                        $required="",
                        "6"
                    );


                    echo generate_inputs_html_take_attrs($attrs);
                ?>

                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Filter</button>
                </div>

            </form>

        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            Products
        </div>
        <div class="panel-body" style="overflow-x: scroll;">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Youtuber</td>
                        <td>product Name</td>
                        <td>return order</td>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($all_products as $key => $product): ?>
                        <tr id="row<?= $product->pro_id ?>">
                            <td><?=$key+1?></td>
                            <td>
                                {{$product->youtuber_email}} <br>
                                {{$product->youtuber_name}}
                            </td>
                            <td>{{$product->pro_name}}</td>
                            <td>
                                <a href="{{url("service_operator/return_orders/save/$product->pro_id")}}">
                                    Return Product
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>

            </table>
        </div>
    </div>

@endsection
