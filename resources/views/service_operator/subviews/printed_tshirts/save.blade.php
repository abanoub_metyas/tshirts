@extends('service_operator.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading" data-toggle="collapse" style="cursor: pointer;"  data-target=".collapse">
            {{$row_id==null?"Add new":"Edit"}}
        </div>
        <div class="panel-body collapse in">

            <form action="{{url("/service_operator/printed_tshirts/save/$pro_id/$row_id")}}" method="POST">

                {!! csrf_field() !!}

                <?php

                    echo generate_select_tags(
                        $field_name="pt_size",
                        $label_name="Size",
                        $text=$all_sizes,
                        $values=$all_sizes,
                        $selected_value=[""],
                        $class="form-control",
                        $multiple="",
                        $required="",
                        $disabled = "",
                        $data = $data_obj,
                        $grid = "col-md-4",
                        $hide_label=false,
                        $remove_multiple = false
                    );

                    echo generate_select_tags(
                        $field_name="pt_color",
                        $label_name="Color",
                        $text=$all_colors,
                        $values=$all_colors,
                        $selected_value=[""],
                        $class="form-control",
                        $multiple="",
                        $required="",
                        $disabled = "",
                        $data = $data_obj,
                        $grid = "col-md-4",
                        $hide_label=false,
                        $remove_multiple = false
                    );

                    $normal_tags=[
                        'pt_quantity',
                    ];

                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $data_obj,
                        "yes",
                        $required="required",
                        "4"
                    );

                    $attrs[0]["pt_quantity"]="Printed Quantity";
                    $attrs[3]["pt_quantity"]="number";

                    echo
                    generate_inputs_html_take_attrs($attrs);
                ?>


                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </form>

        </div>
    </div>


@endsection
