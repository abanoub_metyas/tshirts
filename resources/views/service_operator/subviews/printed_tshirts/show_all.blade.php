@extends('service_operator.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading" data-toggle="collapse" style="cursor: pointer;"  data-target=".collapse">
            Filtration
        </div>
        <div class="panel-body collapse in">

            <form action="" method="GET">

                <?php

                    $normal_tags=array(
                        'youtuber_name',
                        'product_name',
                        'start_date',
                        'end_date',
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $post_data,
                        "yes",
                        $required="",
                        "3"
                    );


                    $attrs[3]["start_date"]="date";
                    $attrs[3]["end_date"]="date";

                    echo generate_inputs_html_take_attrs($attrs);
                ?>

                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Filter</button>
                </div>

            </form>

        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            Printed t-shirts
        </div>
        <div class="panel-body" style="overflow-x: scroll;">

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Youtuber</td>
                        <td>product Name</td>
                        <td>Material type</td>
                        <td>Color</td>
                        <td>Size</td>
                        <td>Quantity</td>
                        <td>Edit</td>
                        <td>Remove</td>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($all_printed_tshirts as $key => $printed_tshirt): ?>
                        <tr id="row<?= $printed_tshirt->ro_id ?>">
                            <td><?=$key+1?></td>
                            <td>
                                {{$printed_tshirt->youtuber_email}} <br>
                                {{$printed_tshirt->youtuber_name}}
                            </td>
                            <td>{{$printed_tshirt->pro_name}}</td>
                            <td>{{$printed_tshirt->pro_material_type}}</td>
                            <td>{{$printed_tshirt->pt_color}}</td>
                            <td>{{$printed_tshirt->pt_size}}</td>
                            <td>{{$printed_tshirt->pt_quantity}}</td>
                            <td>
                                <a href="<?= url("service_operator/printed_tshirts/save/$printed_tshirt->pro_id/$printed_tshirt->pt_id") ?>">
                                    <span class="label label-info">
                                        <i class="fa fa-edit"></i>
                                    </span>
                                </a>
                            </td>

                            <td>
                                <a
                                    href='#'
                                    class="general_remove_item"
                                    data-tablename="App\models\bills\return_orders_m"
                                    data-deleteurl="<?= url("/general_remove_item") ?>"
                                    data-itemid="<?= $printed_tshirt->pt_id  ?>"
                                >
                                    <span class="label label-danger">
                                        <i class="fa fa-remove"></i>
                                    </span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>

            </table>
        </div>
    </div>

@endsection
