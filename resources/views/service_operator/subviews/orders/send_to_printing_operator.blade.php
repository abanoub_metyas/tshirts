@extends('service_operator.main_layout')

@section('subview')


    <div class="panel panel-info">
        <div class="panel-heading" data-toggle="collapse" style="cursor: pointer;"  data-target=".collapse">
            Send to printing operator
        </div>
        <div class="panel-body collapse in">

            <form action="{{url("/service_operator/orders/send_to_printing_operator?order_id=".$order_obj->bill_order_id)}}" method="POST">

                {!! csrf_field() !!}

                <?php
                    $normal_tags=array('request_quantity');

                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        "",
                        "yes",
                        "",
                        "12"
                    );

                    $attrs[2]["request_quantity"].=" step='1'";
                    $attrs[3]["request_quantity"]="number";
                    $attrs[4]["request_quantity"]=$order_obj->order_quantity;

                    echo
                    generate_inputs_html_take_attrs($attrs);
                ?>

                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </form>

        </div>
    </div>


@endsection
