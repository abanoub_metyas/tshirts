@extends('service_operator.main_layout')

@section('subview')

    <div class="alert alert-info">
        <p>if you want to add order to this bill, click at the button below</p>
    </div>

    <button
        class="btn btn-primary add_single_product_to_bill"
        data-bill_id="{{$bill_obj->bill_id}}"
        style="margin-bottom: 10px;"
    >
        Add single product
    </button>

    <div class="panel panel-primary">
        <div class="panel-heading">Products</div>
        <div class="panel-body">
            <form action="{{url("/service_operator/orders/edit_bill_orders?bill_id=$bill_obj->bill_id")}}" method="post">

                {!! csrf_field() !!}


                <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">

                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>Material</th>
                        <th>Size</th>
                        <th>Color</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Delete</th>
                    </tr>
                    </thead>

                    <tbody>
                        <?php foreach($bill_orders as $key=>$order): ?>
                            <tr id="row<?= $order->bill_order_id ?>"  class="order_item">
                                <td>
                                    <a href="{{url("/show_product?pro_id=$order->pro_id")}}">
                                        {{$order->pro_name}}
                                    </a>
                                </td>
                                <td>{{$order->pro_material_type}}</td>
                                <td>{{$order->order_selected_size}}</td>
                                <td>{{$order->pro_color}}</td>
                                <td class="order_price">{{$order->pro_price-$order->pro_discount_amount}}</td>
                                <td class="order_quantity">
                                    <input type="hidden" name="order_id[]" value="{{$order->bill_order_id }}">
                                    <input min="1" type="number" class="form-control" name="order_quantity[]" value="{{$order->order_quantity}}">
                                </td>

                                <td class="hide_at_print">

                                    <a
                                        href='#'
                                        class="general_remove_item"
                                        data-deleteurl="<?= url("/service_operator/orders/remove_order") ?>"
                                        data-tablename="App\models\bills\bill_orders_m"
                                        data-itemid="<?= $order->bill_order_id ?>"
                                        data-func_after_delete="calc_order_total_amount"
                                    >
                                        <span class="label label-danger">
                                            <i class="fa fa-remove"></i>
                                        </span>
                                    </a>

                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>

                </table>


                <div class="col-md-12 text-center" style="margin-top: 10px;">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>

            <h1>
                Total Amount: {{$bill_obj->bill_amount}} EGP
            </h1>

        </div>
    </div>


@endsection