@extends('service_operator.main_layout')

@section('subview')


    <div class="panel panel-info">
        <div class="panel-heading" data-toggle="collapse" style="cursor: pointer;"  data-target=".collapse">
            Change Status
        </div>
        <div class="panel-body collapse in">

            <form action="{{url("/service_operator/orders/change_status?bill_id=".$bill_obj->bill_id)}}" method="POST">

                {!! csrf_field() !!}

                <?php
                    echo generate_select_tags(
                        $field_name="bill_status",
                        $label_name="Bill status",
                        $text=$order_status,
                        $values=$order_status,
                        $selected_value=[],
                        $class="form-control",
                        $multiple="",
                        $required="",
                        $disabled = "",
                        $data = $bill_obj,
                        $grid = "col-md-12",
                        $hide_label=false,
                        $remove_multiple = false
                    );

                ?>

                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </form>

        </div>
    </div>


@endsection
