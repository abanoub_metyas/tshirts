@extends('service_operator.main_layout')

@section('subview')

    <style>
        @media print {

           .hide_at_print,
           .fa-edit
           {
                visibility: hidden;
           }
        }
    </style>

    <div class="print_container">

        <button class="btn btn-info print_page hide_at_print" style="margin-bottom: 10px;">
            Print
            <i class="fa fa-print"></i>
        </button>

        <div class="panel panel-primary">
            <div class="panel-heading">Details</div>
            <div class="panel-body">
                <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">

                    <tr>
                        <td>Order Number</td>
                        <td>#{{$bill_obj->bill_id}}</td>
                    </tr>

                    <tr>
                        <td><b>User Address:</b></td>
                        <td>
                            <span> Email: </span>
                            <strong>{{$address_obj->add_email}}</strong> <br>

                            <span> Name: </span>
                            <strong>{{$address_obj->add_full_name}}</strong> <br>

                            <span> Country: </span>
                            <strong>{{$address_obj->add_country}}</strong> <br>

                            <span> Governorate: </span>
                            <strong>{{$address_obj->add_governorate}}</strong> <br>

                            <span> City: </span>
                            <strong>{{$address_obj->add_city}}</strong> <br>

                            <span> Street: </span>
                            <strong>{{$address_obj->add_street}}</strong> <br>

                            <span> Type: </span>
                            <strong>{{$address_obj->add_type}}</strong> <br>

                            <span> Mobile number :</span>
                            <strong>{{$address_obj->add_tel_number}}</strong> <br>

                            <span> Notes: </span>
                            <strong>{{$address_obj->add_notes}}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Shipping Details:</b></td>
                        <td>
                            Shipping Company: <b>{{$bill_obj->delivery_type}}</b> <br>
                            Shipping Cost: <b>{{$bill_obj->delivery_cost}} EGP</b>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Bill cost details</b></td>
                        <td>
                            <span> Bill cost: </span>
                            <strong>{{$bill_obj->bill_amount}} EGP</strong> <br>

                            <span> Bill Paid: </span>
                            <strong>{{$bill_obj->bill_paid_amount}} EGP</strong> <br>

                            <span> Bill Remain: </span>
                            <strong>{{$bill_obj->bill_amount-$bill_obj->bill_paid_amount}} EGP</strong>
                            <br>

                            <?php if($bill_obj->bill_paid_amount>$bill_obj->bill_amount): ?>
                                <span>
                                    you should make a refund of
                                    <strong>
                                        {{$bill_obj->bill_paid_amount-$bill_obj->bill_amount}} EGP
                                    </strong>
                                    to the user
                                </span>
                                <br>
                            <?php endif; ?>




                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <?php if($bill_obj->delivery_type=="fetchr"): ?>
        <div class="panel panel-info hide_at_print">
            <div class="panel-heading">{{capitalize_string($bill_obj->delivery_type)}}</div>
            <div class="panel-body">
                <?php if(empty($bill_obj->shipping_awb_link)): ?>
                    <a
                        class="btn btn-primary confirm_before_go"
                        href="{{url("/service_operator/orders/send_to_fetchr?bill_id=$bill_obj->bill_id")}}"
                    >
                        Send to Fetchr
                    </a>
                <?php else: ?>
                    <a
                        class="btn btn-primary confirm_before_go"
                        href="{{url("/service_operator/orders/cancel_order_fetchr?bill_id=$bill_obj->bill_id")}}"
                    >
                        Cancel Order
                    </a>
                    <a
                        class="btn btn-primary confirm_before_go"
                        href="{{$bill_obj->shipping_awb_link}}"
                    >
                        Get awb
                    </a>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>

        <?php if($bill_obj->delivery_type=="aramex"): ?>
        <div class="panel panel-info hide_at_print">
            <div class="panel-heading">{{capitalize_string($bill_obj->delivery_type)}}</div>
            <div class="panel-body">

                <div class="alert alert-info">
                    <p>You can overwrite old data by click at this button again</p>
                </div>

                <?php if(!empty($bill_obj->shipping_awb_link)): ?>
                    <a
                        target="_blank"
                        class="btn btn-primary confirm_before_go"
                        href="{{$bill_obj->shipping_awb_link}}"
                    >
                        Get awb
                    </a>
                <?php endif; ?>

                <form method="get" action="{{url("/service_operator/orders/send_to_aramex")}}">

                    <input type="hidden" name="bill_id" value="{{$bill_obj->bill_id}}">

                    <div class="col-md-4 form-group">
                        <label for="">Country Code</label>
                        <input type="text" name="aramex_couyntry_code" value="{{get_country_code($address_obj->add_country)}}" class="form-control">
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="">City</label>
                        <input type="text" name="aramex_city" value="{{$address_obj->add_governorate}}" class="form-control">
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="">Post Code</label>
                        <input type="text" name="aramex_post_code" value="{{$address_obj->add_post_code}}" class="form-control">
                    </div>



                    <button
                        class="btn btn-primary confirm_before_go"
                        type="submit"
                    >
                        Send to Aramex
                    </button>
                </form>

            </div>
        </div>
        <?php endif; ?>


        <a
            class="btn btn-primary hide_at_print"
            href="{{url("/service_operator/orders/change_status?bill_id=$bill_obj->bill_id")}}"
            style="margin-bottom: 10px;"
        >
            Change Status
        </a>

        <a
            class="btn btn-primary hide_at_print"
            href="{{url("/service_operator/orders/edit_bill_orders?bill_id=$bill_obj->bill_id")}}"
            style="margin-bottom: 10px;"
        >
            Edit Order
        </a>

        <a
            class="btn btn-primary hide_at_print"
            href="{{url("/complete_payment?bill_id=$bill_obj->bill_id")}}"
            style="margin-bottom: 10px;"
        >
            Generate Payment link
        </a>



        <div class="panel panel-primary">
            <div class="panel-heading">Products</div>
            <div class="panel-body">
                <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">

                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>Product</th>
                        <th>Material</th>
                        <th>Size</th>
                        <th>Color</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th class="hide_at_print">Material is available?</th>
                        <th class="hide_at_print">Already printed t-shirts</th>
                        <th class="hide_at_print">Send to printing operator</th>
                        <th class="hide_at_print">Delete</th>
                    </tr>
                    </thead>

                    <tbody>
                        <?php foreach($bill_orders as $key=>$order): ?>
                            <tr id="row<?= $order->bill_order_id ?>"  class="order_item">
                                <td>
                                    <img src="{{get_image_or_default($order->small_img_path)}}" alt="" width="100">
                                </td>
                                <td>
                                    <a href="{{url("/show_product?pro_id=$order->pro_id")}}">
                                        {{$order->pro_name}}
                                    </a>
                                </td>
                                <td>{{$order->pro_material_type}}</td>
                                <td>{{$order->order_selected_size}}</td>
                                <td>{{$order->pro_color}}</td>
                                <td class="order_price">{{$order->pro_price-$order->pro_discount_amount}}</td>
                                <td class="order_quantity">{{$order->order_quantity}}</td>
                                <td class="hide_at_print">
                                    <?php
                                        $available_or_not="not available";

                                        if(isset($materials[$order->pro_material_type])){
                                            $colors=$materials[$order->pro_material_type]->groupBy("m_color");

                                            if(isset($colors[$order->pro_color])){
                                                $sizes=$colors[$order->pro_color]->groupBy("m_size");

                                                if(isset($sizes[$order->order_selected_size])){
                                                    $available_or_not="available";
                                                }
                                            }
                                        }
                                    ?>
                                    {{$available_or_not}}
                                </td>
                                <td class="hide_at_print">
                                    <?php

                                        if(isset($printed_tshirts[$order->pro_id])){
                                            $colors=$printed_tshirts[$order->pro_id]->groupBy("pt_color");


                                            if(isset($colors[$order->pro_color])){
                                                $sizes=$colors[$order->pro_color]->groupBy("pt_size");

                                                if(isset($sizes[$order->order_selected_size])){
                                                    echo $sizes[$order->order_selected_size]->first()->pt_quantity;
                                                }
                                            }
                                        }
                                    ?>
                                </td>
                                <td class="hide_at_print">
                                    <?php if(isset($printing_requests[$order->bill_order_id])): ?>
                                        {{$printing_requests[$order->bill_order_id][0]->print_status}}

                                    <?php else: ?>

                                        <?php if($available_or_not=="available"): ?>
                                            <a
                                                href="{{url("/service_operator/orders/send_to_printing_operator?order_id=$order->bill_order_id")}}"
                                                class="btn btn-primary"
                                            >
                                                Send
                                            </a>
                                        <?php endif; ?>

                                    <?php endif; ?>

                                </td>
                                <td class="hide_at_print">
                                    <?php if(isset($printing_requests[$order->bill_order_id])): ?>
                                        you can't delete this because it is sent to printing operator
                                    <?php endif; ?>

                                    <?php
                                        if(
                                            !isset($printing_requests[$order->bill_order_id])
                                        ):
                                    ?>

                                        <a
                                            href='#'
                                            class="general_remove_item"
                                            data-deleteurl="<?= url("/service_operator/orders/remove_order") ?>"
                                            data-tablename="App\models\bills\bill_orders_m"
                                            data-itemid="<?= $order->bill_order_id ?>"
                                            data-func_after_delete="calc_order_total_amount"
                                        >
                                            <span class="label label-danger">
                                                <i class="fa fa-remove"></i>
                                            </span>
                                        </a>

                                    <?php endif; ?>

                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>

                </table>

                <h1>
                    Total Amount:
                    <span class="order_total_amount"></span>
                </h1>

            </div>
        </div>

    </div>

@endsection