@include("main_dashboard_components.header")


<div class="page-wrapper">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{url("/service_operator/dashboard")}}">
                    <img src="{{url("/public/img/logo.png")}}" alt="logo" class="logo-default"/>
                </a>
                <div class="menu-toggler sidebar-toggler">
                    <span></span>
                </div>
            </div>

            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                <span></span>
            </a>

            @include("main_dashboard_components.top_menu")
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="page-container">
        <div class="page-sidebar-wrapper">
            <div class="page-sidebar navbar-collapse collapse">
                @include("service_operator.components.menu")
            </div>
        </div>
        <div class="page-content-wrapper">
            <div class="page-content">
                @include("main_dashboard_components.msg")
                @yield("subview")
            </div>
        </div>
    </div>

    <div class="page-footer">
        <div class="page-footer-inner">
            {{date("Y")}} &copy;
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>

</div>


</body>

</html>