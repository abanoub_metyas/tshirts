<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">

    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li>

    <li class="sidebar-search-wrapper">
        <form class="sidebar-search" method="POST">
            <a href="javascript:;" class="remove">
                <i class="icon-close"></i>
            </a>
            <div class="input-group">
                <input type="text" class="form-control filter_menu" placeholder="Search...">
                <span class="input-group-btn">
                    <a href="javascript:;" class="btn">
                        <i class="icon-magnifier"></i>
                    </a>
                </span>
            </div>
        </form>
    </li>

    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("/")}}">
            <i class="fa fa-home"></i>
            <span class="title">Site</span>
        </a>
    </li>

    <?php if(in_array($current_user->user_type,["admin","dev"])): ?>
    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("admin/dashboard")}}">
            <i class="fa fa-dashboard"></i>
            <span class="title">Admin Dashboard</span>
        </a>
    </li>
    <?php endif; ?>

    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("service_operator/dashboard")}}">
            <i class="fa fa-dashboard"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>


    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Orders</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item">
                <a class="nav-link" href="{{url('service_operator/orders/show_all')}}">Show all</a>
            </li>
        </ul>
    </li>

    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Return orders</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item">
                <a class="nav-link" href="{{url('service_operator/return_orders/show_all')}}">Show all</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('service_operator/return_orders/select_product')}}">Select product</a>
            </li>
        </ul>
    </li>

    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Printed t-shirts</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item">
                <a class="nav-link" href="{{url('service_operator/printed_tshirts/show_all')}}">Show all</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('service_operator/printed_tshirts/add_new')}}">add new</a>
            </li>
        </ul>
    </li>





    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("logout")}}">
            <i class="fa fa-power-off"></i>
            <span class="title">Logout</span>
        </a>
    </li>

</ul>
