@extends('user.main_layout')
@section('subview')

    <div class="container container_height">
        <div class="row">

            <div class="col-12">

                <div class="card card_margin">
                    <div class="card-header bg-primary text-white">
                        Orders List
                    </div>
                    <div class="card-body text-black">

                        <?php if($user_bills->count()): ?>
                            <?php foreach($user_bills as $key => $bill): ?>
                                <?php
                                    if(!isset($bill_orders[$bill->bill_id]))continue;

                                    $this_bill_orders=$bill_orders[$bill->bill_id];
                                ?>
                                <div class="row card_margin" >
                                    <div class="col-12">
                                        <a class="btn btn-primary btn-block" role="button" data-toggle="collapse" href="#collapse_{{$key}}" aria-expanded="false" aria-controls="collapseExample">
                                            <span style="float: left;">Bill Num.{{$bill->bill_id}}</span>
                                            <span>At {{date("Y-m-d",strtotime($bill->bill_created_at))}}</span>
                                            <span>( {{$bill->bill_amount}} EGP )</span>
                                            <span style="float: right;">Bill status {{$bill->bill_status}}</span>
                                        </a>
                                        <div class="collapse" id="collapse_{{$key}}">

                                            <div class="alert alert-info" style="margin-top: 10px;">
                                                <p>Shipping address</p>
                                                <p>Governorate: <strong>{{$bill->add_governorate}}</strong></p>
                                                <p>City: <strong>{{$bill->add_city}}</strong></p>
                                                <p>Street: <strong>{{$bill->add_street}}</strong></p>
                                                <p>Address type: <strong>{{$bill->add_type}}</strong></p>
                                                <p>tel number: <strong>{{$bill->add_tel_number}}</strong></p>
                                                <p>Address notes: <strong>{{$bill->add_notes}}</strong></p>
                                            </div>

                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <td>#</td>
                                                        <td>Product</td>
                                                        <td>Color</td>
                                                        <td>Size</td>
                                                        <td>Quantity</td>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                <?php foreach($this_bill_orders as $key2 => $order): ?>
                                                    <?php
                                                        if(!isset($user_products[$order->pro_id]))continue;

                                                        $pro_obj = $user_products[$order->pro_id]->first();
                                                    ?>
                                                    <tr>
                                                        <td>{{($key2+1)}}</td>
                                                        <td>
                                                            <a href="{{url("/show_product?pro_id=$pro_obj->pro_id")}}">
                                                                {{$pro_obj->pro_name}}
                                                            </a>
                                                        </td>
                                                        <td>{{$order->pro_color}}</td>
                                                        <td>{{$order->order_selected_size}}</td>
                                                        <td>{{$order->order_quantity}}</td>
                                                    </tr>
                                                <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                        <?php else: ?>

                            <div class="alert alert-warning">
                                There is no orders yet
                            </div>

                        <?php endif; ?>


                    </div>
                </div>

            </div>

        </div>
    </div>

@endsection