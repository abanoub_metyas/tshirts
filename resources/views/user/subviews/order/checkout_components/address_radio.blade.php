<div class="col-12" style="display: inline-block;">


    <input
        id="selected_user_address_{{$address->address_id}}"
        type="radio"
        name="selected_user_address"
        value="{{$address->address_id}}"
        {{($key==0)?"checked":""}}
        class="selected_user_address_class select_add_outside_country_list_{{$currency_symbol}}"
        data-country="{{$address->add_country}}"
        data-city="{{$address->add_governorate}}"
    >

    <label for="selected_user_address_{{$address->address_id}}" class="selected_user_address_label_class">
        {{$address->add_country}} <br>
        {{$address->add_governorate}} <br>
        {{$address->add_city}}, {{$address->add_street}} <br>
        {{$address->add_type}} <br>
        {{$address->add_tel_number}} <br>
        {{$address->add_post_code}} <br>
        {{$address->add_notes}}
    </label>

</div>

<hr>
