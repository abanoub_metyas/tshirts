<div class="card" style="margin-top: 20px;">
    <div class="card-header bg-primary text-white">
        Orders
    </div>
    <div class="card-body text-black checkout_page_max_200">
        <?php
            $items_cost=0;
        ?>

        <div class="col-12 padd-left padd-right">
            <div class="page-cart">

                <?php if(count($menu_cart_items)>count($cart_items)): ?>
                    <div class="alert alert-info">
                        <strong>
                            You can not make a bill with products from two diffrenet youtubers
                            so you will make this bill then go to checkout page again
                        </strong>
                    </div>
                <?php endif; ?>

                <?php foreach($cart_items as $key=>$cart_item): ?>
                    <?php
                        if(!isset($pro_objs[$cart_item["pro_id"]]))continue;
                        if(!isset($quantity_objs[$cart_item["quantity_id"]]))continue;

                        $pro_obj=$pro_objs[$cart_item["pro_id"]]->first();
                        $pro_quantity_obj=$quantity_objs[$cart_item["quantity_id"]]->first();


                        $pro_orginal_price=($pro_quantity_obj->pro_price - $pro_quantity_obj->pro_discount_amount);
                        $pro_orginal_price=$currency_symbol=="USD"?($pro_orginal_price*$usd_rate):$pro_orginal_price;

                        $items_cost=$items_cost+($pro_orginal_price*$cart_item["pro_quantity"]);
                    ?>

                    <div class="col-12 padd-left padd-right product_cart_item" data-proid="{{$pro_obj->pro_id}}">

                        <div class="col-4 pull-left padd-right">
                            <div class="col-md-12 cart_item_property">
                                <input type="hidden" name="pro_id[]" value="{{$pro_obj->pro_id}}">
                                <input type="hidden" name="pro_quantity_ids[]" value="{{$cart_item["quantity_id"]}}">
                                <img style="max-width: 100%;" src="{{get_image_or_default($pro_obj->small_img_path)}}" width="150" alt="{{$pro_obj->small_img_alt}}" title="{{$pro_obj->small_img_title}}">
                            </div>
                        </div>
                        <div class="col-8 pull-left padd-right">

                            <div class="col-md-12 cart_item_property">
                                <strong>Name</strong> : {{$pro_obj->pro_name}}
                            </div>
                            <div class="col-md-12 cart_item_property">
                                <strong>Price</strong> :
                                {{$pro_orginal_price}} {{$currency_symbol}}
                            </div>
                            <div class="col-md-12 cart_item_property">
                                <strong>Quantity</strong> :
                                <input
                                    placeholder="1"
                                    min="1"
                                    data-proid="{{$pro_obj->pro_id}}"
                                    data-selected_size="{{$cart_item["selected_product_size"]}}"
                                    data-quantity_id="{{$cart_item["quantity_id"]}}"
                                    data-proprice="{{$pro_orginal_price}}"
                                    value="{{$cart_item["pro_quantity"]}}"
                                    name="order_quantity[]"
                                    class="form-control change_row_quantity"
                                    type="number"
                                >
                                <div class="show_msg">
                                </div>
                            </div>
                            <div class="col-md-12 cart_item_property">
                                <strong>Color</strong> :
                                {{$pro_quantity_obj->pro_color}}
                            </div>
                            <div class="col-md-12 cart_item_property">
                                <strong>Size</strong> :
                                {{$cart_item["selected_product_size"]}}
                            </div>
                            <div class="col-md-12 cart_item_property">
                                <strong>Total</strong> :
                                <span class="row_sum">
                                    {{$pro_orginal_price*$cart_item["pro_quantity"]}}
                                </span>
                                {{$currency_symbol}}
                            </div>

                            <div class="col-md-12 cart_item_property">
                                <a
                                    href="#"
                                    class="btn btn-danger remove_product_form_cart"
                                    data-proid="{{$pro_obj->pro_id}}"
                                    data-selected_size="{{$cart_item["selected_product_size"]}}"
                                    data-quantity_id="{{$cart_item["quantity_id"]}}"
                                >

                                    <i class="fa fa-times"></i>
                                </a>
                            </div>

                        </div>
                    </div>

                <?php endforeach;?>

            </div>
        </div>
    </div>
</div>



<div class="card" style="margin-top: 20px;">
    <div class="card-header bg-primary text-white">Bill Cost</div>
    <div class="card-body text-black">
        <div class="form-group">
            <label>
                If you have valid coupon, Enter here
            </label>

            <input placeholder="Coupon code" class="form-control check_coupon_text" name="check_coupon_text" type="text">
        </div>
        <p></p>
        <button type="button" class="btn btn-info text-white check_coupon_btn">Check Coupon</button>

        <input type="hidden" class="coupon_type" value="">
        <input type="hidden" class="coupon_value" value="">

        <div class="col-12 padd-left padd-right" style="margin-top: 10px;">
            <div class="show_coupons_msg"></div>
        </div>

        <hr>

        <h4>
            <strong>
                Bill Cost:
            </strong>
            <span class="checkout_total">
                {{$items_cost}}
            </span>
            {{$currency_symbol}} + Delivery Cost

        </h4>
    </div>
</div>