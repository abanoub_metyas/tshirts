<div class="card" style="margin-top: 20px;" >
    <div class="card-header bg-primary text-white" data-toggle="collapse" data-target=".select_address" style="cursor: pointer;">
        Delivery Info
    </div>
    <div class="card-body text-black collapse show select_address checkout_page_max_200">

        <input type="hidden" class="do_not_run_select_add_governorate_code">


        <?php if($all_user_addresses->count()==0): ?>
            <div class="alert alert-info">
                <p style="margin: 0px;">
                    You should add new address from above form so you can processed checkout
                </p>
            </div>
        <?php endif; ?>

        <?php foreach ($all_user_addresses as $key => $address): ?>
            @include("user.subviews.order.checkout_components.address_radio")
        <?php endforeach; ?>

    </div>
</div>