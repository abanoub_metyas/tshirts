<div class="card" style="margin-top: 20px;">
    <div class="card-header bg-primary text-white" data-toggle="collapse" data-target=".add_new_address" style="cursor: pointer;">
        Add new address
    </div>
    <div class="card-body text-black collapse {{($all_user_addresses->count()==0?"show":"")}} add_new_address ">


        <form action="" method="post" id="add_new_address_form">

            {!! csrf_field() !!}

            @include("front.subviews.order.checkout_components.add_address_inner_form")

            <div class="col-12 pull-left">
                <div class="ajax_msgs"></div>
            </div>

            <div class="col-12 pull-left">
                <button class="btn btn-primary submit_form" type="submit">Add new address</button>
            </div>

        </form>


    </div>
</div>