<div class="card" style="margin-top: 20px;">
    <div class="card-header bg-primary text-white">
        {{show_content($checkout_page,"finish_order")}}
    </div>
    <div class="card-body text-black">
        {!! csrf_field() !!}

        <h2>Shipping Company</h2>

        <input
            id="selected_shipping_company_aramex"
            class="selected_shipping_input"
            type="radio"
            name="selected_shipping_company"
            value="aramex"
            checked
        >
        <label for="selected_shipping_company_aramex" class="selected_shipping_label" >
            <i class="fa fa-car"></i>
            <span>Aramex - </span>
            <span class="aramex_value item_price">
                <span class="price_value"> ** </span>
                <span class="price_symbol"> {{$currency_symbol}} </span>
            </span>
        </label>

        <?php if($egypt_or_not=="egypt"): ?>
        <input
                id="selected_shipping_company_fetchr"
                class="selected_shipping_input"
                type="radio"
                name="selected_shipping_company"
                value="fetchr"
        >
        <label for="selected_shipping_company_fetchr" class="selected_shipping_label" >
            <i class="fa fa-car"></i>
            <span>Fetchr - </span>
            <span class="fetchr_value item_price">
                <span class="price_value"> ** </span>
                <span class="price_symbol"> {{$currency_symbol}} </span>
            </span>
        </label>
        <?php endif; ?>



        <hr>

        <h2>Payment Method</h2>

        <?php if($currency_symbol=="EGP"): ?>
        <input id="selected_payment_method_deliver" class="payment_method_input" type="radio" name="selected_payment_method" value="deliver">
        <label for="selected_payment_method_deliver" class="payment_method_label" >
            <i class="fa fa-money-bill-alt"></i>
            Pay on deliver
        </label>
        <?php endif; ?>

        <input id="selected_payment_method_payment_online" class="payment_method_input" type="radio" name="selected_payment_method" value="payment_online">
        <label for="selected_payment_method_payment_online" class="payment_method_label" >
            <i class="fa fa-credit-card"></i>
            Payment Online
        </label>

        <?php if($currency_symbol=="EGP"): ?>
        <input id="selected_payment_method_masary" class="payment_method_input" type="radio" name="selected_payment_method" value="masary_aman">
        <label for="selected_payment_method_masary" class="payment_method_label" >
            <i class="fa fa-credit-card"></i>
            Masary - Aman
        </label>
        <?php endif; ?>


        <button type="submit" class="btn pay_now_btn">
            Checkout
        </button>

    </div>
</div>