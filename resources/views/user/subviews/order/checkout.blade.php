@extends('user.main_layout')

@section('subview')

    <script src="{{url("/")}}/public/jscode/order.js"></script>


    <div class="container container_height checkout_page" style="margin-top: 10px;margin-bottom: 10px;">
        <div class="row">
            <div class="col-12">
                @include("user.subviews.order.checkout_components.add_new_address")
            </div>

            <div class="col-12">
                <form action="{{url("/user/checkout")}}" method="post">
                    <div class="row">
                        <div class="col-6">
                            @include("user.subviews.order.checkout_components.select_address")
                            @include("user.subviews.order.checkout_components.finish_order")
                        </div>

                        <div class="col-6">
                            @include("user.subviews.order.checkout_components.list_orders")

                            @include("user.subviews.order.checkout_components.total_amount")
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection

