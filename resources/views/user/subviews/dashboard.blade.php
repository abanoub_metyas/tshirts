@extends('user.main_layout')
@section('subview')

    <div class="container container_height">
        <div class="row">

            <div class="col-12" style="margin-top: 20px;">
                <div class="card">
                    <div class="card-header bg-primary text-white">
                        Welcome {{$current_user->full_name}} !
                    </div>
                    <div class="card-body text-black">

                        <form action="{{url("user/edit_data")}}" method="POST">
                            {{csrf_field()}}

                            <div class="row">
                                <div class="col">
                                    <label for="full_name">Name</label>
                                    <input class="form-control" placeholder="Name" id="full_name" name="full_name" value="{{$current_user->full_name}}" type="text" required>
                                </div>

                                <div class="col">
                                    <label for="email">Email</label>
                                    <input class="form-control" placeholder="Email" id="email" name="email" value="{{$current_user->email}}" type="email" required>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div class="col">
                                    <input type="submit" class="waves-effect waves-light btn color-1" value="Save">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>


            <div class="col-12" style="margin-top: 20px;">
                <div class="card">
                    <div class="card-header bg-info text-white">
                        Change password
                    </div>
                    <div class="card-body text-black">

                        <form action="{{url("user/change_password")}}" method="POST">
                            {{csrf_field()}}

                            <div class="row">
                                <div class="col">
                                    <label for="old_password">Old Password</label>
                                    <input class="form-control" placeholder="Old Password" id="old_password" name="old_password" type="password" required>
                                </div>
                                <div class="col">
                                    <label for="new_password">New Password</label>
                                    <input class="form-control" placeholder="New Password" id="new_password" name="password" type="password" required>
                                </div>
                                <div class="col">
                                    <label for="password_confirmation">Confirm New Password</label>
                                    <input class="form-control" placeholder="Confirm New Password" id="password_confirmation" name="password_confirmation" type="password" required>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div class="col">
                                    <input type="submit" class="waves-effect waves-light btn color-1" value="Change Password">
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
