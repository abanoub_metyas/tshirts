<div class="account-right">
    <h1>{{show_content($account_page,"sidebar_myaccount")}}</h1>
    <p><a href="{{url("$lang_url_segment"."account")}}">{{show_content($account_page,"sidebar_dashboard")}}</a></p>
    <p><a href="{{url("$lang_url_segment"."account/edit_data")}}">{{show_content($account_page,"sidebar_account")}}</a></p>
    <p><a href="{{url("$lang_url_segment"."account/orders")}}">{{show_content($account_page,"sidebar_orders")}}</a></p>
    <p><a href="{{url("$lang_url_segment"."account/edit_subscribe")}}">{{show_content($account_page,"sidebar_subscribe")}}</a></p>
    <p><a href="{{url("/logout")}}">{{show_content($edit_index_header_page,"logout")}}</a></p>
</div>