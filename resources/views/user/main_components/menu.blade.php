<div class="container-fluid">
    <nav class="navbar navbar-expand-sm   navbar-light navexpand">
        <a class="navbar-brand" href="{{url("/")}}">
            <img
                    src="{{show_content($homepage_content,"site_logo",true)}}"
                    class="img-fluid"
                    {{get_image_alt_title(is_object($homepage_content)?$homepage_content->site_logo:"")}}
            >
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                <li class="nav-item">
                    <a class="nav-link" href="{{url("/")}}">
                        Site
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{url("/user/dashboard")}}">
                        Dashboard
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{url("/user/orders")}}">
                        Orders
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{url("/user/checkout")}}">
                        Checkout
                    </a>
                </li>




            </ul>
            <div class="social-part">
                <ul class="list-unstyled">
                    <li class="">
                        <a class="" href="{{url("/logout")}}">
                            Logout
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>