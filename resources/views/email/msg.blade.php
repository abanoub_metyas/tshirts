@extends("email.main_layout")

@section("subview")

    <tr style="background: #FFF;min-height: 15px;padding-left: 10px;padding-right: 10px;">
        <td style="padding-left: 20px;padding-right: 20px;text-align: left;">

            <h1 style="color: #000;margin: 0px;margin-top: 7px;line-height: 50px;">
                {{$header}}
            </h1>

        </td>
    </tr>


    <tr style="background-color: #ffffff;padding-left: 10px;padding-right: 10px;">
        <td style="text-align: left;padding-left: 20px;padding-right: 20px;">

            <table border="0" cellpadding="0" style="width: 100%;border-spacing: 0px;margin-top: 20px;margin-bottom: 20px;">
                <tr>
                    <td> {!! $body !!} </td>
                </tr>
            </table>

        </td>
    </tr>

@endsection