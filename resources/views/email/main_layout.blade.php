<div marginwidth="0" marginheight="0">
    <div style="background:#eeeddd repeat" dir="">
        <center style="padding:20px 0px 50px 0px">
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                <tbody>
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="800" style="width:800px;max-width:96%">
                            <tbody>
                            <tr>
                                <td align="center" valign="top">

                                    <div style="background:#FFF;margin:0px 0px 20px 0px;border-top:0px solid #ffffff!important;border-bottom:0px solid #ffffff!important;border-left:0px solid #ffffff!important;border-right:0px solid #ffffff!important;border-radius:0px!important">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <div style="text-align:center;margin:15px 0px 15px 0px">
                                                        <p style="margin:0px 0 0px 0">
                                                            <img
                                                                    src="{{show_content($homepage_content,"site_logo",true)}}"
                                                                    class="img-fluid"
                                                                    {{get_image_alt_title(is_object($homepage_content)?$homepage_content->site_logo:"")}}
                                                                    style="max-width:800px"
                                                            >
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>


                                    <div style="background:#ffffff;font-family:Arial;font-weight:bold;vertical-align:middle;margin:0px 0px 0px 0px;border:none!important">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            @yield("subview")
                                            </tbody>
                                        </table>
                                    </div>


                                    <div style="background:#ffffff;margin:0px 0px 0px 0px;border:none!important">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td valign="middle" align="center">
                                                    <div style="text-align:center;margin:24px 24px 24px 24px">
                                                        <div style="font:normal 11px Arial,sans-serif!important;color:#000000!important;text-align:center">
                                                            <p style="text-align:center;margin-top:0;padding:0">
                                                                <span>

                                                                </span>

                                                            </p>
                                                            <p style="margin:0 0 10px;padding:0">©
                                                                Copyright {{date("Y")}} teearab</p>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>

                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                    </td>
                </tr>
                </tbody>
            </table>
        </center>
        <div class="yj6qo"></div>
        <div class="adL">
        </div>
    </div>
    <div class="adL">
    </div>
</div>