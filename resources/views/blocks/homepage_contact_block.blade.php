<input type="hidden" id="msg_type" value="support">
<div class="col-md-12 text-center">
    <div class="tit-adv">
        <h3>
            {{show_content($homepage_contact_block,"header")}}
        </h3>
    </div>
</div>
<div class="col-md-3 padd-5">
    <div class="item-build">
        <input type="text" class="form-control" id="name"
               placeholder="{{show_content($homepage_contact_block,"input_name")}}">
    </div>
</div>

<?php if(isset($homepage_contact_block->countries)): ?>
<?php
$countries_list = array_diff($homepage_contact_block->countries, [""]);
?>
<?php if(count($countries_list)): ?>
<div class="col-md-2 padd-5">
    <div class="item-build">
        <select id="nationality" class="form-control">
            <?php foreach($countries_list as $key => $country): ?>
            <option value="{{$country}}">{{$country}}</option>
            <?php endforeach; ?>
        </select>
    </div>
</div>
<?php endif; ?>
<?php endif; ?>

<div class="col-md-3 padd-5">
    <div class="item-build">
        <input type="text" class="form-control" id="phone"
               placeholder="{{show_content($homepage_contact_block,"input_telephone")}}">
    </div>
</div>

<div class="col-md-3 padd-5">
    <div class="item-build">
        <input type="text" class="form-control" id="email"
               placeholder="{{show_content($homepage_contact_block,"input_email")}}">
    </div>
</div>
<div class="col-md-1 padd-5">
    <button class="btn-adv contact_us_btn">{{show_content($homepage_contact_block,"submit_btn")}}</button>
</div>
<div class="col-md-12">
    <div class="display_msgs center"></div>
</div>

