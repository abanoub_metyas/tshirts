<div id="policy_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{show_content($userpanel_register_user_page,"policy_header")}}</h4>
            </div>
            <div class="modal-body">
                {!! show_content($userpanel_register_user_page,"policy_body") !!}
            </div>
        </div>

    </div>
</div>


<h1 class="my_h1">{{show_content($userpanel_register_user_page,"register_header")}}</h1>

<div class="register_parent_div">

    <?php if(isset($sponsor_user)&&is_object($sponsor_user)): ?>
    <div class="alert alert-info">
        <p>{{show_content($userpanel_register_user_page,"sponsor")}}</p>
        <p>{{show_content($userpanel_register_user_page,"email")}} : {{$sponsor_user->email}}</p>
        <p>{{show_content($userpanel_register_user_page,"username")}} : {{$sponsor_user->username}}</p>
    </div>
    <?php endif; ?>

    <input type="hidden" class="parent_user_code" value="{{$parent_user_code}}">

    <div class="col-md-12 padd-left padd-right">
        <div class="form-group">
            <label for="">{{show_content($userpanel_register_user_page,"email")}}</label>
            <input type="email" class="form-control email_input check_valid" required>
        </div>
    </div>

    <div class="col-md-12 padd-left padd-right">
        <div class="form-group">
            <label for="">{{show_content($userpanel_register_user_page,"username")}}</label>
            <input type="text" class="form-control username_input check_valid" required>
        </div>
    </div>


    <div class="col-md-12 padd-left padd-right">
        <div class="form-group">
            <label for="">{{show_content($userpanel_register_user_page,"full_name")}}</label>
            <input type="text" class="form-control full_name_input check_valid" required>
        </div>
    </div>

    <div class="col-md-12 padd-left padd-right">
        <div class="form-group">
            <label for="">{{show_content($userpanel_register_user_page,"phone")}}</label>
            <input type="text" class="form-control phone_input check_valid" required>
        </div>
    </div>

    <div class="col-md-12 padd-left padd-right">
        <div class="form-group">
            <label for="">{{show_content($userpanel_register_user_page,"address")}}</label>
            <input type="text" class="form-control address_input check_valid" required>
        </div>
    </div>

    <div class="col-md-12 padd-left padd-right">
        <?php
        echo generate_select_tags(
            $field_name="country",
            $label_name=show_content($userpanel_register_user_page,"country"),
            $text=list_countries(),
            $values=list_countries(),
            $selected_value=[""],
            $class="form-control country_input",
            $multiple="",
            $required="",
            $disabled = "",
            $data = "",
            $grid = "col-md-12 padd-left padd-right padd-left padd-right",
            $hide_label=false,
            $remove_multiple = false
        );
        ?>
    </div>

    <div class="col-md-12 padd-left padd-right">
        <div class="form-group">
            <label for="">{{show_content($userpanel_register_user_page,"city")}}</label>
            <input type="text" class="form-control city_input check_valid" required>
        </div>
    </div>


    <div class="col-md-12 padd-left padd-right">
        <div class="form-group">
            <label for="">{{show_content($userpanel_register_user_page,"password")}}</label>
            <input type="password" class="form-control password_input check_valid" required>
        </div>
    </div>
    <div class="col-md-12 padd-left padd-right">
        <div class="form-group">
            <label for="">{{show_content($userpanel_register_user_page,"repeat_password")}}</label>
            <input type="password" class="form-control repeat_password_input check_valid" required>
        </div>
    </div>


    <div class="col-md-12 padd-left padd-right ">
        <input type="checkbox" class="check_valid" required>
        <a href="#" data-toggle="modal" data-target="#policy_modal">
            {{show_content($userpanel_register_user_page,"accept_policy")}}
        </a>
    </div>

    <div class="col-md-12 padd-left padd-right">
        <div class="request_msgs"></div>
        <button class="btn btn-site-primary register_btn">{{show_content($userpanel_register_user_page,"register")}}</button>
    </div>

</div>
