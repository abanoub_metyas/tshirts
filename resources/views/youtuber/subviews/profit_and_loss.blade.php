@extends('youtuber.main_layout')
@section('subview')


    <div class="container container_height">
        <div class="row">

            <div class="col-12" style="margin-top: 20px;">
                <div class="card">
                    <div class="card-header bg-primary text-white">
                        Profit&loss {{$current_user->full_name}} !
                    </div>
                    <div class="card-body text-black">
                        <div class="row">


                            <div class="col-12" style="margin-bottom: 10px;">
                                <div class="card">
                                    <div class="card-header bg-info text-white">
                                        Filter
                                    </div>
                                    <div class="card-body text-black">

                                        <form action="" method="GET">

                                            <input type="hidden" name="youtuber_id" value="{{$youtuber_obj->user_id}}">

                                            <?php
                                                echo generate_select_years(
                                                    $already_selected_value="" ,
                                                    $earliest_year="2018",
                                                    $class="form-control",
                                                    $name="filter_year",
                                                    $label="Select year",
                                                    $data=$post_data,
                                                    $grid="12"
                                                );
                                            ?>

                                            <div class="col-md-12 text-center" style="margin-top: 10px;">
                                                <button type="submit" class="btn btn-primary">Filter</button>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div class="col-12" style="margin-bottom: 10px;">
                                <div class="card">
                                    <div class="card-header bg-info text-white">
                                        Profit&loss
                                    </div>
                                    <div class="card-body text-black">
                                        @include("admin.subviews.users.profit_and_loss_inner_view")
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

@endsection
