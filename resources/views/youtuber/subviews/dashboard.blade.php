@extends('youtuber.main_layout')
@section('subview')
    <script src="{{url("/public/ckeditor/ckeditor.js")}}" type="text/javascript"></script>
    <script src="{{url("/public/ckeditor/adapters/jquery.js")}}" type="text/javascript"></script>

    <div class="container container_height">
        <div class="row">

            <div class="col-12" style="margin-top: 20px;">
                <div class="card">
                    <div class="card-header bg-primary text-white">
                        Welcome {{$current_user->full_name}} !
                    </div>
                    <div class="card-body text-black">
                        <div class="row">

                            <div class="col-12" style="margin-bottom: 10px;">
                                <div class="card">
                                    <div class="card-header bg-info text-white">
                                        Your Profile Link
                                    </div>
                                    <div class="card-body text-black">

                                        <input
                                            type="text"
                                            readonly
                                            value="{{url("/stores/$current_user->full_name")}}"
                                            class="form-control copy_link"
                                        >

                                        <button type="button" class="btn btn-primary click_to_copy">
                                            Copy
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4 pull-left">
                                <div class="card">
                                    <div class="card-header bg-info text-white">
                                        Total Views
                                    </div>
                                    <div class="card-body text-black text-center statistics_font">
                                        {{$yotuber_views_count}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-4 pull-left">
                                <div class="card">
                                    <div class="card-header bg-info text-white">
                                        Total Boughts
                                    </div>
                                    <div class="card-body text-black text-center statistics_font">
                                        {{$total_boughts}} EGP
                                    </div>
                                </div>
                            </div>

                            <div class="col-4 pull-left">

                                <div class="card">
                                    <div class="card-header bg-info text-white">
                                        Total Reviews
                                    </div>
                                    <div class="card-body text-black text-center statistics_font">
                                        {{$total_reviews_count}}
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12" style="margin-top: 20px;">
                <div class="card">
                    <div class="card-header bg-info text-white">
                        Edit Data
                    </div>
                    <div class="card-body text-black">

                        <form action="{{url("user/edit_data")}}" method="POST">
                            {{csrf_field()}}

                            <div class="row">

                                <?php
                                    $normal_tags=array("full_name","email","user_desc");

                                    $attrs = generate_default_array_inputs_html(
                                        $normal_tags,
                                        $current_user,
                                        "yes",
                                        "required",
                                        " col-6 pull-left"
                                    );

                                    $attrs[0]["email"]="Email";

                                    $attrs[0]["user_desc"]="Description";
                                    $attrs[3]["user_desc"]="textarea";
                                    $attrs[5]["user_desc"].=" ckeditor";
                                    $attrs[6]["user_desc"]="12";

                                    echo generate_inputs_html_take_attrs($attrs);
                                ?>

                                <hr>

                                <div class="col-12">
                                    <?php
                                        $img_obj = isset($current_user->user_img_file) ? $current_user->user_img_file : "";

                                        echo generate_img_tags_for_form(
                                            $filed_name="user_img_file",
                                            $filed_label="user_img_file",
                                            $required_field="",
                                            $checkbox_field_name="user_img_checkbox",
                                            $need_alt_title="no",
                                            $required_alt_title="",
                                            $old_path_value="",
                                            $old_title_value="",
                                            $old_alt_value="",
                                            $recomended_size="",
                                            $disalbed="",
                                            $displayed_img_width="100",
                                            $display_label="Upload User Image",
                                            $img_obj
                                        );


                                        echo generate_img_tags_for_form(
                                            $filed_name="cover_img_file",
                                            $filed_label="cover_img_file",
                                            $required_field="",
                                            $checkbox_field_name="cover_img_checkbox",
                                            $need_alt_title="no",
                                            $required_alt_title="",
                                            $old_path_value="",
                                            $old_title_value="",
                                            $old_alt_value="",
                                            $recomended_size="",
                                            $disalbed="",
                                            $displayed_img_width="100",
                                            $display_label="Upload User Cover",
                                            isset($current_user->cover_img_file) ? $current_user->cover_img_file : ""
                                        );
                                    ?>

                                </div>


                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div class="col">
                                    <input type="submit" class="waves-effect waves-light btn color-1" value="Save">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>


            <div class="col-12" style="margin-top: 20px;">
                <div class="card">
                    <div class="card-header bg-info text-white">
                        Change password
                    </div>
                    <div class="card-body text-black">

                        <form action="{{url("user/change_password")}}" method="POST">
                            {{csrf_field()}}

                            <div class="row">
                                <div class="col">
                                    <label for="old_password">Old Password</label>
                                    <input class="form-control" placeholder="Old Password" id="old_password" name="old_password" type="password" required>
                                </div>
                                <div class="col">
                                    <label for="new_password">New Password</label>
                                    <input class="form-control" placeholder="New Password" id="new_password" name="password" type="password" required>
                                </div>
                                <div class="col">
                                    <label for="password_confirmation">Confirm New Password</label>
                                    <input class="form-control" placeholder="Confirm New Password" id="password_confirmation" name="password_confirmation" type="password" required>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div class="col">
                                    <input type="submit" class="waves-effect waves-light btn color-1" value="Change Password">
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
