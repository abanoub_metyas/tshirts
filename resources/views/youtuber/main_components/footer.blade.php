
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <ul class="list-unstyled footerlist">
                    <li>
                        <a href="">
                            About
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Blog
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Jobs
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Terms
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Privacy
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Security
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Track order
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Training Center
                        </a>
                    </li>
                    <!--=======================================-->
                    <li>
                        <a href="" class="btn btn-danger">
                            Help
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-2">
                <div class="text-center">
                    <select class="lang_select">
                        <option value="de">Deutsch</option>
                        <option value="" selected="">English (USA)</option>
                        <option value="en-GB">English (UK)</option>
                        <option value="es">Español</option>
                        <option value="fr">Français</option>
                        <option value="it">Italiano</option>
                        <option value="nl">Nederlands</option>
                        <option value="pt-BR">Português do Brasil</option>
                    </select>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="copyright ">
                    © 2019 Teespring, Inc.
                </div>
            </div>
            <div class="col-md-2 text-center">
                <ul class="list-unstyled footersocial">
                    <li>
                        <a href="">
                            <i class="fab fa-facebook-f">

                            </i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="fab fa-pinterest"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

</body>
</html>