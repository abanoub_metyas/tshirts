<div class="top-menu">
    <ul class="nav navbar-nav pull-right">

        <li class="dropdown dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <img alt="" class="img-circle" src="{{get_image_or_default("$current_user->path")}}" style="width: 29px;height: 29px;" />
                <span class="username username-hide-on-mobile"> {{$current_user->full_name}} </span>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
                <li>
                    <a href="{{url("/logout")}}">
                        <i class="icon-key"></i>
                        Log Out
                    </a>
                </li>
            </ul>
        </li>

    </ul>
</div>