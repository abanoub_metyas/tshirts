<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">

    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li>

    <li class="sidebar-search-wrapper">

        <form class="sidebar-search" method="POST">
            <a href="javascript:;" class="remove">
                <i class="icon-close"></i>
            </a>
            <div class="input-group">
                <input type="text" class="form-control filter_menu" placeholder="Search...">
                <span class="input-group-btn">
                    <a href="javascript:;" class="btn">
                        <i class="icon-magnifier"></i>
                    </a>
                </span>
            </div>
        </form>
        <!-- END RESPONSIVE QUICK SEARCH FORM -->
    </li>

    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("dev/dashboard")}}">
            <i class="fa fa-power-off"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>

    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-users"></i>
            <span class="title">Edit Content Generator</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item">
                <a class="nav-link" href="{{url("/dev/generate_edit_content/show_all")}}">
                    <i class="fa fa-link"></i> Show All Generators
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{url('/dev/generate_edit_content/save')}}">
                    <i class="fa fa-link"></i>
                    Add New Generator
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-users"></i>
            <span class="title">Site Permissions</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item">
                <a class="nav-link" href="{{url("dev/permissions/permissions_pages/show_all_permissions_pages")}}">
                    <i class="fa fa-link"></i> Show All Permission pages
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{url('dev/permissions/permissions_pages/save')}}">
                    <i class="fa fa-link"></i>
                    Add New Permission Page
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{url('dev/permissions/assign_permission_for_this_user')}}">
                    <i class="fa fa-link"></i>
                    Edit Main User Permissions
                </a>
            </li>

        </ul>
    </li>



    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("logout")}}">
            <i class="fa fa-power-off"></i>
            <span class="title">Logout</span>
        </a>
    </li>

</ul>

