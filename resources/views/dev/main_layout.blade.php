@include("main_dashboard_components.header")

<div class="page-wrapper">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{url("/dev/dashboard")}}">
                    <img src="{{url("/public/img/logo.png")}}" alt="logo" class="logo-default"/>
                </a>
                <div class="menu-toggler sidebar-toggler">
                    <span></span>
                </div>
            </div>

            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                <span></span>
            </a>

            @include("dev.components.top_menu")
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="page-container">
        <div class="page-sidebar-wrapper">
            <div class="page-sidebar navbar-collapse collapse">
                @include("dev.components.menu")
            </div>
        </div>
        <div class="page-content-wrapper">
            <div class="page-content">
                @yield("subview")
            </div>
        </div>
    </div>


    <div class="page-footer">
        <div class="page-footer-inner">
            2016 &copy;
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>

</div>


</body>

</html>


