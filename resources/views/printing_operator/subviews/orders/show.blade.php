@extends('printing_operator.main_layout')

@section('subview')


    <div class="panel panel-info">
        <div class="panel-heading" data-toggle="collapse" style="cursor: pointer;"  data-target=".collapse">
            Filtration
        </div>
        <div class="panel-body collapse in">

            <form action="" method="GET">

                <?php
                    echo generate_select_tags(
                        $field_name="bill_status",
                        $label_name="Bill status",
                        $text=$order_status,
                        $values=$order_status,
                        $selected_value=[],
                        $class="form-control",
                        $multiple="",
                        $required="",
                        $disabled = "",
                        $data = $post_data,
                        $grid = "col-md-4",
                        $hide_label=false,
                        $remove_multiple = false
                    );

                    $normal_tags=array(
                        'start_date',
                        'end_date',
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $post_data,
                        "yes",
                        $required="",
                        "4"
                    );


                    $attrs[3]["start_date"]="date";
                    $attrs[3]["end_date"]="date";

                    echo generate_inputs_html_take_attrs($attrs);
                ?>

                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Filter</button>
                </div>

            </form>

        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            Orders - total cost ({{array_sum($all_bills->pluck("bill_amount")->all())}}) - EGP
        </div>
        <div class="panel-body" style="overflow-x: scroll;">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Youtuber</td>
                        <td>Bill cost</td>
                        <td>Bill status</td>
                        <td>Payment method</td>
                        <td>Date</td>
                        <td>Details</td>
                        <td>Send to shipping</td>
                    </tr>
                </thead>

                <tbody>
                <?php foreach ($all_bills as $key => $bill): ?>
                <tr id="row<?= $bill->bill_id ?>">
                    <td><?=$key+1?></td>

                    <td>
                        {{$bill->email}} <br>
                        {{$bill->username}}
                    </td>
                    <td>{{$bill->bill_amount}}</td>
                    <td>{{$bill->bill_status}}</td>
                    <td>{{$bill->payment_method}}</td>
                    <td>{{date("Y-m-d",strtotime($bill->created_at))}}</td>
                    <td>
                        <a class="btn btn-primary" href="{{url("/printing_operator/orders/bill_orders/$bill->bill_id")}}">
                            <i class="fa fa-link"></i>
                        </a>
                    </td>
                    <td>
                        <a class="btn btn-primary" href="#">
                            <i class="fa fa-link"></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach ?>
                </tbody>

            </table>
        </div>
    </div>

@endsection
