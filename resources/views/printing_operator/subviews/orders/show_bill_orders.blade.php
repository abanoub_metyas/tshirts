@extends('printing_operator.main_layout')

@section('subview')

    <style>
        @media print {

           .hide_at_print,
           .fa-edit
           {
                visibility: hidden;
           }
        }
    </style>

    <div class="print_container">

        <button class="btn btn-info print_page hide_at_print">Print
            <i class="fa fa-print"></i>
        </button>

        <div class="panel panel-primary">
            <div class="panel-heading">Details</div>
            <div class="panel-body">
                <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">

                    <tr>
                        <td>Order Number</td>
                        <td>#{{$bill_obj->bill_id}}</td>
                    </tr>

                    <tr>
                        <td><b>User Address:</b></td>
                        <td>
                            <span> Governorate: </span>
                            <strong>{{$address_obj->add_governorate}}</strong> <br>

                            <span> City: </span>
                            <strong>{{$address_obj->add_city}}</strong> <br>

                            <span> Street: </span>
                            <strong>{{$address_obj->add_street}}</strong> <br>

                            <span> Type: </span>
                            <strong>{{$address_obj->add_type}}</strong> <br>

                            <span> Mobile number :</span>
                            <strong>{{$address_obj->add_tel_number}}</strong> <br>

                            <span> Mobile number Verified :</span>
                            <strong>{{$address_obj->add_tel_number_verified?"yes":"no"}}</strong><br>

                            <span> Notes: </span>
                            <strong>{{$address_obj->add_notes}}</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">Products</div>
            <div class="panel-body">
                <table id="cat_table_1" class="table table-striped table-bordered" cellspacing="0" width="100%">

                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>Product</th>
                        <th>Material</th>
                        <th>Size</th>
                        <th>Color</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th class="hide_at_print">Material is available?</th>
                        <th class="hide_at_print">Already printed t-shirts</th>
                        <th class="hide_at_print">printing Status</th>
                    </tr>
                    </thead>

                    <tbody>
                        <?php foreach($bill_orders as $key=>$order): ?>
                            <tr id="row<?= $order->bill_order_id ?>"  class="order_item">
                                <td>
                                    <img src="{{get_image_or_default($order->small_img_path)}}" alt="" width="100">
                                </td>
                                <td>{{$order->pro_name}}</td>
                                <td>{{$order->pro_material_type}}</td>
                                <td>{{$order->order_selected_size}}</td>
                                <td>{{$order->pro_color}}</td>
                                <td class="order_price">{{$order->pro_price}}</td>
                                <td class="order_quantity">
                                    {{$order->order_quantity}}
                                </td>
                                <td class="hide_at_print">
                                    <?php
                                        $available_or_not="not available";

                                        if(isset($materials[$order->pro_material_type])){
                                            $colors=$materials[$order->pro_material_type]->groupBy("m_color");

                                            if(isset($colors[$order->pro_color])){
                                                $sizes=$colors[$order->pro_color]->groupBy("m_size");

                                                if(isset($sizes[$order->order_selected_size])){
                                                    $available_or_not="available";
                                                }
                                            }
                                        }
                                    ?>
                                    {{$available_or_not}}
                                </td>
                                <td class="hide_at_print">
                                    <?php

                                    if(isset($printed_tshirts[$order->pro_id])){
                                        $colors=$printed_tshirts[$order->pro_id]->groupBy("pt_color");


                                        if(isset($colors[$order->pro_color])){
                                            $sizes=$colors[$order->pro_color]->groupBy("pt_size");

                                            if(isset($sizes[$order->order_selected_size])){
                                                echo $sizes[$order->order_selected_size]->first()->pt_quantity;
                                            }
                                        }
                                    }
                                    ?>
                                </td>
                                <td class="hide_at_print">
                                    <?php if(isset($printing_requests[$order->bill_order_id])): ?>

                                        {{$printing_requests[$order->bill_order_id][0]->print_status}} <br>
                                        <a href="{{url("/printing_operator/printing_requests/change_status?request_id=".$printing_requests[$order->bill_order_id][0]->print_id)}}" class="btn btn-primary">
                                            Change status
                                        </a>

                                    <?php else: ?>

                                        <?php if($available_or_not=="available"): ?>
                                            Not sent yet
                                        <?php endif; ?>

                                    <?php endif; ?>

                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>

                </table>

                <h1>
                    Total Amount:
                    <span class="order_total_amount"></span>
                </h1>

            </div>
        </div>

    </div>

@endsection