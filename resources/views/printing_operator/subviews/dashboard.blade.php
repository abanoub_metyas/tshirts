@extends('printing_operator.main_layout')

@section("subview")

    <div class="panel panel-info">
        <div class="panel-heading">Welcome</div>
        <div class="panel-body">

            <p>
                Welcome to printing operator panel
            </p>

        </div>
    </div>


    <div class="panel panel-info">
        <div class="panel-heading">Materials (critical quantity)</div>
        <div class="panel-body">

            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Material</td>
                        <td>Size</td>
                        <td>Color</td>
                        <td>Quantity</td>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($critical_materials as $key=>$material): ?>
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$material->m_name}}</td>
                            <td>{{$material->m_size}}</td>
                            <td>{{$material->m_color}}</td>
                            <td>{{$material->m_quantity}}</td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>

        </div>
    </div>

@endsection