@extends('printing_operator.main_layout')

@section('subview')


    <div class="panel panel-info">
        <div class="panel-heading" data-toggle="collapse" style="cursor: pointer;"  data-target=".collapse">
            Filtration
        </div>
        <div class="panel-body collapse in">

            <form action="" method="GET">

                <?php
                    echo generate_select_tags(
                        $field_name="print_status",
                        $label_name="Print status",
                        $text=array_merge(["all"],$print_status),
                        $values=array_merge(["all"],$print_status),
                        $selected_value=[],
                        $class="form-control",
                        $multiple="",
                        $required="",
                        $disabled = "",
                        $data = $post_data,
                        $grid = "col-md-4",
                        $hide_label=false,
                        $remove_multiple = false
                    );

                    $normal_tags=array(
                        'start_date',
                        'end_date',
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $post_data,
                        "yes",
                        $required="",
                        "4"
                    );


                    $attrs[3]["start_date"]="date";
                    $attrs[3]["end_date"]="date";

                    echo generate_inputs_html_take_attrs($attrs);
                ?>

                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Filter</button>
                </div>

            </form>

        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            Print requests
        </div>
        <div class="panel-body" style="overflow-x: scroll;">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Type</td>
                        <td>Color</td>
                        <td>Size</td>
                        <td>Request Quantity</td>
                        <td>Status</td>
                        <td>Change status</td>
                    </tr>
                </thead>

                <tbody>
                <?php foreach ($all_requests as $key => $req): ?>
                <tr id="row<?= $req->print_id ?>">
                    <td><?=$key+1?></td>

                    <td>{{$req->m_name}}</td>
                    <td>{{$req->m_color}}</td>
                    <td>{{$req->m_size}}</td>
                    <td>{{$req->request_quantity}}</td>
                    <td>{{$req->print_status}}</td>
                    <td>
                        <a href="{{url("/printing_operator/printing_requests/change_status?request_id=".$req->print_id)}}" class="btn btn-primary">
                            Change status
                        </a>
                    </td>
                </tr>
                <?php endforeach ?>
                </tbody>

            </table>
        </div>
    </div>

@endsection
