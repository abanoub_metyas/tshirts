@extends('printing_operator.main_layout')

@section('subview')


    <div class="panel panel-info">
        <div class="panel-heading" data-toggle="collapse" style="cursor: pointer;"  data-target=".collapse">
            Change Status
        </div>
        <div class="panel-body collapse in">

            <form action="{{url("/printing_operator/printing_requests/change_status?request_id=".$printing_request_obj->print_id)}}" method="POST">

                {!! csrf_field() !!}

                <?php
                    echo generate_select_tags(
                        $field_name="print_status",
                        $label_name="Print status",
                        $text=$print_status,
                        $values=$print_status,
                        $selected_value=[],
                        $class="form-control",
                        $multiple="",
                        $required="",
                        $disabled = "",
                        $data = $printing_request_obj,
                        $grid = "col-md-12",
                        $hide_label=false,
                        $remove_multiple = false
                    );

                ?>

                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </form>

        </div>
    </div>


@endsection
