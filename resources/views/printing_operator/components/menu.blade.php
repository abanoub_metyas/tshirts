<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">

    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li>

    <li class="sidebar-search-wrapper">
        <form class="sidebar-search" method="POST">
            <a href="javascript:;" class="remove">
                <i class="icon-close"></i>
            </a>
            <div class="input-group">
                <input type="text" class="form-control filter_menu" placeholder="Search...">
                <span class="input-group-btn">
                    <a href="javascript:;" class="btn">
                        <i class="icon-magnifier"></i>
                    </a>
                </span>
            </div>
        </form>
    </li>

    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("/")}}">
            <i class="fa fa-home"></i>
            <span class="title">Site</span>
        </a>
    </li>

    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("printing_operator/dashboard")}}">
            <i class="fa fa-dashboard"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>

    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("printing_operator/printing_requests")}}">
            <i class="fa fa-dashboard"></i>
            <span class="title">Printing Requests</span>
        </a>
    </li>



    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Orders</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item">
                <a class="nav-link" href="{{url('printing_operator/orders/show_all')}}">Show all</a>
            </li>
        </ul>
    </li>


    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("logout")}}">
            <i class="fa fa-power-off"></i>
            <span class="title">Logout</span>
        </a>
    </li>

</ul>
