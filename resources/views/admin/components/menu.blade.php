<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">

    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li>

    <li class="sidebar-search-wrapper">

        <form class="sidebar-search" method="POST">
            <a href="javascript:;" class="remove">
                <i class="icon-close"></i>
            </a>
            <div class="input-group">
                <input type="text" class="form-control filter_menu" placeholder="Search...">
                <span class="input-group-btn">
                    <a href="javascript:;" class="btn">
                        <i class="icon-magnifier"></i>
                    </a>
                </span>
            </div>
        </form>
        <!-- END RESPONSIVE QUICK SEARCH FORM -->
    </li>

    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("/")}}">
            <i class="fa fa-home"></i>
            <span class="title">Site</span>
        </a>
    </li>

    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("admin/dashboard")}}">
            <i class="fa fa-dashboard"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>

    <?php if(check_permission($user_permissions,"admin/materials","show_action")): ?>
    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Materials</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/materials')}}">Show all</a></li>
            <?php if(check_permission($user_permissions,"admin/materials","add_action")): ?>
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/materials/save_material')}}">Add new</a></li>
            <?php endif; ?>

            <li class="nav-item"><a class="nav-link" href="{{url('/admin/materials/material_usage')}}">Material usage</a></li>

        </ul>
    </li>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/countries","show_action")): ?>
    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Countries</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/countries/show_all')}}">Show all</a></li>

            <?php if(check_permission($user_permissions,"admin/countries","add_action")): ?>
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/countries/save')}}">Add new</a></li>
            <?php endif; ?>
        </ul>
    </li>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/cities","show_action")): ?>
    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Cities</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/cities/show_all')}}">Show all</a></li>

            <?php if(check_permission($user_permissions,"admin/cities","add_action")): ?>
                <li class="nav-item"><a class="nav-link" href="{{url('/admin/cities/save')}}">Add new</a></li>
            <?php endif; ?>
        </ul>
    </li>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/category","show_action")): ?>
    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Product Categories</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/category')}}">Show all</a></li>
            <?php if(check_permission($user_permissions,"admin/category","add_action")): ?>
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/category/save_cat')}}">Add new</a></li>
            <?php endif; ?>
        </ul>
    </li>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/products","show_action")): ?>
    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Products</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/product')}}">Show all</a></li>
            <?php if(check_permission($user_permissions,"admin/products","add_action")): ?>
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/product/save_pro')}}">Add new</a></li>
            <?php endif; ?>
        </ul>
    </li>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/orders","show_action")): ?>
    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Orders</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item">
                <a class="nav-link" href="{{url('/service_operator/orders/show_all')}}">Show all</a>
            </li>
        </ul>
    </li>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/pages","show_action")): ?>
    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Pages</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/pages/show_all')}}">Show all</a></li>
            <?php if(check_permission($user_permissions,"admin/pages","add_action")): ?>
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/pages/save_page')}}">Add new</a></li>
            <?php endif; ?>
        </ul>
    </li>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/coupons","show_action")): ?>
    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Coupons</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/coupons')}}">Show all</a></li>
            <?php if(check_permission($user_permissions,"admin/coupons","edit_action")): ?>
            <li class="nav-item"><a class="nav-link" href="{{url('admin/coupons/save_coupon')}}">Add new</a></li>
            <?php endif; ?>
        </ul>
    </li>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/admins","show_action")): ?>
    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Youtubers</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/users/get_all_users?user_type=youtuber')}}">Show all</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/users/save?user_type=youtuber')}}">Add new</a></li>
        </ul>
    </li>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/admins","show_action")): ?>
    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Service operators</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/users/get_all_users?user_type=service_operator')}}">Show all</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/users/save?user_type=service_operator')}}">Add new</a></li>
        </ul>
    </li>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/admins","show_action")): ?>
    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Printing operators</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/users/get_all_users?user_type=printing_operator')}}">Show all</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/users/save?user_type=printing_operator')}}">Add new</a></li>
        </ul>
    </li>
    <?php endif; ?>

    <?php if(check_permission($user_permissions,"admin/admins","show_action")): ?>
    <li class="nav-item parent_nav_item">
        <a class="nav-link nav-toggle" href="javascript:;">
            <i class="fa fa-link"></i>
            <span>Admins</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/users/get_all_users?user_type=admin')}}">Show all</a></li>
            <li class="nav-item"><a class="nav-link" href="{{url('/admin/users/save?user_type=admin')}}">Add new</a></li>
        </ul>
    </li>
    <?php endif; ?>

    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("admin/show_methods")}}">
            <i class="fa fa-dashboard"></i>
            <span class="title">Edit Content</span>
        </a>
    </li>

    <li class="nav-item parent_nav_item">
        <a class="nav-link" href="{{url("logout")}}">
            <i class="fa fa-power-off"></i>
            <span class="title">Logout</span>
        </a>
    </li>

</ul>
