@extends("admin.main_layout")


@section("subview")

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <span>Dashboard</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title">Dashboard</h1>

        <form method="post" action="{{url("admin/set_filter")}}" class="filter_form">
            <div class="generate_radio_btns col-md-12 form-group">
                <p for="">Filter By user status</p>
                <div class="col-md-3">
                    <label class="form-control">
                        <input class="filter_users_status" <?= (Session::get("accept"))?'checked':'' ?> name="accept" type="checkbox" value="accept"> Accept</label>
                </div>
                <div class="col-md-3">
                    <label class="form-control">
                        <input class="filter_users_status" <?= (Session::get("pending"))?'checked':'' ?> name="pending" type="checkbox" value="pending"> Pending
                    </label>
                </div>
                <div class="col-md-3">
                    <label class="form-control">
                        <input class="filter_users_status" <?= (Session::get("reject"))?'checked':'' ?> name="reject" type="checkbox" value="reject"> Reject
                    </label>
                </div>
                <div class="col-md-3">
                    <label class="form-control">
                        <input class="filter_users_status" <?= (Session::get("forever_reject"))?'checked':'' ?> name="forever_reject" type="checkbox" value="forever_reject"> Forever Reject
                    </label>
                </div>
            </div>
            {{csrf_field()}}

        </form>
        <div class="row">
            <?php foreach ($regions as $region):?>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue" href="{{url("admin/dashboard_cuntries/$region->region_id")}}">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup">
                                {{isset($all_data_region[$region->region_id])?$all_data_region[$region->region_id]->count():"0"}}
                            </span>
                        </div>
                        <div class="desc"> {{$region->region_name}} </div>
                    </div>
                </a>
            </div>
            <?php endforeach;?>

        </div>



@endsection


