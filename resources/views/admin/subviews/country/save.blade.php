@extends("admin.main_layout")

@section("subview")

    <?php
    $header_text="Add New";
    $country_id="";

    if (is_object($data_object))
    {
        $header_text="<i class='fa fa-edit'></i> '".$data_object->country_name."'";
        $country_id=$data_object->country_id;
    }

    ?>
    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="{{url("/admin/dashboard")}}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <a href="{{url("/admin/countries/show_all")}}">Show All Countries</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span>{!!$header_text!!}</span>
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="fa fa-files-o font-green-sharp"></i>
                        <span class="caption-subject bold uppercase">{!!$header_text!!}</span>
                    </div>
                    <div class="actions">
                        <a href="<?= url("admin/countries/save") ?>" class="btn btn-circle btn-default btn-sm">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>


                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">

                            <form id="save_form" action="<?=url("admin/countries/save/$country_id")?>" method="POST" enctype="multipart/form-data">




                                <?php




                                $normal_tags=array('country_name','aramex_shipping');

                                $attrs = generate_default_array_inputs_html(
                                    $normal_tags,
                                    $data_object,
                                    "yes",
                                    "required"
                                );

                                $attrs[0]["country_name"]='Country Name';

                                $attrs[3]["aramex_shipping"]="number";

                                echo
                                generate_inputs_html(
                                    reformate_arr_without_keys($attrs[0]),
                                    reformate_arr_without_keys($attrs[1]),
                                    reformate_arr_without_keys($attrs[2]),
                                    reformate_arr_without_keys($attrs[3]),
                                    reformate_arr_without_keys($attrs[4]),
                                    reformate_arr_without_keys($attrs[5])
                                );
                                ?>


                                {{csrf_field()}}
                                <input id="submit" type="submit" value="Save" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg" style="margin-top: 10px;">

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



