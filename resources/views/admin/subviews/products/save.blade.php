@extends('admin.main_layout')
@section('subview')

    <style>
        hr{
            width: 100%;
            height:1px;
        }
    </style>
    <?php

        if (isset($success)&&!empty($success)) {
            echo $success;
        }

        $header_text="Add new product";
        $pro_id="";

        if ($pro_data!="") {
            $header_text="Edit ".$pro_data->pro_name;
            $pro_id=$pro_data->pro_id;
        }

    ?>


    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("admin/product/save_pro/$pro_id")?>" method="POST" enctype="multipart/form-data">

                    <div class="panel panel-info">
                        <div class="panel-heading">Category & Youtuber</div>
                        <div class="panel-body">
                            <?php
                                echo generate_depended_selects(
                                    $field_name_1="parent_id",
                                    $field_label_1="Select Parent Category",
                                    $field_text_1=convert_inside_obj_to_arr($all_parent_cats,"cat_name"),
                                    $field_values_1=convert_inside_obj_to_arr($all_parent_cats,"cat_id"),
                                    $field_selected_value_1=(is_object($pro_data)?$pro_data->parent_cat_id:""),
                                    $field_required_1="",
                                    $field_class_1="form-control",
                                    $field_name_2="cat_id",
                                    $field_label_2="Select Child Category",
                                    $field_text_2=convert_inside_obj_to_arr($all_child_cats,"cat_name"),
                                    $field_values_2=convert_inside_obj_to_arr($all_child_cats,"cat_id"),
                                    $field_selected_value_2=(is_object($pro_data)?$pro_data->child_cat_id:""),
                                    $field_2_depend_values=convert_inside_obj_to_arr($all_child_cats,"parent_id"),
                                    $field_required_2="",
                                    $field_class_2="form-control"
                                );


                                echo generate_select_tags(
                                    $field_name="youtuber_id",
                                    $label_name="Select youtuber",
                                    $text=$all_youtubers->pluck("full_name")->all(),
                                    $values=$all_youtubers->pluck("user_id")->all(),
                                    $selected_value=[""],
                                    $class="form-control select_2_class",
                                    $multiple="",
                                    $required="",
                                    $disabled = "",
                                    $data = $pro_data,
                                    $grid = "col-md-6",
                                    $hide_label=false,
                                    $remove_multiple = false
                                );

                                echo generate_select_tags(
                                    $field_name="pro_material_type",
                                    $label_name="Select material type",
                                    $text=$all_material_types,
                                    $values=$all_material_types,
                                    $selected_value=[""],
                                    $class="form-control",
                                    $multiple="",
                                    $required="",
                                    $disabled = "",
                                    $data = $pro_data,
                                    $grid = "col-md-6",
                                    $hide_label=false,
                                    $remove_multiple = false
                                );


                            ?>

                            <?php
                                $normal_tags=array(
                                    "youtuber_profit_per_tshirt","youtuber_lost_per_return_single_tshirt",
                                );

                                $attrs = generate_default_array_inputs_html(
                                    $normal_tags,
                                    $pro_data,
                                    "yes",
                                    "requried",
                                    "6"
                                );


                                $attrs[3]["youtuber_profit_per_tshirt"]="number";
                                $attrs[3]["youtuber_lost_per_return_single_tshirt"]="number";

                                echo
                                generate_inputs_html_take_attrs($attrs);
                            ?>

                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading">Product Data</div>
                        <div class="panel-body">


                            <div class="panel-group" id="accordion">

                                <?php foreach($lang_ids as $lang_key=>$lang_item): ?>
                                <?php
                                    $lang_id=$lang_item->lang_id;
                                ?>
                                <div class="panel panel-info">
                                    <div class="panel-heading" data-toggle="collapse" style="cursor: pointer;" data-parent="#accordion" href="#collapse{{$lang_id}}">
                                        <h4 class="panel-title">
                                            <a>
                                                Data
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse{{$lang_id}}" class="panel-collapse collapse <?php echo($lang_key==0)?"in":""; ?>">
                                        <div class="panel-body">

                                            <input type="hidden" name="lang_id[]" value="{{$lang_id}}">
                                        <?php

                                        $translate_data=array();


                                        $current_row = $pro_data_translate_rows->filter(function ($value, $key) use($lang_id) {
                                            if ($value->lang_id == $lang_id)
                                            {
                                                return $value;
                                            }

                                        });

                                        if(is_object($current_row->first())){
                                            $translate_data=$current_row->first();
                                        }



                                        $required=($lang_key==0)?"required":"";

                                        $normal_tags=array(
                                            "pro_name","pro_short_desc","pro_desc",
                                            'pro_search_tags',
                                            'pro_meta_title','pro_meta_desc',
                                            'pro_meta_keywords',
                                        );

                                        $attrs = generate_default_array_inputs_html(
                                            $normal_tags,
                                            $translate_data,
                                            "yes",
                                            $required,
                                            "6"
                                        );


                                        foreach ($attrs[1] as $key => $value) {
                                            $attrs[1][$key].="[]";
                                        }


                                        $attrs[0]["pro_name"]="Product Name";
                                        $attrs[0]["pro_short_desc"] = "Short Description";
                                        $attrs[0]["pro_desc"]="Description";
                                        $attrs[0]["pro_search_tags"]="Search Tags";
                                        $attrs[0]["pro_meta_title"]="Product Meta Title";
                                        $attrs[0]["pro_meta_desc"]="Product Meta Desc";
                                        $attrs[0]["pro_meta_keywords"]="Product Meta Keywords";

                                        $attrs[3]["pro_short_desc"]="textarea";
                                        $attrs[3]["pro_desc"]="textarea";
                                        $attrs[3]["pro_search_tags"]="textarea";
                                        $attrs[3]["pro_meta_desc"]="textarea";
                                        $attrs[3]["pro_meta_keywords"]="textarea";

                                        $attrs[5]["pro_desc"].=" ckeditor";

                                        $attrs[6]["pro_desc"]="12";
                                        $attrs[6]["pro_search_tags"]="12";

                                        $attrs[6]["pro_meta_title"]="4";
                                        $attrs[6]["pro_meta_desc"]="4";
                                        $attrs[6]["pro_meta_keywords"]="4";


                                        echo
                                        generate_inputs_html_take_attrs($attrs);


                                        ?>

                                        </div>
                                    </div>
                                </div>

                                <?php endforeach;?>
                            </div>

                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading">Sizes & Colors</div>
                        <div class="panel-body">

                            <div class="old_items">

                                <?php foreach($product_quantities as $key=>$product_quantity): ?>
                                    <div class="item">
                                        <?php
                                            $form_name_prefix=$product_quantity->pq_id."_";
                                        ?>
                                        @include("admin.subviews.products.save_components.pro_quantity_item")
                                    </div>
                                <?php endforeach; ?>

                            </div>


                            <div class="new_items">

                                <div class="first_new_one">
                                    <?php
                                        $product_quantity="";
                                        $form_name_prefix="new_0_";
                                    ?>
                                    @include("admin.subviews.products.save_components.pro_quantity_item")
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading">Images</div>
                        <div class="panel-body">

                            <?php
                            $img_obj=isset($pro_data->pro_small_img)?$pro_data->pro_small_img:"";
                            echo generate_img_tags_for_form(
                                $filed_name="small_img",
                                $filed_label="small_img",
                                $required_field="",
                                $checkbox_field_name="small_img_checkbox",
                                $need_alt_title="yes",
                                $required_alt_title="",
                                $old_path_value="",
                                $old_title_value="",
                                $old_alt_value="",
                                $recomended_size="",
                                $disalbed="",
                                $displayed_img_width="50",
                                $display_label="Upload small image",
                                (isset($pro_data->pro_small_img))?$pro_data->pro_small_img:""
                            );

                            ?>

                                <hr>

                            <?php
                            echo
                            generate_slider_imgs_tags(
                                $slider_photos=(isset($pro_data->slider_imgs)&&isset_and_array($pro_data->slider_imgs))?$pro_data->slider_imgs:"",
                                $field_name="pro_slider_file",
                                $field_label="upload product images",
                                $field_id="pro_slider_file_id",
                                $accept="image/*",
                                $need_alt_title="yes"
                            );

                            ?>




                        </div>
                    </div>

                    <div class="col-md-12 text-center">
                        {{csrf_field()}}
                        <input type="submit" value="Save" class="btn btn-primary btn-lg">
                    </div>

                </form>
            </div>
        </div>
    </div>


@endsection



