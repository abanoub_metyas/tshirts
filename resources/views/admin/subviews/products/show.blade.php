@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            Products
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Yotuber Name</td>
                    <td>Product Name</td>
                    <td>Reviews</td>
                    <td>Edit</td>
                    <td>Delete</td>
                </tr>
                </thead>

                <tbody>
                    <?php foreach ($all_pros as $key => $pro): ?>

                        <tr id="row<?= $pro->pro_id ?>" >
                            <td><?=$key+1?></td>

                            <td><?= $pro->youtuber_name ?></td>
                            <td><?= $pro->pro_name ?></td>

                            <td>
                                <a href="<?= url("admin/product/show_product_reviews?pro_id=$pro->pro_id") ?>">
                                    <span class="label label-info">
                                        <i class="fa fa-link"></i>
                                    </span>
                                </a>
                            </td>

                            <td>
                                <a href="<?= url("admin/product/save_pro/$pro->pro_id") ?>">
                                    <span class="label label-info">
                                        Edit <i class="fa fa-edit"></i>
                                    </span>
                                </a>
                            </td>

                            <td>
                                <a
                                    href='#'
                                    class="general_remove_item"
                                    data-deleteurl="<?= url("/general_remove_item") ?>"
                                    data-tablename="App\models\product\product_m"
                                    data-itemid="<?= $pro->pro_id ?>"
                                >
                                    <span class="label label-danger">
                                        Delete <i class="fa fa-remove"></i>
                                    </span>
                                </a>
                            </td>
                        </tr>

                    <?php endforeach ?>
                </tbody>

            </table>

        </div>
    </div>




@endsection
