<div class="panel panel-info" id="row{{is_object($product_quantity)?$product_quantity->pq_id:""}}">
    <div class="panel-heading">Item</div>
    <div class="panel-body">

        <div class="col-md-12 text-center" style="margin-bottom: 10px;">
            <?php if(is_object($product_quantity)): ?>
                <button
                    type="button"
                    class="btn btn-danger general_remove_item"
                    data-deleteurl="<?= url("/general_remove_item") ?>"
                    data-tablename="App\models\product\product_quantities_m"
                    data-itemid="<?= $product_quantity->pq_id ?>"
                >
                    Remove
                </button>
            <?php else: ?>
                <button
                    type="button"
                    class="btn btn-info add_new_pro_quantity"
                >
                    Add new item
                </button>
            <?php endif; ?>

                <br>
        </div>


        <?php
            if ($product_quantity==""){
                $product_quantity=new stdClass();
                $product_quantity->{"egypt_or_outside"}="";
                $product_quantity->{"pro_color"}="";
                $product_quantity->{"pro_available_sizes"}="";
                $product_quantity->{"pro_price"}="";
                $product_quantity->{"pro_discount_amount"}="";
                $product_quantity->{"alt"}="";
                $product_quantity->{"title"}="";
                $product_quantity->{"path"}="";

            }


            echo generate_select_tags(
                $field_name=$form_name_prefix."egypt_or_outside",
                $label_name="Egypt or Outside",
                $text=["Egypt","Outside"],
                $values=["egypt","outside"],
                $selected_value=[$product_quantity->egypt_or_outside],
                $class="form-control",
                $multiple="",
                $required="",
                $disabled = "",
                $data = "",
                $grid = "col-md-4",
                $hide_label=false,
                $remove_multiple = false
            );

            echo generate_select_tags(
                $field_name=$form_name_prefix."pro_color",
                $label_name="Select Color",
                $text=$all_colors,
                $values=$all_colors,
                $selected_value=[$product_quantity->pro_color],
                $class="form-control",
                $multiple="",
                $required="",
                $disabled = "",
                $data = "",
                $grid = "col-md-4",
                $hide_label=false,
                $remove_multiple = false
            );


            echo generate_select_tags(
                $field_name=$form_name_prefix."pro_available_sizes",
                $label_name="Available sizes",
                $text=$all_sizes,
                $values=$all_sizes,
                $selected_value=json_decode($product_quantity->pro_available_sizes,true),
                $class="form-control",
                $multiple="multiple",
                $required="",
                $disabled = "",
                $data = "",
                $grid = "col-md-4",
                $hide_label=false,
                $remove_multiple = false
            );


            $normal_tags=array(
                $form_name_prefix.'pro_price',
                $form_name_prefix.'pro_discount_amount'
            );

            $attrs = generate_default_array_inputs_html(
                $normal_tags,
                "",
                "yes",
                "",
                "6"
            );


            $attrs[0][$form_name_prefix."pro_price"]="Price";
            $attrs[0][$form_name_prefix."pro_discount_amount"]="Discount Amount";

            $attrs[3][$form_name_prefix."pro_price"]="number";
            $attrs[3][$form_name_prefix."pro_discount_amount"]="number";

            $attrs[2][$form_name_prefix."pro_price"].=" min='0'";
            $attrs[2][$form_name_prefix."pro_discount_amount"].=" min='0'";

            $attrs[4][$form_name_prefix."pro_price"]=$product_quantity->pro_price;
            $attrs[4][$form_name_prefix."pro_discount_amount"]=$product_quantity->pro_discount_amount;


            echo
            generate_inputs_html_take_attrs($attrs);

        ?>

        <div class="col-md-12">

            <?php
                echo generate_img_tags_for_form(
                    $filed_name=$form_name_prefix."pq_img",
                    $filed_label=$form_name_prefix."pq_img",
                    $required_field="",
                    $checkbox_field_name=$form_name_prefix."pq_img_checkbox",
                    $need_alt_title="yes",
                    $required_alt_title="no",
                    $old_path_value="",
                    $old_title_value="",
                    $old_alt_value="",
                    $recomended_size="",
                    $disalbed="",
                    $displayed_img_width="50",
                    $display_label="Upload image",
                    $product_quantity
                );
            ?>
        </div>


    </div>
</div>

