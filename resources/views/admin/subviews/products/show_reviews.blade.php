@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            Products
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td>Text</td>
                    <td>Rate</td>
                    <td>Delete</td>
                </tr>
                </thead>

                <tbody>
                    <?php foreach ($product_reviews as $key => $product_review): ?>

                        <tr id="row<?= $product_review->review_id ?>" >
                            <td><?=$key+1?></td>

                            <td><?= $product_review->review_name ?></td>
                            <td><?= $product_review->review_text ?></td>
                            <td>
                                {!! draw_stars($product_review->review_rate) !!}
                            </td>

                            <td>
                                <a
                                    href='#'
                                    class="general_remove_item"
                                    data-deleteurl="<?= url("/general_remove_item") ?>"
                                    data-tablename="App\models\product\product_reviews_m"
                                    data-itemid="<?= $product_review->review_id ?>"
                                >
                                    <span class="label label-danger">
                                        Delete <i class="fa fa-remove"></i>
                                    </span>
                                </a>
                            </td>
                        </tr>

                    <?php endforeach ?>
                </tbody>

            </table>

        </div>
    </div>




@endsection
