@extends("admin.main_layout")

@section("subview")

    <?php
    $header_text="Add New";
    $city_id="";

    if (is_object($data_object))
    {
        $header_text="<i class='fa fa-edit'></i> '".$data_object->city_name."'";
        $city_id=$data_object->city_id;
    }

    ?>
    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="{{url("/admin/dashboard")}}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <a href="{{url("/admin/cities/show_all")}}">Show All cities</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span>{!!$header_text!!}</span>
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="fa fa-files-o font-green-sharp"></i>
                        <span class="caption-subject bold uppercase">{!!$header_text!!}</span>
                    </div>
                    <div class="actions">
                        <a href="<?= url("admin/cities/save") ?>" class="btn btn-circle btn-default btn-sm">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>


                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">

                            <form id="save_form" action="<?=url("admin/cities/save/$city_id")?>" method="POST" enctype="multipart/form-data">

                                <div class="alert alert-info">
                                    <p>Aramex shipping is not used from here except for Egypt</p>
                                    <p>if you want to add aramex shipping value for another country add it from edit country</p>
                                </div>

                                <?php
                                    echo generate_select_tags(
                                        $field_name="country_id",
                                        $label_name="Select Country",
                                        $text=$all_countries->pluck("country_name")->all(),
                                        $values=$all_countries->pluck("country_id")->all(),
                                        $selected_value=[""],
                                        $class="form-control",
                                        $multiple="",
                                        $required="",
                                        $disabled = "",
                                        $data = $data_object,
                                        $grid = "col-md-3",
                                        $hide_label=false,
                                        $remove_multiple = false
                                    );


                                    $normal_tags=array('city_name','aramex_shipping','fetchr_shipping');

                                    $attrs = generate_default_array_inputs_html(
                                        $normal_tags,
                                        $data_object,
                                        "yes",
                                        "required",
                                        "3"
                                    );

                                    $attrs[3]["aramex_shipping"]="number";
                                    $attrs[3]["fetchr_shipping"]="number";

                                    echo
                                    generate_inputs_html_take_attrs($attrs);
                                ?>


                                {{csrf_field()}}
                                <input id="submit" type="submit" value="Save" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg" style="margin-top: 10px;">

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



