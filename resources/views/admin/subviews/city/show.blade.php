@extends("admin.main_layout")

@section("subview")

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="{{url("/admin/dashboard")}}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span>Cities</span>
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="fa fa-book font-green-sharp"></i>
                        <span class="caption-subject bold uppercase">Cities</span>
                    </div>
                    <div class="actions">
                        <a href="<?= url("admin/cities/save") ?>" class="btn btn-circle btn-default btn-sm">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>


                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">

                            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Country</td>
                                    <td>City</td>
                                    <td>Aramex</td>
                                    <td>Fetchr</td>
                                    <td>Action</td>
                                </tr>
                                </thead>

                                <tbody>
                                <?php foreach ($all_data as $key => $single): ?>
                                <tr id="row<?= $single->country_id ?>">
                                    <td><?= $key+1; ?></td>
                                    <td>{{$single->country_name}}</td>
                                    <td>{{$single->city_name}}</td>
                                    <td>{{$single->aramex_shipping}}</td>
                                    <td>{{$single->fetchr_shipping}}</td>
                                    <td>
                                        <?php if(check_permission($user_permissions, "admin/cities", "edit_action")): ?>
                                            <a href="<?= url("admin/cities/save/$single->city_id") ?>">
                                                <span class="label label-info">  <i class="fa fa-edit"></i></span>
                                            </a>
                                        <?php endif;?>
                                        <?php if(check_permission($user_permissions, "admin/cities", "delete_action")): ?>
                                            <a
                                                href="#"
                                                class="general_remove_item"
                                                data-deleteurl="<?= url("/general_remove_item") ?>"
                                                data-tablename="App\models\cities_m"
                                                data-itemid="<?= $single->city_id ?>"
                                            >
                                                <span class="label label-danger">  <i class="fa fa-remove"></i></span>
                                            </a>
                                        <?php endif;?>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                                </tbody>

                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection


