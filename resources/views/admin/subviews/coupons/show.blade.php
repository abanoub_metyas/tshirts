@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            Coupons
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Coupon</td>
                    <td>Expiration Date</td>
                    <td>Type</td>
                    <td>Value</td>
                    <td>Edit</td>
                    <td>Delete</td>
                </tr>
                </thead>

                <tbody id="sortable">
                <?php foreach ($all_coupons as $key => $coupon): ?>

                <tr id="row<?= $coupon->coupon_id ?>" data-itemid="<?= $coupon->coupon_id ?>" data-tablename="App\models\coupons_m">
                    <td><?=$key+1?></td>
                    <td>{{$coupon->coupon_code}}</td>
                    <td>{{$coupon->coupon_end_date}}</td>

                    <td>
                        <?php
                        echo generate_multi_accepters(
                                $accepturl="",
                                $item_obj=$coupon,
                                $item_primary_col="coupon_id",
                                $accept_or_refuse_col="is_rate",
                                $model='App\models\coupons_m',
                                $accepters_data=["0"=>"Amount","1"=>"Percentage"]
                        );
                        ?>
                    </td>
                    <td>{{$coupon->coupon_value}}</td>


                    <td>
                        <?php if(check_permission($user_permissions,"admin/coupons","edit_action")): ?>
                            <a href="<?= url("admin/coupons/save_coupon/$coupon->coupon_id") ?>"><span class="label label-info"> Edit <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"admin/coupons","delete_action")): ?>
                            <a href='#' class="general_remove_item" data-deleteurl="<?= url("/general_remove_item") ?>" data-tablename="App\models\coupons_m"  data-itemid="<?= $coupon->coupon_id ?>"><span class="label label-danger"> Delete <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

        </div>
    </div>




@endsection
