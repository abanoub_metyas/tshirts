@extends('admin.main_layout')


@section('subview')


    <style>
        hr{
            width: 100%;
            height:1px;
        }
    </style>
    <?php

    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text="Add new";
    $coupon_id="";

    if ($coupon_data!="") {
        $header_text="Edit ".$coupon_data->coupon_code;

        $coupon_id=$coupon_data->coupon_id;
    }

    //dump($pro_data);
    ?>


    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("admin/coupons/save_coupon/$coupon_id")?>" method="POST" enctype="multipart/form-data">


                    <?php
                        echo
                        generate_select_tags(
                            $field_name="is_rate",
                            $label_name="Type",
                            $text=array("percentage","amount"),
                            $values=["1","0"],
                            $selected_value=array(""),
                            $class="form-control",
                            $multiple="",
                            $required = "",
                            $disabled = "",
                            $data = $coupon_data
                        );
                    ?>
                    <hr>

                    <?php

                    $normal_tags=array('coupon_code','coupon_end_date','coupon_value');
                    $attrs = generate_default_array_inputs_html(
                            $normal_tags,
                            $coupon_data,
                            "yes",
                            $required="required",
                            "4"
                    );


                    $attrs[0]["coupon_code"]="Coupon Code *";
                    $attrs[0]["coupon_end_date"]="Expiration date";
                    $attrs[0]["coupon_value"]="Coupon value";


                    $attrs[3]["coupon_code"]="text";
                    $attrs[3]["coupon_end_date"]="date";
                    $attrs[3]["coupon_value"]="number";

                    echo
                    generate_inputs_html(
                            reformate_arr_without_keys($attrs[0]),
                            reformate_arr_without_keys($attrs[1]),
                            reformate_arr_without_keys($attrs[2]),
                            reformate_arr_without_keys($attrs[3]),
                            reformate_arr_without_keys($attrs[4]),
                            reformate_arr_without_keys($attrs[5]),
                            reformate_arr_without_keys($attrs[6])
                    );
                    ?>

                    <div class="col-md-12 text-center">
                        {{csrf_field()}}
                        <input type="submit" value="save" class="btn btn-primary btn-lg">
                    </div>


                </form>
            </div>
        </div>
    </div>


@endsection



