
@extends('admin.main_layout')

@section('subview')

    <?php
    if (count($errors->all()) > 0)
    {
        $dump = "<div class='alert alert-danger'>";
        foreach ($errors->all() as $key => $error)
        {
            $dump .= $error." <br>";
        }
        $dump .= "</div>";

        echo $dump;
    }

    if (isset($success)&&!empty($success)) {
        echo $success;
    }

    $header_text='اضافة ايميلات جديدة <i class="fa fa-plus"></i>';

    ?>


    <div class="row">
        <form action="<?=url("admin/subscribe/save_email")?>" method="POST" enctype="multipart/form-data">

            <h1><?=$header_text?></h1>

            <?php echo generate_inputs_html(
                    $labels_name = array("الايميل"),
                    $fields_name = array("email"),
                    $required = array(""),
                    $type = array("email"),
                    $values = array(""),
                    $class = array("form-control"))
            ?>

            <br />
            <br />
            <hr>
            <h2> او ارفع ملف اكسيل بجميع الايميلات الخاصة بك</h2>
            <h4> ملف الاكسيل يكون الايميلات في اول عمود مع ترك اول خانه فيه فارغه</h4>
            <p> اذهب الي رافع الملفات الخاص بك من  <a href="<?= url("admin/uploader") ?>" target="_blank"> هنا >> </a> وانسخ للينك الملف المرفوع بالاسفل</p>
            <?php echo generate_inputs_html(
                    $labels_name = array("للينك ملف الاكسيل"),
                    $fields_name = array("sheet_url"),
                    $required = array(""),
                    $type = array("sheet_url"),
                    $values = array(""),
                    $class = array("form-control"))
            ?>

            {{csrf_field()}}
            <input id="submit" type="submit" value="save" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">
        </form>
    </div>

@endsection