@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading" data-toggle="collapse" style="cursor: pointer;"  data-target=".collapse">
            Filtration
        </div>
        <div class="panel-body collapse">

            <form action="" method="GET">

                <?php
                    $normal_tags=array(
                        'start_date',
                        'end_date',
                    );
                    $attrs = generate_default_array_inputs_html(
                        $normal_tags,
                        $post_data,
                        "yes",
                        $required="",
                        "6"
                    );


                    $attrs[3]["start_date"]="date";
                    $attrs[3]["end_date"]="date";

                    echo generate_inputs_html_take_attrs($attrs);
                ?>

                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-info">Filter</button>
                </div>


            </form>

        </div>
    </div>


    <div class="panel panel-info">
        <div class="panel-heading">
            Materials
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Material</td>
                    <td>Size</td>
                    <td>Color</td>
                    <td>Used Quantity</td>
                </tr>
                </thead>

                <tbody id="sortable">
                    <?php foreach ($material_objs as $key => $material): ?>

                        <tr id="row<?= $material->m_id ?>" >
                            <td><?=$key+1?></td>
                            <td>{{$material->m_name}}</td>
                            <td>{{$material->m_size}}</td>
                            <td>{{$material->m_color}}</td>
                            <td>
                                <?php if(isset($printing_requests[$material->m_id])): ?>
                                    {{$printing_requests[$material->m_id]->count()}}
                                <?php endif; ?>
                            </td>
                        </tr>

                    <?php endforeach ?>
                </tbody>

            </table>

        </div>
    </div>




@endsection
