@extends('admin.main_layout')


@section('subview')


    <style>
        hr{
            width: 100%;
            height:1px;
        }
    </style>
    <?php

    $header_text="Add new";
    $m_id="";

    if ($material_data!="") {
        $header_text="Edit ".$material_data->m_name;

        $m_id=$material_data->m_id;
    }

    //dump($pro_data);
    ?>


    <div class="panel panel-primary">
        <div class="panel-heading">
            <?=$header_text?>
        </div>
        <div class="panel-body">
            <div class="">
                <form id="save_form" action="<?=url("admin/materials/save_material/$m_id")?>" method="POST" enctype="multipart/form-data">
                    <?php

                        $normal_tags=array(
                            'm_name',
                            'm_size',
                            'm_color',
                            'm_quantity',
                            'm_critical_limit'
                        );

                        $attrs = generate_default_array_inputs_html(
                            $normal_tags,
                            $material_data,
                            "yes",
                            $required="required",
                            "4"
                        );

                        $attrs[0]["m_name"]="Type (ex: t-shirt, hoodie, etc..) *";
                        $attrs[0]["m_size"]="Size";
                        $attrs[0]["m_color"]="Color";
                        $attrs[0]["m_quantity"]="Quantity";
                        $attrs[0]["m_critical_limit"]="Critical limit";

                        $attrs[2]["m_quantity"].=" step='1'";
                        $attrs[3]["m_quantity"]="number";

                        $attrs[2]["m_critical_limit"].=" step='1'";
                        $attrs[3]["m_critical_limit"]="number";

                        echo  generate_inputs_html_take_attrs($attrs);
                    ?>

                    <div class="col-md-12 text-center">
                        {{csrf_field()}}
                        <input type="submit" value="save" class="btn btn-primary btn-lg">
                    </div>


                </form>
            </div>
        </div>
    </div>


@endsection



