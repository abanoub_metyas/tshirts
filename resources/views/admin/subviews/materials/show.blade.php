@extends('admin.main_layout')

@section('subview')

    <div class="panel panel-info">
        <div class="panel-heading">
            Materials
        </div>
        <div class="panel-body">
            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Material</td>
                    <td>Size</td>
                    <td>Color</td>
                    <td>Quantity</td>
                    <td>Edit</td>
                    <td>Delete</td>
                </tr>
                </thead>

                <tbody id="sortable">
                <?php foreach ($all_materials as $key => $material): ?>

                <tr id="row<?= $material->m_id ?>" data-itemid="<?= $material->m_id ?>" data-tablename="App\models\materials_m">
                    <td><?=$key+1?></td>
                    <td>{{$material->m_name}}</td>
                    <td>{{$material->m_size}}</td>
                    <td>{{$material->m_color}}</td>
                    <td>{{$material->m_quantity}}</td>

                    <td>
                        <?php if(check_permission($user_permissions,"admin/materials","edit_action")): ?>
                            <a href="<?= url("admin/materials/save_material/$material->m_id") ?>"><span class="label label-info"> Edit <i class="fa fa-edit"></i></span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if(check_permission($user_permissions,"admin/materials","delete_action")): ?>
                            <a href='#' class="general_remove_item" data-deleteurl="<?= url("/general_remove_item") ?>" data-tablename="App\models\materials_m"  data-itemid="<?= $material->m_id ?>"><span class="label label-danger"> Delete <i class="fa fa-remove"></i></span></a>
                        <?php endif; ?>
                    </td>
                </tr>

                <?php endforeach ?>
                </tbody>


            </table>

        </div>
    </div>




@endsection
