@extends("admin.main_layout")

@section("subview")

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="{{url("/admin/dashboard")}}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span>{{$youtuber_obj->full_name}}</span>
            </li>
        </ul>
    </div>


    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-info">
                <div class="panel-heading">Filter</div>
                <div class="panel-body">
                    <form action="" method="GET">

                        <input type="hidden" name="youtuber_id" value="{{$youtuber_obj->user_id}}">

                        <?php
                        echo generate_select_years(
                            $already_selected_value="" ,
                            $earliest_year="2018",
                            $class="form-control",
                            $name="filter_year",
                            $label="Select year",
                            $data=$post_data,
                            $grid="12"
                        );
                        ?>

                        <div class="col-md-12 text-center" style="margin-top: 10px;">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>

                    </form>

                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">Profit & Loss ({{$youtuber_obj->full_name}})</div>
                <div class="panel-body">
                    <div class="col-md-12">
                        @include("admin.subviews.users.profit_and_loss_inner_view")
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection



