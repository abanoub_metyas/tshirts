@extends("admin.main_layout")

@section("subview")

    <?php

        $header_text="Add New";
        $user_id="";
        $password_required = "required";

        if ($user_data!="") {
            $header_text="Edit ".$user_data->full_name;
            $user_id=$user_data->user_id;
            $password_required = "";
        }

    ?>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="{{url("/admin/dashboard")}}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <a href="{{url("/admin/users/get_all_users?user_type=$user_type")}}">Show All</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span>{!!$header_text!!}</span>
            </li>
        </ul>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="fa fa-files-o font-green-sharp"></i>
                        <span class="caption-subject bold uppercase">{!!$header_text!!}</span>
                    </div>
                    <div class="actions">
                        <a href="<?= url("admin/admins/save") ?>" class="btn btn-circle btn-default btn-sm">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>


                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="save_form" action="<?=url("admin/users/save/$user_id")?>" method="POST" enctype="multipart/form-data">

                                <input type="hidden" name="user_type" value="{{$user_type}}">

                                <?php

                                    $normal_tags=array("email","username","password");

                                    if ($user_type=="youtuber"){
                                        $normal_tags[]="user_desc";

                                        $normal_tags[]="meta_title";
                                        $normal_tags[]="meta_desc";
                                        $normal_tags[]="meta_keywords";
                                    }

                                    $attrs = generate_default_array_inputs_html(
                                        $normal_tags,
                                        $user_data,
                                        "yes",
                                        "required",
                                        "4"
                                    );

                                    $attrs[0]["email"]="Email";
                                    $attrs[0]["password"]="Password";

                                    $attrs[2]["password"]=$password_required;
                                    $attrs[3]["password"]="password";
                                    $attrs[4]["password"]="";

                                    if ($user_type=="youtuber"){
                                        $attrs[0]["user_desc"]="Description";
                                        $attrs[3]["user_desc"]="textarea";
                                        $attrs[5]["user_desc"].=" ckeditor";
                                        $attrs[6]["user_desc"]="12";


                                        $attrs[3]["meta_desc"]="textarea";
                                        $attrs[3]["meta_keywords"]="textarea";

                                    }

                                    echo generate_inputs_html_take_attrs($attrs);
                                ?>

                                <hr>

                                <div class="col-md-12">
                                    <?php
                                        $img_obj = isset($user_data->user_img_file) ? $user_data->user_img_file : "";

                                        echo generate_img_tags_for_form(
                                            $filed_name="user_img_file",
                                            $filed_label="user_img_file",
                                            $required_field="",
                                            $checkbox_field_name="user_img_checkbox",
                                            $need_alt_title="yes",
                                            $required_alt_title="",
                                            $old_path_value="",
                                            $old_title_value="",
                                            $old_alt_value="",
                                            $recomended_size="",
                                            $disalbed="",
                                            $displayed_img_width="100",
                                            $display_label="Upload User Image",
                                            $img_obj
                                        );

                                        if ($user_type=="youtuber"){

                                            echo generate_img_tags_for_form(
                                                $filed_name="cover_img_file",
                                                $filed_label="cover_img_file",
                                                $required_field="",
                                                $checkbox_field_name="cover_img_checkbox",
                                                $need_alt_title="yes",
                                                $required_alt_title="",
                                                $old_path_value="",
                                                $old_title_value="",
                                                $old_alt_value="",
                                                $recomended_size="",
                                                $disalbed="",
                                                $displayed_img_width="100",
                                                $display_label="Upload User Cover",
                                                isset($user_data->cover_img_file) ? $user_data->cover_img_file : ""
                                            );

                                        }

                                    ?>

                                </div>


                                {{csrf_field()}}
                                <input type="submit" value="Save" class="col-md-4 col-md-offset-4 btn btn-primary btn-lg">
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

