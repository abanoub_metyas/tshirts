<table class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Month</th>
        <th>Profit</th>
        <th>Loss</th>
        <th>Total</th>
    </tr>
    </thead>

    <tbody>
    <?php for($i=1;$i<=12;$i++): ?>
    <?php
        $profit=0;
        $loss=0;
        if(isset($youtuber_profit[$i])){
            $profit=$youtuber_profit[$i]->sum("profit");
        }

        if(isset($youtuber_loss[$i])){
            $loss=$youtuber_loss[$i]->sum("loss");
        }

    ?>
    <tr>
        <td>{{$i}}</td>
        <td>
            {{$profit}}
        </td>
        <td>
            {{$loss}}
        </td>
        <td class="{{$profit-$loss>=0?"success bg-success":"danger bg-danger"}}">
            {{$profit-$loss}}
        </td>
    </tr>
    <?php endfor; ?>
    </tbody>
</table>
