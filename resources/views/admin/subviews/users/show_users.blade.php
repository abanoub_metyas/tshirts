@extends("admin.main_layout")

@section("subview")

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="{{url("/admin/dashboard")}}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span>Show All</span>
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="fa fa-files-o font-green-sharp"></i>
                        <span class="caption-subject bold uppercase">Show All</span>
                    </div>
                    <div class="actions">
                        <a href="<?= url("admin/users/save?user_type=$user_type") ?>" class="btn btn-circle btn-default btn-sm">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>


                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">

                            <table id="cat_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Email</td>
                                    <td>Full Name</td>

                                    <?php if($user_type=="youtuber"): ?>
                                        <td>Profile link</td>
                                        <td>Orders of fans</td>
                                        <td>Return Orders</td>
                                        <td>Profit & Loss</td>
                                    <?php endif; ?>

                                    <td>Edit</td>
                                    <td>Remove</td>
                                </tr>
                                </thead>

                                <tbody>
                                    <?php foreach ($users as $key => $user): ?>
                                        <tr id="row<?= $user->user_id ?>">
                                            <td><?=$key+1?></td>
                                            <td><?=$user->email ?></td>
                                            <td><?=$user->full_name ?></td>

                                            <?php if($user_type=="youtuber"): ?>
                                                <td>
                                                    <a  class="btn btn-info" href="{{url("stores/$user->full_name")}}">
                                                        <i class="fa fa-link"></i>
                                                    </a>
                                                </td>

                                                <td>
                                                    <a  class="btn btn-info" href="{{url("/service_operator/orders/show_all?youtuber_id=$user->user_id")}}">
                                                        <i class="fa fa-link"></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a  class="btn btn-info" href="{{url("/service_operator/return_orders/show_all?youtuber_name=$user->full_name")}}">
                                                        <i class="fa fa-link"></i>
                                                    </a>
                                                </td>

                                                <td>
                                                    <a  class="btn btn-info" href="{{url("/admin/users/show_profit_and_loss?youtuber_id=$user->user_id")}}">
                                                        <i class="fa fa-link"></i>
                                                    </a>
                                                </td>
                                            <?php endif; ?>

                                            <td>
                                                <a href="<?= url("admin/users/save/$user->user_id?user_type=$user_type") ?>">
                                                    <span class="label label-info">  <i class="fa fa-edit"></i></span>
                                                </a>
                                            </td>
                                            <td>
                                                <a
                                                    href="#"
                                                    class="general_remove_item"
                                                    data-deleteurl="<?= url($user_type=="admin"?"/admin/users/remove_admin":"general_remove_item") ?>"
                                                    data-tablename="App\User"
                                                    data-itemid="<?= $user->user_id ?>"
                                                >
                                                    <span class="label label-danger">
                                                        <i class="fa fa-remove"></i>
                                                    </span>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>

                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



