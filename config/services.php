<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id' => '776131693011-puso13hdfuv85s4kk6l82aocv0f5vngp.apps.googleusercontent.com',
        'client_secret' => 'Zbn0FOV5XBStd-r9ycuiu_iC',
        'redirect' => url('/login/google/callback'),
    ],

    'facebook' => [
        'client_id' => '437704123670219',
        'client_secret' => 'f98e73d1a50f548f94f39ebc2111946d',
        'redirect' => url('/login/facebook/callback'),
    ],

];
